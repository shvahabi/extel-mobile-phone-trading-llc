﻿
var ssa = 1;
var NPParams = {};
var isloaded = false;
var combodata = {};
var Purecombodata = {};
var Extra = {};
var cdata = null;
var combowait = [];
var thisrow = new Array();
var counter = 1;
var drawid = 1;
var free = true;
var freeform = true;
var freene = true;
var allmande;
var mmdd;
var Token = "";
var UserName = "";
var CompanyName = "";
var typingTimer;
var ExtraReady = true;
var test = {};
window.chartColors = [
    'rgb(255, 99, 132)',
    'rgb(0, 255, 191)',
    'rgb(255, 0, 64)',
    'rgb(153, 102, 255)',
    'rgb(0, 128, 255)',
    'rgb(191, 255, 0)',
    'rgb(64, 0, 255)',
    'rgb(0, 255, 128)',
    'rgb(255, 0, 191)',
    'rgb(255, 205, 86)',
    'rgb(128,128,0)',
    'rgb(60,179,113)',
    'rgb(255,192,203)',
    'rgb(75, 192, 192)',
    'rgb(54, 162, 235)',
    'rgb(128, 0, 255)',
    'rgb(201, 203, 207)',
    'rgb(210,180,140)'];
//_____________________RManager__________________________//
class Dash {
}
//______________________Classess_________________________//
class DashBoard {
    //++++++++++++user action++++++++++++++++//
    static getItemValue(id) {
        var el = document.getElementById(id);
        if (el) {
            switch (el.getAttribute("UsType")) {
                case "6":
                    return TextBox.getValue(id);
                    break;
                case "7":
                    return ComboBox.getValue(id);
                    break;
                case "8":
                    return DatePicker.getValue(id);
                    break;
                case "9":
                    return TimePicker.getValue(id);
                    break;
                case "10":
                    return CheckBox.getValue(id);
                    break;
                case "12":
                    return MaskTextBox.getValue(id);
                    break;
            }
        } else {
            console.error("Item Not Fount");
        }
    }
    //++++++++++++++inner action++++++++++++++//
    static itemcreate(item, bit, moduals, di, isEditMode) {

        if (bit) {
            item.parent = item.parent + '-body';
            switch (item.placeid) {
                case 1:
                    item.parent = item.parent;
                    break;
                case 2:
                    item.parent = item.parent.replace('-body', '-footer');
                    break;
                case 3:
                    item.parent = item.parent.replace('-body', '-header');
                    break;
            }
        }
        if (di != null) {
            di.id = "tempid" + ssa.toString(); ssa++;
            item.parent = di.id;
        }
        var temp = document.getElementById(item.parent);
        if (!temp) {
            temp = document.getElementById(item.parent.replace("Edit", ""));
            item.parent = item.parent.replace("Edit", "");
        }

        if (!temp) {
            temp = document.getElementById("Edit" + item.parent);
            item.parent = "Edit" + item.parent;
        }
        if (!temp) {
            console.log(item);
        }
        var mode = temp.parentElement.getAttribute("mode");
        if (item.nonfilter) {
            var doc = document.getElementById(item.parent).parentElement.parentElement;
            if (doc.attributes.getNamedItem("KG-FIL")) {
                return;
            }
        }
        if (item.modalhide || item.dvisible) {
            if (mode == "modal") {
                return;
            }
        }
        if (item.modalhide || item.fvisible) {
            if (mode == "form") {
                return;
            }
        }
        if (item.requirement) {
            if (eval(item.requirement) != true) {
                switch (item.itemTypeId) {
                    case 13: // grid
                        Dash["GridTable" + item.id] = new NOTRE(item);
                        //DashBoard.addgrid(item);
                        break;
                    case 11: // button
                        // DashBoard.addbutton(item);
                        Dash["Btn" + item.id] = new NOTRE(item);
                        break;
                }
                return;
            }
        }
        switch (item.itemTypeId) {
            case 1: // smallbox
                DashBoard.addSmallBox(item.id, item.color, item.title, item.icon, item.value, item.parent, item);
                break;
            case 2: // barchart
                DashBoard.addchart(item, moduals);
                break;
            case 3: // smallboxprogress
                DashBoard.addSmallProgressBox(item.id, item.color, item.title, item.icon, item.value, item.progress, item.description, item.parent);
                break;
            case 4: // smallInfoBox
                DashBoard.addSmallInfoBox(item.id, item.color, item.title, item.icon, item.value, item.link, item.parent);
                break;
            case 5: // progress
                DashBoard.addprogressbar(item);
                break;
            case 6: // textbox
                Dash[item.dbsource + item.id] = new TBox(item);
                // DashBoard.addtextbox(item);
                break;
            case 7: // combobox
                //DashBoard.addcombobox(item);
                Dash[item.dbsource + item.id] = new CBox(item);
                break;
            case 8: // datepicker
                //DashBoard.adddatepicker(item);
                Dash[item.dbsource + item.id] = new PDatePicker(item);
                break;
            case 9: // timepicker
                // DashBoard.addtimepicker(item);
                Dash[item.dbsource + item.id] = new TimePicker(item);
                break;
            case 10: // checkbox
                Dash[item.dbsource + item.id] = new PCheckBox(item);
                //  DashBoard.addcheckbox(item);
                break;
            case 11: // button
                // DashBoard.addbutton(item);
                Dash["Btn" + item.id] = new Btn(item);
                break;
            case 12: // masktextbox
                //  DashBoard.addmasktextbox(item);
                Dash[item.dbsource + item.id] = new MTbox(item);
                break;
            case 13: // grid
                Dash["GridTable" + item.id] = new Grid(item);
                //DashBoard.addgrid(item);
                break;
            case 14: // grid
                Dash["GridTable" + item.id] = new DGrid(item, isEditMode);

                break;
            case 15: // RADIO
                DashBoard.addradio(item);
                break;
            case 16: // MultylineTextBox
                // DashBoard.addtextboxMline(item);
                Dash[item.dbsource + item.id] = new MultiTBox(item);
                break;
            case 17: // Number
                Dash[item.dbsource + item.id] = new NumBox(item);
                break;
            case 18: // NumberDecimal
                Dash[item.dbsource + item.id] = new NumDecBox(item);
                break;
            case 20: // FileUploader
                Dash[item.dbsource + item.id] = new FileUpload(item);
                break;
            case 21: // customhtml
                Dash["CHTML" + item.id] = new CHTML(item);
                break;
            case 22: // tablewiz
                Dash["TBLWIZ" + item.id] = new TBLWIZ(item);
                break;
            case 23: // formdetails
                Dash["FD" + item.id] = new FormDetails(item);
                break;
            case 24: // ReportGrid
                Dash["ReportGrid" + item.id] = new ReportGrid(item);
                break;
        }
    }
    static addHeader2Menu(text, id) {
        var menu = document.getElementById('Lmenu');
        var li = document.createElement("li");
        if (id)
            li.id = 'leftSideHeaderMenu' + id;
        li.className = " header ";
        li.innerHTML = text;
        menu.appendChild(li);
    }
    static addtempdiv(item) {
        if (true) {
            item.parent = item.parent + '-body';
            switch (item.placeid) {
                case 1:
                    item.parent = item.parent;
                    break;
                case 2:
                    item.parent = item.parent.replace('-body', '-footer');
                    break;
                case 3:
                    item.parent = item.parent.replace('-body', '-header');
                    break;
            }

        }
        var main = null;
        if (item.parent) {
            //switch (item.placeid) {
            //    case 1:
            main = document.getElementById(item.parent);
            //    break;
            //case 2:
            //    main = document.getElementById(item.parent.replace('-body', '-footer'));
            //    break;
            //case 3:
            //    main = document.getElementById(item.parent.replace('-body', '-header'));
            //    break;
            // }
        } else {
            main = document.getElementById('MainContent');
        }
        if (main == null) {
            main = document.getElementById(item.parent.replace("-body", ""));
        }
        var div = document.createElement("div");
        div.style = "margin-bottom: 21px;";
        main.appendChild(div);
        return div;

    }
    static addMenuItem(text, iconclassname, id) {
        var menu = document.getElementById('Lmenu');
        var li = document.createElement("li");
        if (id)
            li.id = 'leftSideMenu' + id;
        var a = document.createElement("a");
        a.href = "#";
        var i = document.createElement("i");
        i.className = iconclassname;


        a.appendChild(i);
        var span = document.createElement("span");
        span.innerHTML = text;
        span.style.textAlign = "left";
        span.style.marginRight = "-6px";
        span.style.paddingRight = "10px";

        a.appendChild(span);
        li.appendChild(a);
        menu.appendChild(li);
    }
    static addMenuItemLevel(text, iconclassname, ParentId, id, title) {
        if (title) {

        } else {
            title = "";
        }
        var menu = document.getElementById('Lmenu');
        var parent = document.getElementById(ParentId);
        var childrencount = parent.children[0].childElementCount;
        if (childrencount < 3) {
            var levelspan = document.createElement("span");
            levelspan.className = "pull-right-container";
            var span_i = document.createElement("i");
            span_i.className = "fa fa-angle-left pull-right";
            levelspan.appendChild(span_i);
            parent.children[0].appendChild(levelspan);
            parent.className = "treeview animated fadeInDown";
        }

        if (parent.childElementCount < 2) {
            var ul = document.createElement("ul");
            ul.className = "treeview-menu ";
            parent.appendChild(ul);
        }
        var li = document.createElement("li");
        if (id)
            li.id = 'leftSideMenu' + id;
        var a = document.createElement("a");
        a.href = "#";

        var i = document.createElement("i");
        i.className = iconclassname;
        a.appendChild(i);
        var span = document.createElement("span");
        span.innerHTML = text;
        a.appendChild(span);
        li.appendChild(a);
        li.title = title;
        parent.children[1].appendChild(li);

    }
    static addMenuItemLevelEntity(text, iconclassname, ParentId, id, title) {
        if (title) {

        } else {
            title = "";
        }
        var menu = document.getElementById('Lmenu');
        var parent = document.getElementById(ParentId);
        if (parent) {


            var childrencount = parent.children[0].childElementCount;
            if (childrencount < 3) {
                var levelspan = document.createElement("span");
                levelspan.className = "pull-right-container";
                var span_i = document.createElement("i");
                span_i.className = "fa fa-angle-left pull-right";
                levelspan.appendChild(span_i);
                parent.children[0].appendChild(levelspan);
                parent.className = "treeview animated fadeInDown";
            }

            if (parent.childElementCount < 2) {
                var ul = document.createElement("ul");
                ul.className = "treeview-menu";
                parent.appendChild(ul);
            }
            var li = document.createElement("li");
            if (id)
                li.id = 'leftSideMenuEn' + id;
            var a = document.createElement("a");
            a.href = "#";

            var i = document.createElement("i");
            i.className = iconclassname;
            a.appendChild(i);
            var span = document.createElement("span");
            span.innerHTML = text;
            span.style.paddingRight = "10px";
            a.appendChild(span);
            li.appendChild(a);
            li.title = title;
            parent.children[1].appendChild(li);
            li.onclick = function (item) {

                Dash = new class { };
                NPParams = {};
                getEntityItems(item.target);

                if ($(window).width() <= 779) {
                    $(document).find('[data-toggle="push-menu"]').trigger('click');
                }
                //  console.log(item.target);

            }
        }
    }
    static addBox(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata) {
        var main = null;
        if (parent != undefined) {
            main = document.getElementById('MainLayout' + parent);
        }
        else {
            main = document.getElementById('MainContent');
        }
        if (main == null) {
            main = document.getElementById('MainLayout' + parent + "-body");
        }
        var box = document.createElement("Div");
        box.className = classname + " " + boxtype + " animated fadeIn ";
        box.id = 'Box' + id;
        if (header) {
            var boxheader = document.createElement("Div");
            boxheader.className = "box-header";
            boxheader.id = id + '-header';
            var boxtitle = document.createElement("h3");
            boxtitle.className = "box-title";
            boxtitle.innerHTML = title;
            if (icn != undefined) {
                var boxicn = document.createElement("i");
                boxicn.setAttribute("style", "color: turquoise;");
                boxicn.className = icn;
                boxheader.appendChild(boxicn);
            }
            boxheader.appendChild(boxtitle);
            var btndiv = document.createElement("Div");
            btndiv.className = "box-tools pull-right";
            btndiv.id = 'Box' + id + 'hbbtnbox';

            var closebtn = document.createElement("Button");
            closebtn.className = "btn btn-box-tool";
            closebtn.type = "button";
            closebtn.setAttribute("data-widget", "collapse");
            var icncol = document.createElement("i");
            icncol.className = "fa fa-minus";
            closebtn.appendChild(icncol);

            var closebtn2 = document.createElement("Button");
            closebtn2.className = "btn btn-box-tool";
            closebtn2.type = "button";
            closebtn2.setAttribute("data-widget", "remove");
            var icncol2 = document.createElement("i");
            icncol2.className = "fa fa-times";
            closebtn2.appendChild(icncol2);


            btndiv.appendChild(closebtn);
            btndiv.appendChild(closebtn2);



            boxheader.appendChild(btndiv);
            //
            box.appendChild(boxheader);

        }

        var boxbody = document.createElement("Div");
        boxbody.className = "box-body";
        boxbody.id = id + '-body';
        box.appendChild(boxbody)
        if (footer) {
            var boxfooter = document.createElement("Div");
            boxfooter.className = "box-footer";
            boxfooter.id = id + '-footer';
            box.appendChild(boxfooter)
        }


        main.appendChild(box);
        return box;
    }
    static addForm(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, masterid, otheroption) {




        var main = null;
        if (parent) {
            if (infooter) {
                main = document.getElementById('MainLayout' + parent + "-footer");
            }
            else {

                main = document.getElementById('MainLayout' + parent);

            }
        }
        else {
            main = document.getElementById('MainContent');
        }
        if (main == null) {
            main = document.getElementById('MainLayout' + parent + "-body");
        }
        if (main == null) {
            if (document.getElementById(parent)) {
                main = document.getElementById(parent);
            }
        }

        var bigdiv = document.createElement("div");
        bigdiv.style.overflow = "hidden";
        bigdiv.className = "form-inline form-inline-rtl";
        if (iseditmode != undefined && iseditmode >= 0)
            bigdiv.setAttribute("mode", "modal");
        else
            bigdiv.setAttribute("mode", "form");


        if (hasdata != undefined && hasdata)
            bigdiv.setAttribute("datamode", "edit");
        else
            bigdiv.setAttribute("datamode", "new");

        if (idprop)
            bigdiv.setAttribute("BGID", idprop);
        if (targetid)
            bigdiv.setAttribute("targettableid", targetid);
        bigdiv.id = id;
        var bodydiv = document.createElement("div");
        bodydiv.id = id + "-body";
        var footerdiv = document.createElement("div");
        footerdiv.className = "rtlform-footer";
        footerdiv.id = id + "-footer";



        var details = document.createElement("Div");

        details.innerHTML = "<div  class=\"nav-tabs-custom bg-teal-gradient\"><ul class=\"nav nav-tabs pull-right \"><li class=\"pull-left header\"><i class=\"fa fa-inbox\"></i> </li></ul><div style=\"border: 1px solid #d2d6de;\" class=\"tab-content no-padding\"></div></div>"

        details.setAttribute("style", "visibility: hidden;height: 0px;");
        details.setAttribute("kg-dtz", "")

        bigdiv.appendChild(bodydiv);
        bigdiv.appendChild(details);
        bigdiv.appendChild(footerdiv);

        if (otheroption && otheroption.hasmaster) {
            bigdiv.style = "float: unset !important;overflow:unset !important;"
        }


        if (masterid) {

            bigdiv.style = "float: unset !important;overflow:unset !important;"
            main = document.getElementById("MainLayout" + masterid);

            if (main == null) {
                main = document.getElementById("EditMainLayout" + masterid);
            }
            var lii = document.createElement("li");

            lii.innerHTML = "<a href=\"#" + id + "Tab\" data-toggle=\"tab\" aria-expanded=\"true\">" + title + "</a>";
            var bool = false;
            if (main && main.children[1].children[0].children[0].children.length == 1) {
                lii.className = " active ";
                bool = true;
            }
            if (main)
                main.children[1].children[0].children[0].insertBefore(lii, main.children[1].children[0].children[0].lastChild);





            var dd = document.createElement("div");
            if (bool)
                dd.className = "tab-pane active ";
            else
                dd.className = "tab-pane ";
            dd.id = id + "Tab";
            dd.setAttribute("type", "grid");
            dd.setAttribute("style", "position: relative; padding-top: 10px;");
            dd.appendChild(bigdiv);
            if (main) {

                main.children[1].children[0].children[1].appendChild(dd);
                main.children[1].setAttribute("style", "margin-top: 20px;");
            }

        }
        else {

            main.appendChild(bigdiv);
        }

        return bigdiv;

        //$.ajax({

        //    url: "../api/values/getLayoutItems?id=" + id.replace('MainLayout', '').replace("Edit", ""), success: function (result) {
        //        if (result.startsWith('NotFound-')) {
        //            free = true;
        //            // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
        //        }
        //        else if (result == ("NL")) {
        //            window.location.replace("../Dashboard/Login");
        //        }
        //        else {
        //            var moduals = JSON.parse(result);
        //            moduals.forEach(function (item) {
        //                DashBoard.additemfrom(item, iseditmode);
        //            });
        //            $("#" + parent).preloader('remove');
        //            //  free = true;
        //            startlife();
        //        }
        //    }

        //});




        //if (iseditmode != undefined && iseditmode >= 0) {
        //    $.ajax({
        //        url: "../api/values/getLayoutItemsForDetails?id=" + id.replace('MainLayout', '').replace("Edit", ""), success: function (result) {
        //            if (result.startsWith('NotFound-')) {
        //                // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
        //                freene = true;
        //            }
        //            else if (result == ("NL")) {
        //                window.location.replace("../Dashboard/Login");
        //            }
        //            else {
        //                var moduals = JSON.parse(result);
        //                moduals.forEach(function (item) {
        //                    DashBoard.additemfrom(item, iseditmode);
        //                });
        //                //   $("#modal-default").preloader('remove');
        //                //   free = true;
        //                startlife();
        //            }
        //            free = true;
        //        }

        //    });


        //}
        //else {
        //    freene = true;
        //}



    }
    static addTabForm(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, masterid) {




        var main = null;
        if (parent) {
            if (infooter) {
                main = document.getElementById('MainLayout' + parent + "-footer");
            } else {

                main = document.getElementById('MainLayout' + parent);

            }
        }
        else {
            main = document.getElementById('MainContent');
        }
        if (main == null) {
            main = document.getElementById('MainLayout' + parent + "-body");
        }

        var bigdiv = document.createElement("div");
        bigdiv.style.overflow = "hidden";
        bigdiv.className = "form-inline form-inline-rtl";
        if (iseditmode != undefined && iseditmode >= 0)
            bigdiv.setAttribute("mode", "modal");
        else
            bigdiv.setAttribute("mode", "form");


        if (hasdata != undefined && hasdata)
            bigdiv.setAttribute("datamode", "edit");
        else
            bigdiv.setAttribute("datamode", "new");

        if (idprop)
            bigdiv.setAttribute("BGID", idprop);
        if (targetid)
            bigdiv.setAttribute("targettableid", targetid);
        bigdiv.id = id;
        var bodydiv = document.createElement("div");
        bodydiv.id = id + "-body";
        var footerdiv = document.createElement("div");
        footerdiv.className = "rtlform-footer";
        footerdiv.id = id + "-footer";



        var details = document.createElement("Div");

        details.innerHTML = "<div  class=\"nav-tabs-custom bg-teal-gradient\"><ul class=\"nav nav-tabs pull-right \"><li class=\"pull-left header\"><i class=\"fa fa-inbox\"></i> </li></ul><div style=\"border: 1px solid #d2d6de;\" class=\"tab-content no-padding\"></div></div>"

        details.setAttribute("style", "visibility: hidden;height: 0px;");
        details.setAttribute("kg-dtz", "")

        bigdiv.appendChild(details);
        bigdiv.appendChild(bodydiv);
        bigdiv.appendChild(footerdiv);




        if (masterid) {

            bigdiv.style = "float: unset !important;overflow:unset !important;"
            main = document.getElementById("MainLayout" + masterid);

            if (main == null) {
                main = document.getElementById("EditMainLayout" + masterid);
            }
            var lii = document.createElement("li");

            lii.innerHTML = "<a href=\"#" + id + "Tab\" data-toggle=\"tab\" aria-expanded=\"true\">" + title + "</a>";
            var bool = false;
            if (main && main.children[1].children[0].children[0].children.length == 1) {
                lii.className = " active ";
                bool = true;
            }
            if (main)
                main.children[1].children[0].children[0].insertBefore(lii, main.children[1].children[0].children[0].lastChild);





            var dd = document.createElement("div");
            if (bool)
                dd.className = "tab-pane active ";
            else
                dd.className = "tab-pane ";
            dd.id = id + "Tab";
            dd.setAttribute("type", "grid");
            dd.setAttribute("style", "position: relative; padding-top: 10px;");
            dd.appendChild(bigdiv);
            if (main) {

                main.children[1].children[0].children[1].appendChild(dd);
                main.children[1].setAttribute("style", "margin-top: 20px;");
            }

        }
        else {

            main.appendChild(bigdiv);
        }

        return bigdiv;

        //$.ajax({

        //    url: "../api/values/getLayoutItems?id=" + id.replace('MainLayout', '').replace("Edit", ""), success: function (result) {
        //        if (result.startsWith('NotFound-')) {
        //            free = true;
        //            // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
        //        }
        //        else if (result == ("NL")) {
        //            window.location.replace("../Dashboard/Login");
        //        }
        //        else {
        //            var moduals = JSON.parse(result);
        //            moduals.forEach(function (item) {
        //                DashBoard.additemfrom(item, iseditmode);
        //            });
        //            $("#" + parent).preloader('remove');
        //            //  free = true;
        //            startlife();
        //        }
        //    }

        //});




        //if (iseditmode != undefined && iseditmode >= 0) {
        //    $.ajax({
        //        url: "../api/values/getLayoutItemsForDetails?id=" + id.replace('MainLayout', '').replace("Edit", ""), success: function (result) {
        //            if (result.startsWith('NotFound-')) {
        //                // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
        //                freene = true;
        //            }
        //            else if (result == ("NL")) {
        //                window.location.replace("../Dashboard/Login");
        //            }
        //            else {
        //                var moduals = JSON.parse(result);
        //                moduals.forEach(function (item) {
        //                    DashBoard.additemfrom(item, iseditmode);
        //                });
        //                //   $("#modal-default").preloader('remove');
        //                //   free = true;
        //                startlife();
        //            }
        //            free = true;
        //        }

        //    });


        //}
        //else {
        //    freene = true;
        //}



    }
    static adddetailsForm(itemm) {

        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + itemm.formid, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }

                var moduals = JSON.parse(result);

                moduals.forEach(function (item) {

                    DashBoard.addLayouts('MainLayout' + item.id, item.classname, "newmd" + itemm.formid, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, 0, item.idproperty, null, null, itemm.masterid);
                });



            }
        });

    }
    static addLayouts(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, masterid) {

        if (isbox) {
            Dash["Box" + id] = new BoxControl(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata);
            Dash["Box" + id].load();
        }

        else if (isform) {

            if (isform != 1) {
                Dash["TabForm" + id] = new TabFormControl(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, masterid);
                Dash["TabForm" + id].load();

            }
            else {
                Dash["Form" + id] = new FormControl(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, masterid);
                Dash["Form" + id].load();
            }
        }
        else {
            var main = null;
            if (parent) {
                if (infooter) {
                    main = document.getElementById('MainLayout' + parent + "-footer");
                } else {
                    main = document.getElementById('MainLayout' + parent);
                }
            }
            else {
                main = document.getElementById('MainContent');
            }
            if (main == null) {
                main = document.getElementById('MainLayout' + parent + "-body");
            }
            var row = null;
            if (classname.indexOf('connectedSortable ui-sortable') < 0) {
                row = document.createElement("Div");
            } else {
                row = document.createElement("section");
            }
            row.className = classname;
            row.id = id;
            main.appendChild(row);
            $.ajax({
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: "../api/values/getLayoutItems?id=" + id.replace('MainLayout', '').replace("details", "").replace("Edit", "") + "&draw=" + drawid, success: function (result) {
                    if (result.startsWith('NotFound-')) {
                        DashBoard.LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
                    }
                    else if (result == ("NL")) {
                        window.location.replace("../Dashboard/Login");
                    }
                    else {
                        if (result.startsWith(drawid)) {
                            var moduals = JSON.parse(result.substring(drawid.toString().length));
                            moduals.forEach(function (item) {
                                DashBoard.additemLayout(item);

                            });
                        }
                    }
                }

            });
        }
    }
    static addSmallBox(id, color, title, iconn, valuee, parent, item) {
        var main = null;
        if (parent) {
            //switch (item.placeid) {
            //    case 1:
            main = document.getElementById(parent);
            //    break;
            //case 2:
            //    main = document.getElementById(item.parent.replace('-body', '-footer'));
            //    break;
            //case 3:
            //    main = document.getElementById(item.parent.replace('-body', '-header'));
            //    break;
            // }
        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        var container = document.createElement("Div");
        var span = document.createElement("Span");
        var message = document.createElement("Span");
        var value = document.createElement("Span");
        var icon = document.createElement("i");
        container.className = "info-box-content";
        message.className = "info-box-text";
        value.className = "info-box-number";
        if (item.valstyle)
            value.setAttribute("style", item.valstyle);
        div.className = " info-box animated fadeInDown ";
        span.className = " info-box-icon " + color;
        if (item.onclick) {
            span.onclick = new Function(item.onclick);
            span.style = "cursor: pointer;";
        }
        div.id = 'SmallBox' + id;
        icon.className = iconn;
        span.appendChild(icon);
        div.appendChild(span);
        message.innerHTML = title;
        value.innerHTML = valuee;
        container.appendChild(message);
        container.appendChild(value);
        div.appendChild(container);
        main.appendChild(div);
    }
    static addSmallProgressBox(id, color, title, iconn, valuee, progress, descriptionn, parent) {
        var main = null;
        if (parent) {
            main = document.getElementById(parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        var container = document.createElement("Div");
        var span = document.createElement("Span");
        var message = document.createElement("Span");
        var value = document.createElement("Span");
        var icon = document.createElement("i");
        var progressdiv = document.createElement("Div");
        var progressbar = document.createElement("Div");
        var description = document.createElement("Span");
        container.className = "info-box-content";
        message.className = "info-box-text";
        value.className = "info-box-number";
        div.className = "info-box " + color;
        span.className = " info-box-icon ";
        progressdiv.className = "progress";
        progressbar.className = "progress-bar";
        description.className = "progress-description";
        progressbar.style.width = progress.toString() + '%';
        description.innerHTML = descriptionn
        div.id = 'SmallProgressBox' + id;
        progressdiv.appendChild(progressbar);
        icon.className = iconn;
        span.appendChild(icon);
        div.appendChild(span);
        message.innerHTML = title;
        value.innerHTML = valuee;
        container.appendChild(message);
        container.appendChild(value);
        container.appendChild(progressdiv);
        container.appendChild(description);
        div.appendChild(container);
        main.appendChild(div);
    }
    static addSmallInfoBox(id, color, title, iconn, value, linkset, parent) {
        var main = null;
        if (parent) {
            main = document.getElementById(parent);
        } else {
            main = document.getElementById('MainContent');
        }

        var div = document.createElement("Div");
        div.id = "SmallInfoBox" + id;
        div.className = "small-box " + color;
        var fchdiv = document.createElement("Div");
        fchdiv.className = "inner";
        var schdiv = document.createElement("Div");
        schdiv.className = "icon";
        var val = document.createElement("H3");
        val.innerHTML = value;
        var titlee = document.createElement("P");
        titlee.innerHTML = title;
        var icon = document.createElement("i");
        icon.className = iconn;
        var link = document.createElement("a");
        link.href = linkset;
        link.className = "small-box-footer";
        var linkicon = document.createElement("i");
        linkicon.className = "fa fa-arrow-circle-right";
        fchdiv.appendChild(val);
        fchdiv.appendChild(titlee);
        div.appendChild(fchdiv);
        schdiv.appendChild(icon);
        div.appendChild(schdiv);
        link.appendChild(linkicon);
        div.appendChild(link);
        main.appendChild(div);






    }
    static addprogressbar(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var bigdiv = document.createElement("Div");
        var stringg = "<p><code>" + "تست" + "</code></p>";
        bigdiv.innerHTML = stringg;

        var div = document.createElement("Div");
        div.className = "progress";
        var indiv = document.createElement("Div");
        indiv.className = "progress-bar progress-bar-primary progress-bar-striped";
        indiv.setAttribute("role", "progressbar");
        indiv.setAttribute("aria-valuenow", "40");
        indiv.setAttribute("aria-valuemin", "0");
        indiv.setAttribute("aria-valuemax", "100");
        indiv.style.width = "70%";
        var span = document.createElement("Span");
        span.className = "sr-only";
        span.innerHTML = "تست";
        indiv.appendChild(span);
        div.appendChild(indiv);
        bigdiv.appendChild(div);

        main.appendChild(bigdiv);




    }
    static additemBox(item) {

        if (item.hasq != undefined && item.hasq == '1') {
            var div = DashBoard.addtempdiv(item);
            $.ajax({
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: "../api/values/getDashboardItemData?id=" + item.id, data: NPParams, success: function (result) {
                    if (result == ("NL")) {
                        window.location.replace("../Dashboard/Login");
                    }
                    var moduals = JSON.parse(result);
                    $.each(item, function (index, value) {
                        if (value != undefined && value.toString().indexOf('Q[') >= 0) {
                            while (value.toString().indexOf('Q[') >= 0) {
                                var inval = value.substring(value.indexOf('Q['), value.substring(value.indexOf('Q[')).indexOf(']Q') + value.indexOf('Q[') + 2);
                                value =
                                    value.replace(
                                        inval
                                        ,
                                        moduals[0][Object.keys(moduals[0])[Object.keys(moduals[0]).indexOf(inval.replace('Q[', '').replace(']Q', ''))]]
                                    )
                            }
                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                    });
                    DashBoard.itemcreate(item, true, moduals, div);

                }
            });

        }



        else {
            if (item.extra) {
                $.each(item, function (index, value) {
                    var c = 0;
                    if (value != undefined && value.toString().indexOf('E[') >= 0) {

                        while (value.toString().indexOf('E[') >= 0) {
                            var inval = value.substring(value.indexOf('E['), value.substring(value.indexOf('E[')).indexOf(']E') + value.indexOf('E[') + 2);
                            value =
                                value.replace(
                                    inval
                                    ,
                                    getExtra(inval.replace('E[', '').replace(']E', ''))
                                );
                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                        item['ex' + Object.keys(item)[Object.keys(item).indexOf(index)]] = getExtra(inval.replace('E[', '').replace(']E', ''));
                    }
                });
            }
            DashBoard.itemcreate(item, true, null, null);
        }
    }
    static additemLayout(item) {
        if (item.hasq != undefined, item.hasq == '1') {
            var div = DashBoard.addtempdiv(item);
            $.ajax({
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: "../api/values/getDashboardItemData?id=" + item.id, data: NPParams, success: function (result) {
                    if (result == ("NL")) {
                        window.location.replace("../Dashboard/Login");
                    }
                    var moduals = JSON.parse(result);
                    //$.each(item, function (index, value) {
                    //    if (value.toString().startsWith('Q[')) {
                    //        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = moduals[0][Object.keys(moduals[0])[Object.keys(moduals[0]).indexOf(value.toString().replace('Q[', '').replace(']Q', ''))]];
                    //    }
                    //});

                    $.each(item, function (index, value) {
                        if (value != undefined && value.toString().indexOf('Q[') >= 0) {
                            while (value.toString().indexOf('Q[') >= 0) {
                                var inval = value.substring(value.indexOf('Q['), value.substring(value.indexOf('Q[')).indexOf(']Q') + value.indexOf('Q[') + 2);
                                value =
                                    value.replace(
                                        inval
                                        ,
                                        moduals[0][Object.keys(moduals[0])[Object.keys(moduals[0]).indexOf(inval.replace('Q[', '').replace(']Q', ''))]]
                                    )
                            }
                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                    });

                    DashBoard.itemcreate(item, false, moduals, div);
                }
            });

        }


        else {
            if (item.extra) {
                $.each(item, function (index, value) {

                    if (value != undefined && value.toString().indexOf('E[') >= 0) {
                        while (value.toString().indexOf('E[') >= 0) {
                            var inval = value.substring(value.indexOf('E['), value.substring(value.indexOf('E[')).indexOf(']E') + value.indexOf('E[') + 2);
                            value =
                                value.replace(
                                    inval
                                    ,
                                    getExtra(inval.replace('E[', '').replace(']E', ''))
                                );
                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                        item['ex' + Object.keys(item)[Object.keys(item).indexOf(index)]] = getExtra(inval.replace('E[', '').replace(']E', ''));
                    }
                });
            }
            DashBoard.itemcreate(item, false, null, null);
        }
    }
    static additemfrom(item, isedit) {

        if (isedit) {
            item.parent = "Edit" + item.parent;
            item.id = "Edit" + item.id;
        }
        if (item.hasq != undefined, item.hasq == '1') {
            var div = DashBoard.addtempdiv(item);
            $.ajax({
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: "../api/values/getDashboardItemData?id=" + item.id, data: NPParams, success: function (result) {
                    if (result == ("NL")) {
                        window.location.replace("../Dashboard/Login");
                    }
                    var moduals = JSON.parse(result);
                    //$.each(item, function (index, value) {
                    //    if (value.toString().startsWith('Q[')) {
                    //        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = moduals[0][Object.keys(moduals[0])[Object.keys(moduals[0]).indexOf(value.toString().replace('Q[', '').replace(']', ''))]];
                    //    }
                    //});
                    $.each(item, function (index, value) {
                        if (value != undefined && value.toString().indexOf('Q[') >= 0) {
                            while (value.toString().indexOf('Q[') >= 0) {
                                var inval = value.substring(value.indexOf('Q['), value.substring(value.indexOf('Q[')).indexOf(']Q') + value.indexOf('Q[') + 2);
                                value =
                                    value.replace(
                                        inval
                                        ,
                                        moduals[0][Object.keys(moduals[0])[Object.keys(moduals[0]).indexOf(inval.replace('Q[', '').replace(']Q', ''))]]
                                    )
                            }
                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                    });
                    DashBoard.itemcreate(item, true, moduals, div);
                }
            });

        }

        else {

            if (item.extra) {
                $.each(item, function (index, value) {

                    if (value != undefined && value.toString().indexOf('E[') >= 0) {
                        while (value.toString().indexOf('E[') >= 0) {

                            while (value.toString().indexOf('P[') >= 0) {
                                var inval = value.substring(value.indexOf('P['), value.substring(value.indexOf('P[')).indexOf(']P') + value.indexOf('P[') + 2);
                                value =
                                    value.replace(
                                        inval
                                        ,
                                        getExtraItem(inval.replace('P[', '').replace(']P', ''), item)
                                    );

                            }
                            var inval = value.substring(value.indexOf('E['), value.substring(value.indexOf('E[')).indexOf(']E') + value.indexOf('E[') + 2);
                            value =
                                value.replace(
                                    inval
                                    ,
                                    getExtra(inval.replace('E[', '').replace(']E', ''))
                                );


                        }
                        item[Object.keys(item)[Object.keys(item).indexOf(index)]] = value;
                        item['ex' + Object.keys(item)[Object.keys(item).indexOf(index)]] = getExtra(inval.replace('E[', '').replace(']E', ''));
                    }
                });
            }

            DashBoard.itemcreate(item, true, null, null, isedit);
        }
    }
    static addchart(item, moduals) {
        switch (item.type) {
            case 'bar':
                DashBoard.addbarchart(item, moduals);
                break;
            case 'horizontalBar':
                DashBoard.addhorizontalBarchart(item, moduals);
                break;
            case 'line':
                DashBoard.addLinechart(item, moduals);
                break;
            case 'pie':
                DashBoard.addpiechart(item, moduals);
                break;
            case 'doughnut':
                DashBoard.adddoughnutchart(item, moduals);
                break;
            case 'polar':
                DashBoard.addpolarchart(item, moduals);
                break;
            case 'radar':
                DashBoard.addradarchart(item, moduals);
                break;
        }
    }
    static addbarchart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'bar', data: { labels: [], datasets: [] }, options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] }, title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;
        var inex = 0;
        //  var colors = item.colors.split('~');
        var cls = (item.colors != undefined) ? JSON.parse(item.colors) : window.chartColors;
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var name = e.name;
            var Lname = e.pname;
            var datasettemp = { label: Lname, data: [], backgroundColor: cls[inex++].toString() };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);
            })
            chartjson.data.datasets.push(datasettemp);

        })


        var chart = new Chart(chartcanvas, chartjson);

    }
    static addhorizontalBarchart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'horizontalBar', data: { labels: [], datasets: [] }, options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] }, title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;
        var inex = 0;
        var cls = (item.colors != undefined) ? JSON.parse(item.colors) : window.chartColors;
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var name = e.name;
            var Lname = e.pname;
            var datasettemp = { label: Lname, data: [], backgroundColor: cls[inex++].toString() };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);

            })
            chartjson.data.datasets.push(datasettemp);

        })

        var chart = new Chart(chartcanvas, chartjson);

    }
    static addLinechart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'line', data: { labels: [], datasets: [] }, options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] }, title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;
        var inex = 0;
        var cls = (item.colors != undefined) ? JSON.parse(item.colors) : window.chartColors;
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var name = e.name;
            var Lname = e.pname;
            var color = cls[inex++].toString();
            var datasettemp = { label: Lname, data: [], borderColor: color, backgroundColor: color, fill: false };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);

            })
            chartjson.data.datasets.push(datasettemp);

        })

        var chart = new Chart(chartcanvas, chartjson);

    }
    static addpiechart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'pie', data: { labels: [], datasets: [], options: { title: { display: true, text: item.title } } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;

        // var cls = JSON.parse(item.colors);
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var inex = 0;
            var name = e.name;
            var Lname = e.pname;
            var datasettemp = { label: Lname, data: [], backgroundColor: [] };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);
                datasettemp.backgroundColor.push(window.chartColors[Object.keys(window.chartColors)[inex++]]);
            })
            chartjson.data.datasets.push(datasettemp);

        })

        var chart = new Chart(chartcanvas, chartjson);

    }
    static adddoughnutchart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'doughnut', data: { labels: [], datasets: [] }, options: { title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;

        //  var cls = JSON.parse(item.colors);
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var inex = 0;
            var name = e.name;
            var Lname = e.pname;
            var datasettemp = { label: Lname, data: [], backgroundColor: [] };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);
                datasettemp.backgroundColor.push(window.chartColors[Object.keys(window.chartColors)[inex++]]);
            })
            chartjson.data.datasets.push(datasettemp);

        })
        if (item.options != undefined) {
            chartjson.options = JSON.parse(item.options);
        }
        var chart = new Chart(chartcanvas, chartjson);

    }
    static addpolarchart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { data: { labels: [], datasets: [] }, options: { title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;

        //  var cls = JSON.parse(item.colors);
        var dts = JSON.parse(item.dataset);
        dts.forEach(function (e) {
            var inex = 0;
            var name = e.name;
            var Lname = e.pname;
            var datasettemp = { label: Lname, data: [], backgroundColor: [] };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);
                datasettemp.backgroundColor.push(window.chartColors[Object.keys(window.chartColors)[inex++]].replace(')', ',0.4)'));
            })
            chartjson.data.datasets.push(datasettemp);

        })
        if (item.options != undefined) {
            chartjson.options = JSON.parse(item.options);
        }
        var chart = Chart.PolarArea(chartcanvas, chartjson);

    }
    static addradarchart(item, moduals) {
        var main = null;
        if (item.parent) {

            main = document.getElementById(item.parent);


        } else {
            main = document.getElementById('MainContent');
        }
        var div = document.createElement("Div");
        div.id = "ChartDiv" + item.id;
        div.className = "chart";
        var chartcanvas = document.createElement("Canvas");
        chartcanvas.id = "chart" + item.id;
        div.appendChild(chartcanvas);
        main.appendChild(div);
        var chartjson = { type: 'radar', data: { labels: [], datasets: [] }, options: { title: { display: true, text: item.title } } };
        moduals.forEach(function (itemm) {
            chartjson.data.labels.push(itemm.lable.toString());
        })
        //  chartjson.data.labels = null;
        var cls = (item.colors != undefined) ? JSON.parse(item.colors) : window.chartColors;
        //  var cls = JSON.parse(item.colors);
        var dts = JSON.parse(item.dataset);
        var inex = 0;
        dts.forEach(function (e) {

            var name = e.name;
            var Lname = e.pname;
            var color = cls[inex++].toString();
            var datasettemp = {
                label: Lname, data: [], backgroundColor: color.replace(')', ',0.4)'), borderColor: color, pointBackgroundColor: color, pointRadius: 6, pointHoverRadius: 12,
            };
            moduals.forEach(function (item) {
                datasettemp.data.push(item[Object.keys(item)[Object.keys(item).indexOf(name)]]);

            })
            chartjson.data.datasets.push(datasettemp);

        })
        if (item.options != undefined) {
            chartjson.options = JSON.parse(item.options);
        }
        var chart = new Chart(chartcanvas, chartjson);

    }
    static addreportgrid(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "24");
        groupdiv.setAttribute("KG-Bag", "");
        if (item.style) {
            groupdiv.style = groupdiv.style = item.style
        }
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var div = document.createElement("div");
        div.style = "height:500px";
        div.className = "ag-theme-balham";
        div.id = ("repgrid" + item.id);
        groupdiv.style.width = item.width + 'px';

        groupdiv.appendChild(lab);
        groupdiv.appendChild(div);
        main.appendChild(groupdiv);

        return groupdiv;
    }
    //__________formcontrols______________//
    static addtextbox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "6");
        groupdiv.setAttribute("KG-Bag", "");
        if (item.style) {
            groupdiv.style = groupdiv.style = item.style
        }
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var inputt = document.createElement("input");
        inputt.className = "form-control form-control-us";
        if (item.placeholder)
            inputt.placeholder = item.placeholder;
        groupdiv.setAttribute("dbid", item.dbsource);
        inputt.setAttribute("KG-INP", "");
        if (item.readonly) {
            inputt.setAttribute("readonly", "");
        }
        if (item.enable) {
            inputt.setAttribute("disabled", "");
        }
        if (item.defualtvalue) {
            inputt.value = item.defualtvalue;
        }
        groupdiv.style.width = item.width + 'px';

        groupdiv.appendChild(lab);
        groupdiv.appendChild(inputt);
        main.appendChild(groupdiv);
        inputt.onchange = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputt.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputt.onfocusout = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };
        return groupdiv;
    }
    static addnumberbox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "17");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var inputt = document.createElement("input");
        inputt.className = "form-control form-control-us";
        if (item.placeholder)
            inputt.placeholder = item.placeholder;
        inputt.type = "number";
        inputt.onkeypress = function (event) { return isNumber(event); };
        groupdiv.setAttribute("dbid", item.dbsource);
        inputt.setAttribute("KG-INP", "");
        inputt.id = "NumberFeild" + item.id;
        if (item.readonly) {
            inputt.setAttribute("readonly", "");
        }
        if (item.disable) {
            inputt.setAttribute("disabled", "");
        }
        if (item.defualtvalue) {
            inputt.value = item.defualtvalue;
        }
        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(inputt);

        main.appendChild(groupdiv);

        inputt.onchange = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputt.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputt.onfocusout = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };
        $('#' + inputt.id).bind('paste', function () {
            var el = this;
            setTimeout(function () {
                el.value = el.value.replace(/\D/g, '');
            }, 0);
        });
        return groupdiv;
    }
    static addnumberdecimalbox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "18");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var inputt = document.createElement("input");
        inputt.className = "form-control form-control-us";
        if (item.placeholder)
            inputt.placeholder = item.placeholder;
        inputt.type = "number";
        inputt.step = "0.1";
        inputt.id = "NumberDecimalFeild" + item.id;
        inputt.onkeypress = function (event) { return isNumberDecimal(event, inputt); };
        groupdiv.setAttribute("dbid", item.dbsource);
        inputt.setAttribute("KG-INP", "");
        if (item.readonly) {
            inputt.setAttribute("readonly", "");
        }
        if (item.enable) {
            inputt.setAttribute("disabled", "");
        }
        if (item.defualtvalue) {
            inputt.value = item.defualtvalue;
        }
        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(inputt);

        main.appendChild(groupdiv);
        inputt.onchange = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputt.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputt.onfocusout = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };
        $('#' + inputt.id).bind('paste', function () {
            var el = this;
            setTimeout(function () {
                el.value = el.value.replace(/[^\d\.]/g, '');
            }, 0);
        });
        return groupdiv;
    }
    static addtextboxMline(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = " form-group-multyline form-group  animated fadeInDown";

        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "6");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.setAttribute("KG-Lab", "");
        lab.innerHTML = item.title;
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var inputt = document.createElement("textarea");
        if (item.rows)
            inputt.setAttribute("rows", item.rows);
        else
            inputt.setAttribute("rows", "3");
        inputt.className = "form-control form-control-us";
        inputt.placeholder = item.placeholder;
        inputt.setAttribute("KG-INP", "");
        groupdiv.setAttribute("dbid", item.dbsource);
        if (item.readonly) {
            inputt.setAttribute("readonly", "");
        }
        if (item.enable) {
            inputt.setAttribute("disabled", "");
        }
        groupdiv.setAttribute("style", "width:" + item.width + "px !important ")

        groupdiv.appendChild(lab);
        groupdiv.appendChild(inputt);




        main.appendChild(groupdiv);

        inputt.onchange = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputt.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputt.onfocusout = function () { DashBoard.TextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };
        return groupdiv;
    }
    static addcombobox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "7");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var selectinput = document.createElement("select");
        selectinput.className = "select 2 form-control form-control-us";
        groupdiv.setAttribute("dbid", item.dbsource);

        selectinput.id = 'select' + item.id;
        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(selectinput);
        main.appendChild(groupdiv);

        //$.ajax({
        //    url: (item.RelatedID == null || item.RelatedID == null || item.RelatedID == 0) ? "../api/values/getDashboardItemData?id=" + item.id.replace("Edit", "") : "../api/values/getDashboardItemData?relatedid=" + ComboBox.getValue(item.RelatedID) + "&relatedname=" + item.RelatedName + "&id=" + item.id.replace("Edit", "") 
        //    , success: function (result) {
        //     if (result == ("NL")) {
        //        window.location.replace("../Dashboard/Login");
        //    }
        //        var moduals = JSON.parse(result);

        //        $('#select' + item.id).select2(
        //    {
        //        placeholder: item.placeholder,
        //        data: moduals,
        //        allowClear: true,
        //    }

        //    );
        //        if (item.defualtvalue) {
        //            $('#select' + item.id).val(item.defualtvalue).trigger('change');
        //        } else {
        //            $('#select' + item.id).val(null).trigger('change');
        //        }
        //        if (item.RelatedID || item.RelatedID == "null") {
        //            $('#select' + item.RelatedID.replace("ComboBox", "")).on('change', function () {


        //                $.ajax({
        //                    url: (item.RelatedID == null || item.RelatedID == null || item.RelatedID == 0) ? "../api/values/getDashboardItemData?id=" + item.id.replace("Edit", "") : "../api/values/getDashboardItemData?relatedid=" + ComboBox.getValue(item.RelatedID.replace("select", "ComboBox")) + "&relatedname=" + item.RelatedName + "&id=" + item.id.replace("Edit", "")
        //                   , success: function (result) {
        //                    if (result == ("NL")) {
        //                       window.location.replace("../Dashboard/Login");
        //                   }
        //                       var moduals = JSON.parse(result);
        //                       $('#select' + item.id).empty();
        //                       $('#select' + item.id).select2('destroy');
        //                       $('#select' + item.id).select2(
        //                       {
        //                           placeholder: item.placeholder,
        //                           data: moduals,
        //                           allowClear: true,
        //                       }

        //                         );
        //                       $('#select' + item.id).val(null).trigger("change");
        //                   }
        //                });



        //            });//ComboBox.reload(this.id.replace("select", "ComboBox"));
        //        }
        //        $('#select' + item.id).on("select2:close", function () { ComboBox.validate(this.id.replace("select", "ComboBox")); });
        //        // selectinput.onfocusout = function () { ComboBox.validate(groupdiv.id); };
        //        // selectinput.onchange = function () { ComboBox.validate(groupdiv.id); };
        //    }
        //});


        if (item.id.toString().indexOf("Edit") >= 0 && item.RelatedID) {
            item.RelatedID = item.RelatedID.replace(item.RelatedIDD, ("Edit" + item.RelatedIDD));
            item.RelatedIDD = "Edit" + item.RelatedIDD;
        }


        if (item.pureid && Purecombodata[item.pureid]) {

            $waitUntil(
                function () { return Purecombodata[item.pureid] !== true },
                function () {
                    var moduals = Purecombodata[item.pureid];
                    $('#select' + item.id).select2(
                        {
                            placeholder: item.placeholder,
                            data: moduals,
                            allowClear: true,
                        });

                    if (item.defualtvalue) {
                        $('#select' + item.id).val(item.defualtvalue).trigger('change');
                    } else {
                        $('#select' + item.id).val(null).trigger('change');
                    }
                    if (item.RelatedID || item.RelatedID == "null") {

                        groupdiv.onmouseenter = function () {
                            Dash[item.RelatedID].dom.className += " bggr ";
                            lab.innerHTML += "<span style=\"font-size:10px;\">" + "  " + " وابسته به " + $(Dash[item.RelatedID].dom).find('[KG-Lab]')[0].innerHTML.replace("<span style=\"Color:red;\"> *</span>", "") + "</span>";
                        }
                        groupdiv.onmouseleave = function () {
                            lab.innerHTML = lab.innerHTML.replace("<span style=\"font-size:10px;\">" + "  " + " وابسته به " + $(Dash[item.RelatedID].dom).find('[KG-Lab]')[0].innerHTML.replace("<span style=\"Color:red;\"> *</span>", "") + "</span>", "");
                            Dash[item.RelatedID].dom.className = Dash[item.RelatedID].dom.className.replace("bggr", "");
                        }


                        $('#select' + item.RelatedIDD.replace("ComboBox", "")).on('change', function () {


                            Dash[item.dbsource + item.id].doEvents("change");

                            $.ajax({
                                beforeSend: function (request) {
                                    request.setRequestHeader("Authorization", Token);
                                },
                                url: (item.RelatedID == null || item.RelatedID == null || item.RelatedID == 0) ? "../api/values/getDashboardItemData?id=" + (item.pureid ? item.pureid : item.id.toString().replace("Edit", "")) : "../api/values/getDashboardItemData?relatedid=" + ((Dash[item.RelatedID].getValue() == undefined || Dash[item.RelatedID].getValue() == "") ? 0 : Dash[item.RelatedID].getValue()) + "&relatedname=" + item.RelatedName + "&id=" + (item.pureid ? item.pureid : item.id.toString().replace("Edit", ""))
                                , data: NPParams, success: function (result) {
                                    if (result == ("NL")) {
                                        window.location.replace("../Dashboard/Login");
                                    }
                                    var moduals = JSON.parse(result);
                                    var val = Dash[item.dbsource + item.id].getLoadedValue();
                                    $('#select' + item.id).empty();
                                    $('#select' + item.id).select2('destroy');
                                    $('#select' + item.id).select2(
                                        {
                                            placeholder: item.placeholder,
                                            data: moduals,
                                            allowClear: true,
                                        }
                                    );

                                    if (!val && (item.id.indexOf("Edit") >= 0))
                                        $('#select' + item.id).val(null).trigger("change");
                                    else
                                        $('#select' + item.id).val(val).trigger("change");



                                    if (item.readonly) {
                                        try {

                                            readonly_select($('#select' + item.id)[0].nextSibling, true);
                                        } catch (el) {

                                        }
                                    }

                                }



                            });



                        });
                    }
                    $('#select' + item.id).on('change', function () {
                        Dash[item.dbsource + item.id].doEvents("change");
                    });
                    $('#select' + item.id).on('click', function () {
                        Dash[item.dbsource + item.id].doEvents("click");
                    });
                    $('#select' + item.id).on("select2:close", function () { Dash[item.dbsource + item.id].validate(); });
                    if (item.readonly) {
                        try {

                            readonly_select($('#select' + item.id)[0].nextSibling, true);
                        } catch (el) {

                        }
                    }
                });

        }
        else {

            if (item.pureid) {
                Purecombodata[item.pureid] = true;
            }

            combowait.push(item.id);

            $.ajax({
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: (item.RelatedID == null || item.RelatedID == null || item.RelatedID == 0) ? "../api/values/getDashboardItemData?id=" + (item.pureid ? item.pureid : item.id.toString().replace("Edit", "")) : "../api/values/getDashboardItemData?relatedid=" + ((!Dash[item.RelatedID].getValue() || Dash[item.RelatedID].getValue() == undefined || Dash[item.RelatedID].getValue() == "") ? 0 : Dash[item.RelatedID.replace("select", "ComboBox")].getValue()) + "&relatedname=" + item.RelatedName + "&id=" + item.id.toString().replace("Edit", "")
                , data: NPParams, success: function (result) {
                    if (result == ("NL")) {
                        window.location.replace("../Dashboard/Login");
                    }
                    var moduals = JSON.parse(result);

                    if (item.pureid) {
                        Purecombodata[item.pureid] = moduals
                    }


                    $('#select' + item.id).select2(
                        {
                            placeholder: item.placeholder,
                            data: moduals,
                            allowClear: true,
                        });
                    var i = combowait.indexOf(item.id);
                    if (i != -1) {
                        combowait.splice(i, 1);
                    }
                    if (item.defualtvalue) {
                        $('#select' + item.id).val(item.defualtvalue).trigger('change');
                    } else {
                        $('#select' + item.id).val(null).trigger('change');
                    }
                    if (item.RelatedID || item.RelatedID == "null") {


                        groupdiv.onmouseenter = function () {
                            Dash[item.RelatedID].dom.className += " bggr ";
                            lab.innerHTML += "<span style=\"font-size:10px;\">" + "  " + " وابسته به " + $(Dash[item.RelatedID].dom).find('[KG-Lab]')[0].innerHTML.replace("<span style=\"Color:red;\"> *</span>", "") + "</span>";
                        }
                        groupdiv.onmouseleave = function () {
                            lab.innerHTML = lab.innerHTML.replace("<span style=\"font-size:10px;\">" + "  " + " وابسته به " + $(Dash[item.RelatedID].dom).find('[KG-Lab]')[0].innerHTML.replace("<span style=\"Color:red;\"> *</span>", "") + "</span>", "");
                            Dash[item.RelatedID].dom.className = Dash[item.RelatedID].dom.className.replace("bggr", "");
                        }



                        $('#select' + item.RelatedIDD.replace("ComboBox", "")).on('change', function () {


                            Dash[item.dbsource + item.id].doEvents("change");

                            $.ajax({
                                beforeSend: function (request) {
                                    request.setRequestHeader("Authorization", Token);
                                },
                                url: (item.RelatedID == null || item.RelatedID == null || item.RelatedID == 0) ? "../api/values/getDashboardItemData?id=" + item.id.toString().replace("Edit", "") : "../api/values/getDashboardItemData?relatedid=" + (Dash[item.RelatedID].getValue() == undefined ? 0 : Dash[item.RelatedID].getValue()) + "&relatedname=" + item.RelatedName + "&id=" + item.id.toString().replace("Edit", "")
                                , data: NPParams, success: function (result) {
                                    if (result == ("NL")) {
                                        window.location.replace("../Dashboard/Login");
                                    }
                                    var moduals = JSON.parse(result);
                                    var val = Dash[item.dbsource + item.id].getLoadedValue();
                                    $('#select' + item.id).empty();
                                    $('#select' + item.id).select2('destroy');
                                    $('#select' + item.id).select2(
                                        {
                                            placeholder: item.placeholder,
                                            data: moduals,
                                            allowClear: true,
                                        }

                                    );

                                    if (!val && val != 0 && (item.id.indexOf("Edit") >= 0))
                                        $('#select' + item.id).val(null).trigger("change");
                                    else
                                        $('#select' + item.id).val(val).trigger("change");



                                    if (item.readonly) {
                                        try {

                                            readonly_select($('#select' + item.id)[0].nextSibling, true);
                                        } catch (el) {

                                        }
                                    }
                                }
                            });



                        });//ComboBox.reload(this.id.replace("select", "ComboBox"));
                    }
                    $('#select' + item.id).on('change', function (el1, el2, el3) {
                        Dash[item.dbsource + item.id].doEvents("change", el1, el2, el3);
                    });
                    $('#select' + item.id).on('click', function () {
                        Dash[item.dbsource + item.id].doEvents("click");
                    });
                    $('#select' + item.id).on("select2:close", function () { Dash[item.dbsource + item.id].validate(); });
                    if (item.readonly) {

                        readonly_select($('#select' + item.id)[0].nextSibling, true);
                    }
                    //DashBoard.ComboBox.validate(item.id.replace("select", "ComboBox"));
                    // selectinput.onfocusout = function () { ComboBox.validate(groupdiv.id); };
                    // selectinput.onchange = function () { ComboBox.validate(groupdiv.id); };
                }
            });
        }
        return groupdiv;
    }
    static adddatepicker(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "8");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var indiv = document.createElement("div");
        indiv.className = "input-group form-control-us";

        var inputtext = document.createElement("input");
        inputtext.id = 'DatePickerText' + item.id;
        inputtext.className = "form-control";
        inputtext.placeholder = item.placeholder;
        if (!item.readonly) {
            inputtext.setAttribute("data-mddatetimepicker", "true");
            inputtext.setAttribute("data-trigger", "click");
            inputtext.setAttribute("data-targetselector", "#" + inputtext.id);
            inputtext.setAttribute("data-groupid", "group1");
            inputtext.setAttribute("data-enabletimepicker", "false");
            inputtext.setAttribute("data-placement", "bottom");
            inputtext.setAttribute("data-mdpersiandatetimepicker", "");
            if (item.formatdate) {
                inputtext.setAttribute("data-mdformat", item.formatdate);
            }
            else {
                inputtext.setAttribute("data-mdformat", "yyyy/MM/dd");
            }
            if (item.gregorian) {
                inputtext.setAttribute("data-isgregorian", "true");
                inputtext.setAttribute("data-englishnumber", "true");
                inputtext.style = " direction: ltr;text-align: end;";
            }
            if (item.timepicker) {//
                inputtext.setAttribute("data-enabletimepicker", "true");
            }
        }

        inputtext.setAttribute("type", "text");
        inputtext.setAttribute("KG-INP", "");
        groupdiv.setAttribute("dbid", item.dbsource);


        var indivbtn = document.createElement("div");
        indivbtn.className = "input-group-addon";

        inputtext.className = "form-control";
        indivbtn.placeholder = item.placeholder;
        if (!item.readonly) {
            indivbtn.setAttribute("data-mddatetimepicker", "true");
            indivbtn.setAttribute("data-trigger", "click");
            indivbtn.setAttribute("data-targetselector", "#" + inputtext.id);
            indivbtn.setAttribute("data-groupid", "group1");
            indivbtn.setAttribute("data-enabletimepicker", "false");
            indivbtn.setAttribute("data-placement", "bottom");
            indivbtn.setAttribute("data-mdpersiandatetimepicker", "");
            if (item.formatdate) {
                indivbtn.setAttribute("data-mdformat", item.formatdate);
            }
            else {
                indivbtn.setAttribute("data-mdformat", "yyyy/MM/dd");
            }
            indivbtn.setAttribute("data-mdpersiandatetimepickershowing", "false");
            if (item.gregorian) {
                indivbtn.setAttribute("data-isgregorian", "true");
                inputtext.setAttribute("data-englishnumber", "true");
                inputtext.style = " direction: ltr;text-align: end;";
            }
            if (item.timepicker) {//
                inputtext.setAttribute("data-enabletimepicker", "true");
            }
        }
        indivbtn.setAttribute("style", "border:1px solid #d2d6de !important");
        if (item.readonly) {
            inputtext.setAttribute("readonly", "");
            inputtext.className = "form-control disabled-field";
            indivbtn.setAttribute("readonly", "");
            indivbtn.className = "input-group-addon disabled-field";

        }
        if (item.enable) {
            inputtext.setAttribute("disabled", "");
            inputtext.className = "form-control disabled-field";
            indivbtn.setAttribute("disabled", "");
            indivbtn.className = "input-group-addon disabled-field";
        }
        if (item.defualtvalue) {
            inputtext.value = item.defualtvalue;
        }

        var span = document.createElement("span");
        span.className = "glyphicon glyphicon-calendar";

        indivbtn.appendChild(span);
        indiv.appendChild(inputtext);
        indiv.appendChild(indivbtn);

        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(indiv);
        main.appendChild(groupdiv);

        // var $this = $('#'+ inputtext.id)[0],
        //trigger = $this.attr('data-trigger'),
        //placement = $this.attr('data-Placement'),
        //enableTimePicker = $this.attr('data-enabletimepicker'),
        //targetSelector = $this.attr('data-targetselector'),
        //groupId = $this.attr('data-groupid'),
        //toDate = $this.attr('data-todate'),
        //fromDate = $this.attr('data-fromdate'),
        //disableBeforeToday = $this.attr('data-disablebeforetoday'),
        //englishNumber = $this.attr('data-englishnumber'),
        //disable = $this.attr('data-disabled') != undefined && $this.attr('data-disabled').toLowerCase() == 'true',
        //format = $this.attr('data-mdformat'),
        //gregorianStartDayIndex = $this.attr('data-gregorianstartdayindex') == undefined ? 0 : Number($this.attr('data-gregorianstartdayindex')),
        //isGregorian = $this.attr('data-isgregorian') == 'true';
        // if (!isNumber(gregorianStartDayIndex)) gregorianStartDayIndex = 0;
        // if (!$this.is(':input') && $this.css('cursor') == 'auto')
        //     $this.css({ cursor: 'pointer' });
        // var settings = {
        //     Placement: placement,
        //     EnglishNumber: englishNumber == 'true',
        //     Trigger: trigger == undefined || trigger == '' ? 'click' : trigger,
        //     EnableTimePicker: enableTimePicker == 'true',
        //     TargetSelector: targetSelector != undefined ? targetSelector : '',
        //     GroupId: groupId != undefined ? groupId : '',
        //     ToDate: toDate != undefined ? toDate : '',
        //     FromDate: fromDate != undefined ? fromDate : '',
        //     DisableBeforeToday: disableBeforeToday == 'true',
        //     Disabled: disable,
        //     Format: format == undefined || format == '' ? getDefaultFormat(enableTimePicker == 'true') : format,
        //     GregorianStartDayIndex: gregorianStartDayIndex,
        //     IsGregorian: isGregorian
        // };
        // $this.MdPersianDateTimePicker(settings);

        inputtext.onchange = function () { DashBoard.DatePicker.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputtext.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputtext.onfocusout = function () { DashBoard.DatePicker.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };


        return groupdiv;
    }
    static addtimepicker(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "9");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.setAttribute("KG-Lab", "");
        lab.innerHTML = item.title;
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var indiv = document.createElement("div");
        indiv.className = "input-group clockpicker form-control-us";
        indiv.id = 'TimePickerText' + item.id;

        var inputtext = document.createElement("input");
        inputtext.className = "form-control";
        inputtext.placeholder = item.placeholder;
        inputtext.setAttribute("type", "text");
        inputtext.setAttribute("KG-INP", "");
        groupdiv.setAttribute("dbid", item.dbsource);


        var span2 = document.createElement("span");
        span2.className = "input-group-addon";
        span2.setAttribute("style", "border:1px solid #d2d6de !important");
        if (item.enable) {
            inputtext.setAttribute("disabled", "");
            inputtext.className = "form-control disabled-field";
            span2.setAttribute("disabled", "");
            span2.className = "input-group-addon disabled-field";
        }
        if (item.readonly) {
            inputtext.setAttribute("readonly", "");
        }
        if (item.defualtvalue) {
            inputtext.value = item.defualtvalue;
        }
        var span = document.createElement("span");
        span.className = "glyphicon glyphicon-time";

        span2.appendChild(span);
        indiv.appendChild(inputtext);
        indiv.appendChild(span2);

        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(indiv);
        main.appendChild(groupdiv);
        if (item.readonly || item.enable) {

        }
        else {
            $('#TimePickerText' + item.id).clockpicker();
        }

        inputtext.onchange = function () { DashBoard.TimePicker.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputtext.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputtext.onfocusout = function () { DashBoard.TimePicker.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };

        return groupdiv;
    }
    static addcheckbox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown ";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "10");
        groupdiv.setAttribute("KG-Bag", "");

        var indiv = document.createElement("div");
        indiv.className = "input-group form-control-us";

        var inputtext = document.createElement("div");
        inputtext.className = "checkbox ";
        inputtext.setAttribute("style", "width:100%");
        groupdiv.setAttribute("dbid", item.dbsource);
        inputtext.placeholder = item.placeholder;
        // inputtext.setAttribute("type","text");

        var lab = document.createElement("label");
        lab.className = " label-us ";
        lab.setAttribute("KG-Lab", "");
        var input2 = document.createElement("input");
        input2.type = "checkbox";
        input2.className = " ich ";
        input2.setAttribute("style", "margin-left: 10PX;top: 2px;margin-right: 9px;position: inherit;");
        input2.setAttribute("KG-INP", "");
        lab.appendChild(input2);
        lab.innerHTML += item.title;

        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }

        inputtext.appendChild(lab);
        //  var sp = document.createElement("span");
        //  sp.className = "checkmark"
        //   inputtext.appendChild(sp);
        indiv.appendChild(inputtext);


        groupdiv.style.width = item.width + 'px';

        groupdiv.appendChild(indiv);
        main.appendChild(groupdiv);

       
        $(lab).on("click", function (event) {
          
            Dash[item.dbsource + item.id].doEvents("change");
        });
       

     

        return groupdiv;
    }
    static addbutton(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        var button = document.createElement("button");
        button.className = " btn btn-info pull-right  ";
        button.innerText = item.title;
        if (item.style) {
            button.style = item.style;
        }
        if (item.gpstyle) {
            groupdiv.style = item.gpstyle;
        }
        if (item.targetid)
            button.setAttribute("targetGridid", item.targetid);
        button.id = item.dbsource + item.id;
        var flowid = '0';
        if (item.flowid) {
            flowid = item.flowid;
        }
        if (item.handler) {
            var fn =
                button.onclick = new Function(item.handler);
        } else {
            switch (item.type) {
                case "1":
                    button.onclick = function (item) {

                        var ids = item.target.parentElement.parentElement.id.replace("-footer", "");
                        console.log(ids);
                        if (Dash["Form" + ids].Validation()) {
                            $("#" + ids).preloader({ text: "لطفا کمی صبر کنید.." });

                            $.ajax({
                                type: 'POST',
                                beforeSend: function (request) {
                                    request.setRequestHeader("Authorization", Token);
                                },
                                data: {
                                    DBID: ids.replace("MainLayout", "").replace("Edit", ""),
                                    values: JSON.stringify(Dash["Form" + ids].getValues()),
                                    Flow: flowid,
                                    targetid: Dash["Form" + ids].targetid,
                                },
                                url: "../api/values/SaveDataDashBoard",
                                success: function (dataa, status) {
                                    // var data = JSON.parse(dataa);
                                    var data = dataa;
                                    if (status == 'success') {
                                        if (data.success == true) {
                                            if (data.message) {
                                                if (data.isok)
                                                    notify({ "title": "عملیات موفق", "message": data.message });
                                                else
                                                    showmodal('عملیات موفق', data.message, 2);
                                            }
                                            else
                                                showmodal('عملیات موفق', 'ثبت اطلاعات با موفقیت انجام شد', 1);


                                            if (document.getElementById(ids).id.indexOf("Edit") < 0) {
                                                Dash["Form" + ids].resetValues();
                                                // DashBoard.Form.resetValues(ids);//
                                                if (item.target.getAttribute("targetGridid"))
                                                    Dash[item.target.getAttribute("targetGridid")].reload();
                                            }
                                            else {
                                                if (item.target.getAttribute("targetGridid"))
                                                    if (Dash[item.target.getAttribute("targetGridid")])
                                                        Dash[item.target.getAttribute("targetGridid")].reload();

                                                DashBoard.Form.reloadinnerTable(ids);
                                            }
                                        } else {
                                            showmodal('عملیات ناموفق', data.message, 3);
                                        }
                                    } else {
                                        showmodal('عملیات ناموفق', 'عملیات با اشکال مواجه شد', 3);
                                    }
                                    $("#" + ids).preloader('remove');
                                }
                                , error: function (dataa, status) {

                                    showmodal('عملیات ناموفق', 'عملیات با اشکال مواجه شد', 3);

                                    $("#" + ids).preloader('remove');
                                }
                            });
                        } else {
                            notify({ "title": "خطا", "message": "ورودی ها کامل نیست", type: 2 });
                        }
                    }
                    break;
                case "2":
                    button.onclick = function (item) {
                        debugger;
                        var ids = item.target.parentElement.id.replace("-footer", "");
                        var dady = document.getElementById(ids);
                        if (Dash["Form" + ids].Validation()) {
                            if (dady.getAttribute("datamode") != undefined && dady.getAttribute("datamode") == "edit") {
                                $("#" + ids).preloader({ text: "لطفا کمی صبر کنید.." });

                                var values = Dash['Form' + ids].getFieldValues(2)

                                var table = $("#" + document.getElementById(ids).getAttribute("targettableid")).DataTable();

                                table.row(document.getElementById(thisrow[thisrow.length - 1])).data(values).draw();
                                showmodal('عملیات موفق', 'ثبت اطلاعات با موفقیت انجام شد', 1);
                                $("#" + ids).preloader('remove');
                            } else {
                                $("#" + ids).preloader({ text: "لطفا کمی صبر کنید.." });
                                var values = Dash['Form' + ids].getFieldValues(1);//DashBoard.Form.getFieldValues(ids, 1);
                                var table = $("#" + document.getElementById(ids).getAttribute("targettableid")).DataTable();
                                table.row.add(values).draw(false);
                                showmodal('عملیات موفق', 'ثبت اطلاعات با موفقیت انجام شد', 1);
                                $("#" + ids).preloader('remove');
                                Dash["Form" + ids].resetValues();
                                // DashBoard.Form.resetValues(ids);
                            }
                        } else {
                            notify({ "title": "خطا", "message": "ورودی ها کامل نیست", type: 2 });
                        }

                    }
                    break;
            }
        }
        groupdiv.appendChild(button)
        main.appendChild(groupdiv);
        return button;
    }
    static addmasktextbox(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown ";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "12");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.className = "label-us";
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var indiv = document.createElement("div");
        indiv.className = "input-group clockpicker form-control-us";
        indiv.id = 'MaskTextBoxinner' + item.id;

        var inputtext = document.createElement("input");
        inputtext.className = "form-control";
        inputtext.id = 'MaskTextBoxInput' + item.id;
        inputtext.placeholder = item.placeholder;
        inputtext.setAttribute("type", "text");
        //inputtext.setAttribute("data-mask", "");
        //inputtext.setAttribute("data-inputmask", item.mask);
        inputtext.setAttribute("style", "  text-align: right; direction: ltr !important;");
        inputtext.setAttribute("KG-INP", "");
        groupdiv.setAttribute("dbid", item.dbsource);



        var span2 = document.createElement("span");
        span2.className = "input-group-addon";
        span2.setAttribute("style", "border:1px solid #d2d6de !important");

        var span = document.createElement("span");
        span.className = item.icon;

        span2.appendChild(span);
        indiv.appendChild(inputtext);
        if (item.icon) {
            indiv.appendChild(span2);
        }

        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(indiv);
        main.appendChild(groupdiv);
        if (item.incomplete) {
            $('#MaskTextBoxInput' + item.id).inputmask(item.mask, { "clearIncomplete": true });
        } else {
            $('#MaskTextBoxInput' + item.id).inputmask(item.mask, { clearMaskOnLostFocus: true });
        }



        inputtext.onchange = function () { DashBoard.MaskTextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("change"); };
        inputtext.onclick = function () { Dash[item.dbsource + item.id].doEvents("click") };
        inputtext.onfocusout = function () { DashBoard.MaskTextBox.validate(groupdiv.id); Dash[item.dbsource + item.id].doEvents("focusout"); };

        return groupdiv;

    }
    static addgrid(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }

        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated zoomInUp  ";
        groupdiv.id = 'TableGrid' + item.id;
        groupdiv.setAttribute("UsType", "13");
        groupdiv.setAttribute("style", "max-width:100% !important;direction: rtl;width:100%;width: -moz-available;");
        var tbl = document.createElement("table");
        tbl.id = "Grid" + item.id;
        tbl.className = "display responsive nowrap";
        tbl.style.width = "100%";
        tbl.setAttribute("cellspacing", "0");
        tbl.setAttribute("sourcePID", "0");
        tbl.setAttribute("formid", item.formid);

        tbl.innerHTML = " <thead><tr></tr></thead><tfoot><tr></tr></tfoot>";
        groupdiv.appendChild(tbl);

        if (item.masterid) {
            main = document.getElementById("MainLayout" + item.masterid + "-" + groupdiv.id.substring(groupdiv.id.split("-", 2).join("-").length + 1));
            var lii = document.createElement("li");
            lii.className = "active";
            lii.innerHTML = "<a href=\"#" + item.id + "Tab\" data-toggle=\"tab\" aria-expanded=\"true\">" + item.title + "</a>";
            main.children[1].children[0].children[0].insertBefore(lii, main.children[1].children[0].children[0].firstChild);
            var dd = document.createElement("div");
            dd.className = "tab-pane active";
            dd.id = item.id + "Tab";
            dd.setAttribute("style", "position: relative; padding-top: 10px;");
            dd.appendChild(groupdiv);
            main.children[1].children[0].children[1].appendChild(dd);
            main.children[1].setAttribute("style", "margin-top: 20px;");
        }
        else {
            main.appendChild(groupdiv);
        }
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getFormDataComboData?id=" + item.formid,
            data: NPParams,
            success: function (result) {
                var bigmoduals = JSON.parse(result);
                $.ajax({
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    },
                    url: "../api/values/getGridHeaders?id=" + item.formid, success: function (result) {
                        if (result == ("NL")) {
                            window.location.replace("../Dashboard/Login");
                        }
                        var moduals = JSON.parse(result);

                        var table = document.getElementById('Grid' + item.id);
                        // "<input class=\"\" placeholder=\"\" aria-controls=\"Grid81\" type=\"search\">"
                        var th0 = document.createElement("th");
                        var th1 = document.createElement("th");
                        th1.innerHTML = "شماره";
                        table.children[0].children[0].appendChild(th0);
                        table.children[0].children[0].appendChild(th1);
                        var th20 = document.createElement("th");
                        var th21 = document.createElement("th");
                        th21.innerHTML = "شماره";
                        table.children[1].children[0].appendChild(th20);
                        table.children[1].children[0].appendChild(th21);
                        var formid = item.formid;

                        moduals.forEach(function (item) {
                            if (item.pval2) {
                                var th = document.createElement("th");
                                th.className = " unselectable";
                                th.innerHTML = item.pval2;
                                th.setAttribute("typeid", item.itemtypeid);
                                table.children[0].children[0].appendChild(th);
                                var th2 = document.createElement("th");
                                th2.innerHTML = item.pval2;
                                th2.className = " unselectable";
                                table.children[1].children[0].appendChild(th2);
                            }
                        });

                        var th = document.createElement("th");
                        th.className = "center unselectable";
                        th.innerHTML = "ویرایش و حذف";
                        table.children[0].children[0].appendChild(th);
                        var th2 = document.createElement("th");
                        th2.className = "center unselectable";
                        th2.innerHTML = "ویرایش و حذف";
                        table.children[1].children[0].appendChild(th2);

                        getTabledata2(table.id, formid, moduals, bigmoduals, item);
                        // gettableview();
                        // selectinput.onfocusout = function () { ComboBox.validate(groupdiv.id); };
                        // selectinput.onchange = function () { ComboBox.validate(groupdiv.id); };
                    }
                });
            }
        });
        return groupdiv;
    }
    static adddetailgrid(item, isineditmode) {
        console.log(item);
        var main = null;

        if (item.parent) {
            main = document.getElementById(item.parent.replace("-body", ""));
        } else {
            main = document.getElementById('MainContent');
        }

        if (main == null) {
            main = document.getElementById(item.parent + "-body");
        }

        //if (isEditMode) {
        //    item.id = item.id + "EditMode";
        //}

        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated zoomInUp ";
        groupdiv.id = 'TableDetailsGrid' + item.id;
        groupdiv.setAttribute("UsType", "14");
        groupdiv.setAttribute("style", "max-width:100% !important;direction: rtl;width:100%;width: -moz-available;");
        var tbl = document.createElement("table");
        tbl.id = "DetailsGrid" + item.id;
        tbl.className = "display responsive nowrap";
        tbl.style.width = "100%";
        tbl.setAttribute("cellspacing", "0");
        tbl.setAttribute("sourcePID", "0");
        tbl.setAttribute("formid", item.formid);







        tbl.innerHTML = " <thead><tr></tr></thead><tfoot><tr></tr></tfoot>";
        groupdiv.appendChild(tbl);

        if (item.masterid) {
            main = document.getElementById("MainLayout" + item.masterid);

            if (main == null) {
                main = document.getElementById("EditMainLayout" + item.masterid);
            }
            var lii = document.createElement("li");

            lii.innerHTML = "<a href=\"#" + item.id + "Tab\" data-toggle=\"tab\" aria-expanded=\"true\">" + item.title + "</a>";
            var bool = false;
            if (main && main.children[1].children[0].children[0].children.length == 1) {
                lii.className = " active ";
                bool = true;
            }
            if (main)
                main.children[1].children[0].children[0].insertBefore(lii, main.children[1].children[0].children[0].lastChild);





            var dd = document.createElement("div");
            if (bool)
                dd.className = "tab-pane active ";
            else
                dd.className = "tab-pane ";
            dd.id = item.id + "Tab";
            dd.setAttribute("type", "grid");
            dd.setAttribute("style", "position: relative; padding-top: 10px;");
            dd.appendChild(groupdiv);
            if (main) {

                main.children[1].children[0].children[1].appendChild(dd);
                main.children[1].setAttribute("style", "margin-top: 20px;");
            }

        }
        else {

            main.appendChild(groupdiv);
        }



        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getFormDataComboData?id=" + item.formid, data: NPParams, success: function (result) {
                var bigmoduals = JSON.parse(result);
                // combodata = moduals;
                $.ajax({
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    },
                    url: "../api/values/getGridHeaders?id=" + item.formid, success: function (result) {
                        //if (result == ("NL")) {
                        //    window.location.replace("../Dashboard/Login");
                        //}
                        //var moduals = JSON.parse(result);

                        //var table = document.getElementById('Grid' + item.id);
                        //var th0 = document.createElement("th");

                        //table.children[0].children[0].appendChild(th0);
                        //var th20 = document.createElement("th");

                        //table.children[1].children[0].appendChild(th20);



                        //var th01 = document.createElement("th");
                        //th01.innerHTML = "rowst";
                        //table.children[0].children[0].appendChild(th01);
                        //var th201 = document.createElement("th");
                        //th201.innerHTML = "rowst";
                        //table.children[1].children[0].appendChild(th201);

                        //var th01 = document.createElement("th");
                        //th01.innerHTML = "ddata";
                        //table.children[0].children[0].appendChild(th01);
                        //var th201 = document.createElement("th");
                        //th201.innerHTML = "ddata";
                        //table.children[1].children[0].appendChild(th201);



                        //var formid = item.formid;
                        //moduals.head.forEach(function (item) {
                        //    var th = document.createElement("th");
                        //    th.innerHTML = item.pval;
                        //    table.children[0].children[0].appendChild(th);
                        //    var th2 = document.createElement("th");
                        //    th2.innerHTML = item.pval;
                        //    table.children[1].children[0].appendChild(th2);
                        //});

                        //var th = document.createElement("th");
                        //th.innerHTML = "ویرایش و حذف";
                        //table.children[0].children[0].appendChild(th);
                        //var th2 = document.createElement("th");
                        //th2.innerHTML = "ویرایش و حذف";
                        //table.children[1].children[0].appendChild(th2);


                        if (result == ("NL")) {
                            window.location.replace("../Dashboard/Login");
                        }
                        var moduals = JSON.parse(result);

                        var table = document.getElementById('DetailsGrid' + item.id);
                        // "<input class=\"\" placeholder=\"\" aria-controls=\"Grid81\" type=\"search\">"
                        var th0 = document.createElement("th");
                        var th1 = document.createElement("th");
                        th1.innerHTML = "شماره";
                        table.children[0].children[0].appendChild(th0);
                        table.children[0].children[0].appendChild(th1);
                        var th20 = document.createElement("th");
                        var th21 = document.createElement("th");
                        th21.innerHTML = "شماره";
                        table.children[1].children[0].appendChild(th20);
                        table.children[1].children[0].appendChild(th21);
                        var formid = item.formid;

                        moduals.forEach(function (item) {
                            if (item.pval2) {
                                var th = document.createElement("th");
                                th.className = " unselectable";
                                th.innerHTML = item.pval2;
                                th.setAttribute("typeid", item.itemtypeid);
                                table.children[0].children[0].appendChild(th);
                                var th2 = document.createElement("th");
                                th2.innerHTML = item.pval2;
                                th2.className = " unselectable";
                                table.children[1].children[0].appendChild(th2);
                            }
                        });

                        var th = document.createElement("th");
                        th.className = "center unselectable";
                        th.innerHTML = "ویرایش و حذف";
                        table.children[0].children[0].appendChild(th);
                        var th2 = document.createElement("th");
                        th2.className = "center unselectable";
                        th2.innerHTML = "ویرایش و حذف";
                        table.children[1].children[0].appendChild(th2);




                        getTabledata22(table.id, formid, moduals, bigmoduals, item, isineditmode);
                        // gettableview();
                        // selectinput.onfocusout = function () { ComboBox.validate(groupdiv.id); };
                        // selectinput.onchange = function () { ComboBox.validate(groupdiv.id); };
                    }
                });
            }
        });

        return groupdiv;


    }
    static addradio(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "15");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        //div class="input-group clockpicker form-control-us" id="TimePickerText13-11-2-2">

        var innerdiv = document.createElement("Div");
        innerdiv.className = "input-group form-control-us";
        innerdiv.id = 'Radiogroup' + item.id;

        var jj = JSON.parse(item.btns);
        jj.forEach(function (item2) {
            var input = document.createElement("Lable");
            input.className = "radio-inline";
            input.setAttribute("style", " direction: rtl; margin-right: 20px; ");
            input.innerHTML = "<input class=\"ich\" style=\"margin-left: 5px !important;position: unset !important;\" value=\"" + item2.val + "\" type=\"radio\" name=\"Radioname" + item.id + "\">" + item2.name;
            innerdiv.appendChild(input);
        });



        //var inputt = document.createElement("input");
        //inputt.className = "form-control form-control-us";
        //inputt.placeholder = item.placeholder;
        groupdiv.setAttribute("dbid", item.dbsource);
        //if (item.readonly) {
        //    inputt.setAttribute("readonly", "");
        //}
        //if (item.enable) {
        //    inputt.setAttribute("disabled", "");
        //}
        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(innerdiv);




        main.appendChild(groupdiv);
        //inputt.onchange = function () { TextBox.validate(groupdiv.id); };
        //inputt.onfocusout = function () { TextBox.validate(groupdiv.id); };

    }
    static addkhob(item) {
        //
    }
    static addfileupload(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        groupdiv.setAttribute("UsType", "20");
        groupdiv.setAttribute("KG-Bag", "");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        groupdiv.appendChild(lab);
        var form = document.createElement("form");
        form.id = "uform" + item.id;

        form.setAttribute("method", "POST");
        form.setAttribute("enctype", "multipart/form-data");

        var ch1 = document.createElement("div");
        ch1.className = "row fileupload-buttonbar";


        var ch11 = document.createElement("div");
        ch11.className = "col-lg-7";
        ch11.innerHTML = " <span class=\"btn btn-success fileinput-button\"> <i class=\"glyphicon glyphicon-plus\"></i>  <span>افزودن فایل</span><input type=\"file\" name=\"files[]\" multiple> </span><span class=\"fileupload-process\"></span>";

        var ch2 = document.createElement("div");
        ch2.className = "col-lg-5 fileupload-progress fade";

        var ch12 = document.createElement("div");
        ch12.className = "progress progress-striped active";
        ch12.setAttribute("role", "progressbar");
        ch12.setAttribute("aria-valuemin", "0");
        ch12.setAttribute("aria-valuemax", "100");

        var ch121 = document.createElement("div");
        ch121.className = "progress-bar progress-bar-success";
        ch121.setAttribute("style", "width:0%;");

        var ch13 = document.createElement("div");
        ch13.className = "progress-extended";
        ch13.innerHTML = "&nbsp;";
        ch13.innerText = "&nbsp;";

        ch12.appendChild(ch121);

        ch2.appendChild(ch12);
        ch2.appendChild(ch13);

        ch1.appendChild(ch11);
        ch1.appendChild(ch2);


        var tbl = document.createElement("table");
        tbl.className = "table table-striped";
        tbl.setAttribute("role", "presentation");

        var tbl2 = document.createElement("table");
        tbl2.className = "files";
        //var input = document.createElement("input");
        //input.type = "hidden";
        //input.name = "parid";
        //input.value = item.id;
        tbl.appendChild(tbl2);
        form.appendChild(ch1);
        form.appendChild(tbl);
        //  form.appendChild(input);
        groupdiv.appendChild(form);
        main.appendChild(groupdiv);




        return groupdiv;
    }
    static addcustomhtml(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = "CHML" + item.id;
        groupdiv.setAttribute("UsType", "21");
        var lab = document.createElement("label");
        lab.className = "label-us";
        lab.innerHTML = item.title;
        lab.setAttribute("KG-Lab", "");
        if (item.forced) {
            lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
            groupdiv.setAttribute("forced", "1");
        }
        var html = document.createElement("div");
        html.innerHTML = item.html;
        html.setAttribute("KG-HTML", "");
        html.style = "max-height: 400px;overflow: scroll;max-width: 99% !important;";



        groupdiv.style.width = item.width + 'px';
        groupdiv.appendChild(lab);
        groupdiv.appendChild(html);
        main.appendChild(groupdiv);
        return groupdiv;
    }
    static addnotready(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = item.dbsource + item.id;
        if (item.nttext) {
            groupdiv.innerHTML = "<span>" + item.nttext + "</span>"
        } else {
            groupdiv.innerHTML = "<span>هنوز آماده نیست ..</span>"
        }

        if (item.style) {
            groupdiv.style = item.style;
        }
        main.appendChild(groupdiv);
        return groupdiv;
    }
    static addtablewiz(item) {
        var main = null;
        if (item.parent) {
            main = document.getElementById(item.parent);
        } else {
            main = document.getElementById('MainContent');
        }
        var groupdiv = document.createElement("div");
        groupdiv.className = "form-group animated fadeInDown";
        groupdiv.id = "TBLWIZ" + item.id;
        groupdiv.style.direction = "ltr";
        groupdiv.style.cssText += 'max-Width:100% !important';
        groupdiv.setAttribute("UsType", "23");
        groupdiv.setAttribute("KG-Bag", "");
        groupdiv.setAttribute("dbid", item.dbsource);
        if (item.style) {
            groupdiv.style = groupdiv.style = item.style
        }
        var fulldiv = document.createElement("div");
        fulldiv.id = "wiz-" + item.id;
        var uls = document.createElement("ul");
        uls.setAttribute("KG-ULF", "");
        var uldivs = document.createElement("div");
        uldivs.setAttribute("KG-ULDF", "");
        var meta = createmetaforwiz(item.meta);
       
        meta.groups.forEach(function (el) {
            var li = document.createElement("li");
            li.innerHTML = '<a href="#DID-' + el.value + '">' + el.Name + '<br /><small>' + el.DES + '</small></a>';
            li.setAttribute("KG-ID", el.value);
            var idv = document.createElement("div");
            idv.style = "margin-left:50px";
            idv.id = 'DID-' + el.value;
            uls.appendChild(li);
            uldivs.appendChild(idv);
        });
        fulldiv.appendChild(uls);
        fulldiv.appendChild(uldivs);
        if (item.width)
            groupdiv.style.width = item.width + 'px';
        else
            groupdiv.style.width = '100%';

        groupdiv.appendChild(fulldiv);
        main.appendChild(groupdiv);



        var innerformmeta = Extra[meta.formdata][0];
        innerformmeta.id = innerformmeta.DashBoardLayoutID;
        meta.groups.forEach(function (el) {
            var innerels = el.FieldValues;
            var fatherdiv = document.getElementById('DID-' + el.value);
            var innerfulldiv = document.createElement("div");
            innerfulldiv.id = "inner-wiz-" + el.value + item.id;
            var inneruls = document.createElement("ul");
            inneruls.className += "animated fadeInDown";
            var inneruldivs = document.createElement("div");
            inneruls.setAttribute("KG-ULS", "");
            inneruldivs.setAttribute("KG-ULDS", "");
            innerels.forEach(function (elm) {
                var ids = elm.VAL.toString();
                while (ids.indexOf(" ") >= 0) {
                    ids = ids.replace(" ", "");
                }
                while (ids.indexOf("/") >= 0) {
                    ids = ids.replace("/", "");
                }
               
                var innerli = document.createElement("li");
                innerli.innerHTML = '<a href="#DID-' + ids + '-' + el.value + '">' + elm.Name + '<br /><small>' + elm.DES + '</small></a>';
                innerli.setAttribute("KG-ID", ids + '-' + el.value);
                var inneridv = document.createElement("div");
                inneridv.id = 'DID-' + ids + '-' + el.value;
                inneridv.setAttribute("KGNAME", elm.Name);
                inneridv.setAttribute("KG-FormID", "Form" + innerformmeta.id + "wiz-" + ids + el.value);
                var ddv = document.createElement("div");
                ddv.id = 'MainLayout' + innerformmeta.id + "wiz-" + ids + el.value;
                ddv.className = "KGINNERFormC";
                inneridv.appendChild(ddv);

                inneruls.appendChild(innerli);
                inneruldivs.appendChild(inneridv);
            });

            innerfulldiv.appendChild(inneruls);
            innerfulldiv.appendChild(inneruldivs);
            fatherdiv.appendChild(innerfulldiv);


            innerels.forEach(function (elm) {
                var innermeta = JSON.parse(JSON.stringify(Extra[meta.formitems]));
                var ids = elm.VAL.toString();
                while (ids.indexOf(" ") >= 0) {
                    ids = ids.replace(" ", "");
                }
                while (ids.indexOf("/") >= 0) {
                    ids = ids.replace("/", "");
                }
                var inneridv = document.getElementById('DID-' + ids + '-' + el.value);

                console.log("Form" + innerformmeta.id + "wiz-" + ids + el.value);

                Dash["Form" + innerformmeta.id + "wiz-" + ids + el.value] = new FormControl(innerformmeta.id + "wiz-" + ids + el.value, innerformmeta.classname, innerformmeta.id + "wiz-" + ids + el.value, innerformmeta.isbox, innerformmeta.boxtype, innerformmeta.title, innerformmeta.hasheader, innerformmeta.hasfooter, innerformmeta.onparentfooter, innerformmeta.icn, innerformmeta.isform, null, innerformmeta.IDProperty, null, null, item.readonly);
                Dash["Form" + innerformmeta.id + "wiz-" + ids + el.value].Customload(innermeta, innerformmeta, el, ids, elm);
            });

            $(innerfulldiv).smartWizard({
                theme: 'dots', transitionEffect: 'fade', lang: {
                    next: 'بعدی',
                    previous: 'قبلی'
                }, toolbarSettings: { toolbarPosition: 'none' }, anchorSettings: { enableAllAnchors: true }
            });
            startlife();
        });



        $(fulldiv).smartWizard({
            theme: 'circles', transitionEffect: 'slide', lang: {
                next: 'بعدی',
                previous: 'قبلی'
            }, toolbarSettings: {
                toolbarPosition: 'bottom', // none, top, bottom, both
                toolbarButtonPosition: 'right', // left, right
                showNextButton: false, // show/hide a Next button
                showPreviousButton: false, // show/hide a Previous button
                toolbarExtraButtons: [
                    $('<button></button>').text('نمایش همه')
                        .addClass('btn btn-default')
                        .attr("KG-FID", groupdiv.id)
                        .attr("KG-CCL", 'N')
                        .attr("TRIGGERCLICK", "SHOWALL_" + item.id)
                        .on('click', function () {

                            if ($(this).attr("KG-CCL") == "N") {
                                $(this).attr("KG-CCL", "Y");
                                var doc = document.getElementById($(this).attr("kg-fid"));
                                var ddt = $(doc).children().children('[kg-uldf]').children();

                                for (var i = 0; i < ddt.length; i++) {
                                    var indiv = ddt[i].firstChild;
                                    var chls = $(indiv.lastChild).children();
                                    indiv.firstChild.className += " hide ";
                                    for (var j = 0; j < chls.length; j++) {
                                        var ediv = chls[j];
                                        ediv.className += " KGShowStuff ";
                                        ediv.children[0].children[0].className += " customform ";
                                    }

                                }
                                $(this).html('نمایش تکی');
                            }
                            else {
                                $(this).attr("KG-CCL", "N");

                                var doc = document.getElementById($(this).attr("kg-fid"));
                                var ddt = $(doc).children().children('[kg-uldf]').children();

                                for (var i = 0; i < ddt.length; i++) {
                                    var indiv = ddt[i].firstChild;
                                    var chls = $(indiv.lastChild).children();
                                    indiv.firstChild.className = indiv.firstChild.className.replace("hide", "");
                                    for (var j = 0; j < chls.length; j++) {
                                        var ediv = chls[j];
                                        ediv.className = ediv.className.replace("KGShowStuff", "");
                                        ediv.children[0].children[0].className = ediv.children[0].children[0].className.replace("customform", "");
                                    }

                                }



                                $(this).html('نمایش همه');
                            }
                        })
                ]
            }, anchorSettings: { enableAllAnchors: true }
        });
        if (item.defshowall) {
            $("[TRIGGERCLICK='SHOWALL_" + item.id + "']").trigger("click");
        }


        return groupdiv;
    }
    //__________formcontrols______________//
}

DashBoard.SmallBox = class SmallBox {
    static getText(id) {
        var el = document.getElementById(id);
        return el.children[1].children[0].innerHTML;
    }
    static setText(id, text) {
        var el = document.getElementById(id);
        el.children[1].children[0].innerHTML = text;
    }
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[1].children[1].innerHTML;
    }
    static setValue(id, value) {
        var el = document.getElementById(id);
        el.children[1].children[1].innerHTML = value;
    }
    static getColor(id) {
        var el = document.getElementById(id);
        var classname = el.children[0].className;
        return classname.replace('info-box-icon', '');
    }
    static setColor(id, colorclassName) {
        var el = document.getElementById(id);
        el.children[0].className = "info-box-icon " + colorclassName;
    }
    static getIcon(id) {
        var el = document.getElementById(id);
        return el.children[0].children[0].className;
    }
    static setIcon(id, IconClassname) {
        var el = document.getElementById(id);
        el.children[0].children[0].className = IconClassname;
    }
}

DashBoard.SmallProgressBox = class SmallProgressBox {
    static getText(id) {
        var el = document.getElementById(id);
        return el.children[1].children[0].innerHTML;
    }
    static setText(id, text) {
        var el = document.getElementById(id);
        el.children[1].children[0].innerHTML = text;
    }
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[1].children[1].innerHTML;
    }
    static setValue(id, value) {
        var el = document.getElementById(id);
        el.children[1].children[1].innerHTML = value;
    }
    static getIcon(id) {
        var el = document.getElementById(id);
        return classname = el.children[0].children[0].className;
    }
    static setIcon(id, IconClassname) {
        var el = document.getElementById(id);
        el.children[0].children[0].className = IconClassname;
    }
    static getColor(id) {
        var el = document.getElementById(id);
        var classname = el.className;
        return classname.replace('info-box', '');
    }
    static setColor(id, colorclassName) {
        var el = document.getElementById(id);
        el.className = "info-box " + colorclassName;
    }
    static getProgress(id) {
        var el = document.getElementById(id);
        return el.children[1].children[2].children[0].style.width.replace('%', '');

    }
    static setProgress(id, progresss) {
        var el = document.getElementById(id);
        el.children[1].children[2].children[0].style.width = progresss.toString() + "%";
    }
    static getDescription(id) {
        var el = document.getElementById(id);
        return el.children[1].children[3].innerHTML

    }
    static setDescription(id, descriptionn) {
        var el = document.getElementById(id);
        el.children[1].children[3].innerHTML = descriptionn;
    }
}

DashBoard.SmallInfoBox = class SmallInfoBox {
    static getText(id) {
        var el = document.getElementById(id);
        return el.children[0].children[1].innerHTML;
    }
    static setText(id, text) {
        var el = document.getElementById(id);
        el.children[0].children[1].innerHTML = text;
    }
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[0].children[0].innerHTML;
    }
    static setValue(id, value) {
        var el = document.getElementById(id);
        el.children[0].children[0].innerHTML = value;
    }
    static getColor(id) {
        var el = document.getElementById(id);
        var classname = el.className;
        return classname.replace('small-box', '');
    }
    static setColor(id, colorclassName) {
        var el = document.getElementById(id);
        el.className = "small-box " + colorclassName;
    }
    static getLink(id) {
        var el = document.getElementById(id);
        return el.children[2].href;
    }
    static setLink(id, ref) {
        var el = document.getElementById(id);
        el.children[2].href = ref;
    }
    static getIcon(id) {
        var el = document.getElementById(id);
        return el.children[1].children[0].className;
    }
    static setIcon(id, IconClassname) {
        var el = document.getElementById(id);
        el.children[1].children[0].className = IconClassname;
    }
}

DashBoard.LayoutBox = class LayoutBox {
    static setloading(id) {
        var el = document.getElementById(id);
        var lastdiv = el.children[el.children.length - 1];
        if (lastdiv.className != "overlay") {
            var overdiv = document.createElement("Div");
            var icon = document.createElement("I");
            icon.className = "fa fa-refresh fa-spin";
            overdiv.className = "overlay";
            overdiv.appendChild(icon);
            el.appendChild(overdiv);


        }
    }
    static Removeloading(id) {
        var el = document.getElementById(id.replace('-body', '').replace('-header', '').replace('-footer', ''));
        if (el != undefined) {

            var lastdiv = el.children[el.children.length - 1];
            if (lastdiv.className == "overlay") {
                el.removeChild(lastdiv);
                startlife();
            }
        }
    }

}

DashBoard.TextBox = class TextBox {
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[1].value;
    }
    static setValue(id, text) {
        var el = document.getElementById(id);
        el.children[1].value = text;
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("readonly", "");
        else {
            el.children[1].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("disabled", "");
        else {
            el.children[1].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                break;
        }
    }
    static validate(id) {
        var item = document.getElementById(id);
        if (item.attributes.disabled == undefined && item.attributes.forced != undefined && item.attributes.forced.value == "1" && TextBox.getValue(item.id) == "") {
            TextBox.setError(item.id, 3);
            return false;
        }
        else {
            TextBox.setError(item.id, 0);
            return true;
        }
    }
    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
}

DashBoard.ComboBox = class ComboBox {
    static getValue(id) {

        return $("#" + id.replace("ComboBox", "select")).val();
    }
    static setValue(id, valuee) {
        $("#" + id.replace("ComboBox", "select")).val(valuee).trigger("change");
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[2].setAttribute("readonly", "");
        else {
            el.children[2].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[2].setAttribute("disabled", "");
        else {
            el.children[2].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                el.children[2].style = " width:100%; ";
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                el.children[2].style = " border:1px solid #dd4b39 !important; width:100%;";
                break;
        }
    }
    static validate(id) {
        var item = document.getElementById(id);
        if (item.attributes.forced != undefined && item.attributes.forced.value == "1" && ComboBox.getValue(item.id) == null) {
            ComboBox.setError(item.id, 3);
            return false;
        }
        else {
            ComboBox.setError(item.id, 0);
            return true;
        }
    }
    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
    static reload(id) {
        $("#" + id.replace("ComboBox", "select")).select2('reload');
    }
}

DashBoard.DatePicker = class DatePicker {
    static getValue(id) {
        var el = document.getElementById(id);
        return tabdil(el.children[1].children[0].value);
    }
    static setValue(id, text) {
        var el = document.getElementById(id);
        el.children[1].children[0].value = tabdilB(text);
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("readonly", "");
        else {
            el.children[1].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("disabled", "");
        else {
            el.children[1].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                break;
        }
    }
    static validate(id) {
        var item = document.getElementById(id);
        if (item.attributes.forced != undefined && item.attributes.forced.value == "1" && DatePicker.getValue(item.id) == "") {
            DatePicker.setError(item.id, 3);
            return false;
        }
        else {
            DatePicker.setError(item.id, 0);
            return true;
        }
    }
    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
}

DashBoard.TimePicker = class TimePicker {
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[1].children[0].value;
    }
    static setValue(id, text) {
        var el = document.getElementById(id);
        el.children[1].children[0].value = text;
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("readonly", "");
        else {
            el.children[1].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("disabled", "");
        else {
            el.children[1].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                break;
        }
    }
    static validate(id) {
        var item = document.getElementById(id);
        if (item.attributes.forced != undefined && item.attributes.forced.value == "1" && TimePicker.getValue(item.id) == "") {
            TimePicker.setError(item.id, 3);
            return false;
        }
        else {
            TimePicker.setError(item.id, 0);
            return true;
        }
    }
    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
}

DashBoard.CheckBox = class CheckBox {
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[0].children[0].children[0].children[0].checked;
    }
    static setValue(id, text) {
        var el = document.getElementById(id);
        el.children[0].children[0].children[0].children[0].checked = text;
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("readonly", "");
        else {
            el.children[1].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("disabled", "");
        else {
            el.children[1].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                break;
        }
    }

    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
}

DashBoard.MaskTextBox = class MaskTextBox {
    static getValue(id) {
        var el = document.getElementById(id);
        return el.children[1].children[0].value;
    }
    static setValue(id, text) {
        var el = document.getElementById(id);
        el.children[1].children[0].value = text;
    }
    static setReadOnly(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("readonly", "");
        else {
            el.children[1].removeAttribute("readonly");
        }
    }
    static setDisabled(id, i) {
        var el = document.getElementById(id);
        if (i == 0)
            el.children[1].setAttribute("disabled", "");
        else {
            el.children[1].removeAttribute("disabled");
        }
    }
    static setError(id, i) {
        var el = document.getElementById(id);
        switch (i) {
            case 0:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
                break;
            case 1:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
                break;
            case 2:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
                break;
            case 3:
                el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
                break;
        }
    }
    static validate(id) {
        var item = document.getElementById(id);
        if (item.attributes.forced != undefined && item.attributes.forced.value == "1" && MaskTextBox.getValue(item.id) == "") {
            MaskTextBox.setError(item.id, 3);
            return false;
        }
        else {
            TimePicker.setError(item.id, 0);
            return true;
        }
    }
    static getDBName(id) {
        var el = document.getElementById(id);
        return el.attributes.dbid.value;
    }
}

DashBoard.Grid = class Grid {
    static reloaddata(tableid) {




        $("#" + tableid).preloader({ text: "در حال بارگذاری .." });
        $('#' + tableid).DataTable().ajax.reload(function () { $("#" + tableid).preloader('remove'); }, false);

        // $('#' + id).DataTable().ajax.reload(null, false);


    }


}

DashBoard.Form = class Form {
    static Validation(id) {
        var result = true;
        var main = document.getElementById(id);
        var childs = null;
        if (main.children[0].className == "preloader") {
            var childs = main.children[1].childNodes;
        } else {
            var childs = main.children[0].childNodes;
        }
        childs.forEach(
            function (item) {
                switch (item.attributes.ustype.value) {
                    case "6":
                        if (!DashBoard.TextBox.validate(item.id)) {
                            result = false;
                        }

                        break;
                    case "7":
                        if (!DashBoard.ComboBox.validate(item.id)) {
                            result = false;
                        }
                        break;
                    case "8":
                        if (!DashBoard.DatePicker.validate(item.id)) {
                            result = false;
                        }
                        break;
                    case "9":
                        if (!DashBoard.TimePicker.validate(item.id)) {
                            result = false;
                        }
                        break;
                    case "12":
                        if (!DashBoard.MaskTextBox.validate(item.id)) {
                            result = false;
                        }
                        break;
                }
            }
        );
        return result;
    }

    static getValues(id) {
        var json = [];
        var main = document.getElementById(id);
        var childs = null;
        if (main.children[0].className == "preloader") {
            var childs = main.children[1].childNodes;
        } else {
            var childs = main.children[0].childNodes;
        }
        childs.forEach(
            function (item) {
                var ival = {};
                switch (item.attributes.ustype.value) {
                    case "6":
                        ival["Name"] = DashBoard.TextBox.getDBName(item.id);
                        ival["Val"] = DashBoard.TextBox.getValue(item.id);
                        ival["Type"] = 6;
                        break;
                    case "7":
                        ival["Name"] = DashBoard.ComboBox.getDBName(item.id);
                        ival["Val"] = DashBoard.ComboBox.getValue(item.id);
                        ival["Type"] = 7;
                        break;
                    case "8":
                        ival["Name"] = DashBoard.DatePicker.getDBName(item.id);
                        ival["Val"] = DashBoard.DatePicker.getValue(item.id);
                        ival["Type"] = 8;
                        break;
                    case "9":
                        ival["Name"] = DashBoard.TimePicker.getDBName(item.id);
                        ival["Val"] = DashBoard.TimePicker.getValue(item.id);
                        ival["Type"] = 9;
                        break;
                    case "10":
                        ival["Name"] = DashBoard.CheckBox.getDBName(item.id);
                        ival["Val"] = DashBoard.CheckBox.getValue(item.id);
                        ival["Type"] = 10;
                        break;
                    case "12":
                        ival["Name"] = DashBoard.MaskTextBox.getDBName(item.id);
                        ival["Val"] = DashBoard.MaskTextBox.getValue(item.id);
                        ival["Type"] = 12;
                        break;
                }
                json.push(ival);
            }
        );
        var jj = {};
        var details = [];
        if (main.children[2].style.getPropertyValue("visibility") != "hidden") {
            main.children[2].children[0].children[1].childNodes.forEach(
                function (ch) {
                    var inner = {};
                    var table = $("#" + ch.children[0].children[0].children[2].id).DataTable();
                    var data = table.data().toArray().filter(function (el) { return el['rowst'] != 1 });
                    if (data.length > 0) {
                        inner[ch.children[0].children[0].children[2].getAttribute("formid")] = data;
                        details.push(inner);
                    }
                });
            if (details.length > 0) {
                jj["details"] = details;
            }
        }
        if (main.id.indexOf("Edit") >= 0) {
            jj["Updated"] = json;
            var ss = [];
            var ival = {};
            // ival[main.getAttribute("BGID")] = ;
            ss.push(ival);
            jj["idprop"] = main.getAttribute("BGIDVAL");
        } else {
            jj["Created"] = json;
        }
        return jj;
    }

    static getFieldValues(id, mode) {
        var json = [];
        var ival = {};
        if (mode != undefined && mode == "2") {
            ival["rowst"] = 3;
        } else {

            ival["rowst"] = 2;
        }
        var main = document.getElementById(id);
        var childs = null;
        var dta = null;
        if (main.children[0].className == "preloader") {
            childs = main.children[1].childNodes;
            dta = main.children[1].nextElementSibling;
        } else {
            childs = main.children[0].childNodes;
            dta = main.children[0].nextElementSibling;
        }

        if (dta.style.getPropertyValue("visibility") != "hidden") {
            var innerlival = [];
            var innerival = {};

            var chil = dta.children[0].children[1].childNodes;
            chil.forEach(
                function (item) {

                    var dataaa = $("#" + item.children[0].children[0].children[2].id).DataTable().data().toArray();
                    if (dataaa.length > 0) {
                        innerival[item.children[0].children[0].children[2].id] = dataaa;

                    }


                });


            innerlival.push(innerival);
            if (innerlival.length > 0)
                ival["ddata"] = innerlival;
            else
                ival["ddata"] = "";
        } else {

            ival["ddata"] = "";


        }



        childs.forEach(
            function (item) {
                switch (item.attributes.ustype.value) {
                    case "6":
                        //  ival["Name"] = ;
                        ival[DashBoard.TextBox.getDBName(item.id)] = DashBoard.TextBox.getValue(item.id);
                        // ival["Type"] = 6;
                        break;
                    case "7":
                        // ival["Name"] = 
                        ival[DashBoard.ComboBox.getDBName(item.id)] = DashBoard.ComboBox.getValue(item.id);
                        //  ival["Type"] = 7;
                        break;
                    case "8":
                        //   ival["Name"] = ;
                        ival[DashBoard.DatePicker.getDBName(item.id)] = DashBoard.DatePicker.getValue(item.id);
                        //   ival["Type"] = 8;
                        break;
                    case "9":
                        //   ival["Name"] = ;
                        ival[DashBoard.TimePicker.getDBName(item.id)] = DashBoard.TimePicker.getValue(item.id);
                        //   ival["Type"] = 9;
                        break;
                    case "10":
                        //   ival["Name"] = ;
                        ival[DashBoard.CheckBox.getDBName(item.id)] = DashBoard.CheckBox.getValue(item.id);
                        //  ival["Type"] = 10;
                        break;
                    case "12":
                        //  ival["Name"] = ;
                        ival[DashBoard.MaskTextBox.getDBName(item.id)] = DashBoard.MaskTextBox.getValue(item.id);
                        //  ival["Type"] = 12;
                        break;
                }
                //    json.push(ival);

            }
        );
        ival[main.getAttribute("bgid")] = (main.getAttribute("bgidval") != undefined && main.getAttribute("bgidval") != null) ? parseInt(main.getAttribute("bgidval")) : 0;
        //var jj = {};

        //if (main.id.indexOf("Edit") >= 0) {
        //    jj["Updated"] = json;
        //    var ss = [];
        //    var ival = {};
        //    // ival[main.getAttribute("BGID")] = ;
        //    ss.push(ival);
        //    jj["idprop"] = main.getAttribute("BGIDVAL");
        //} else {

        //    jj["Created"] = json;
        //}


        return ival;
    }
    static resetValues(id) {

        var main = document.getElementById(id);
        var childs = null;
        if (main.children[0].className == "preloader") {
            var childs = main.children[1].childNodes;
        } else {
            var childs = main.children[0].childNodes;
        }
        childs.forEach(
            function (item) {
                //  var ival = {};
                switch (item.attributes.ustype.value) {
                    case "6":
                        DashBoard.TextBox.setValue(item.id, "");
                        //  json.push(ival);
                        break;
                    case "7":
                        DashBoard.ComboBox.setValue(item.id, "");
                        //  json.push(ival);
                        break;
                    case "8":
                        DashBoard.DatePicker.setValue(item.id, "");
                        // json.push(ival);
                        break;
                    case "9":
                        DashBoard.TimePicker.setValue(item.id, "");
                        // json.push(ival);
                        break;
                    case "10":
                        DashBoard.CheckBox.setValue(item.id, "");
                        //  json.push(ival);
                        break;
                    case "12":
                        DashBoard.MaskTextBox.setValue(item.id, "");
                        //  json.push(ival);
                        break;
                }
            }
        );



        if (main.children[0].className == "preloader") {
            if (main.children[2].style.getPropertyValue("visibility") != "hidden") {
                main.children[2].children[0].children[1].childNodes.forEach(
                    function (ch) {

                        var table = $("#" + ch.children[0].children[0].children[2].id).DataTable();
                        table.clear().draw();

                    });

            }
        } else {
            if (main.children[1].style.getPropertyValue("visibility") != "hidden") {
                main.children[1].children[0].children[1].childNodes.forEach(
                    function (ch) {

                        var table = $("#" + ch.children[0].children[0].children[2].id).DataTable();
                        table.clear().draw();

                    });

            }
        }








    }
    static startEdit(id, rowindex, tableid, dataall) {
        NPParams["_" + id + "mid"] = rowindex;
        createmodal('newmd' + id, tableid.replace("Edit", "").replace("Grid", "").replace("Table", ""));
        document.getElementById("newmd" + id + "closebtn").addEventListener("click", function () { thisrow.pop() });
        showmodal('ویرایش', '', 6, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
        free = false;
        freene = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + id, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                moduals.forEach(function (item) {
                    DashBoard.addLayouts('EditMainLayout' + item.id, item.classname, "newmd" + id, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, rowindex, item.idproperty, tableid, 1);
                });
            }
        });
        $waitUntil(
            // element is an element whose height affected by a rule in file.css
            function () { return freene },
            function () {
                if (dataall && dataall.rowst == 3) {
                    DashBoard.Form.setValuesEdit(null, id, dataall);
                } else {

                    $.ajax({
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", Token);
                        },
                        url: "../api/values/getDformforeditdata?id=" + id + "&mid=" + rowindex, success: function (result) {
                            if (result == ("NL")) {
                                window.location.replace("../Dashboard/Login");
                            }
                            var moduals = JSON.parse(result);
                            // if (dataall)

                            // else

                            DashBoard.Form.setValuesEdit(moduals, id);
                            $("#newmd" + id).preloader('remove');

                        }
                    });
                }
                // $("#modal-default").preloader({ text: "لطفا کمی صبر کنید.." });

                //  
                startlife();
            }
        );


    }
    static starttwzform(iddd, tableid, fid, fname, val) {
        var id = iddd.replace("Edit", "");
        createmodal('newmd' + id);
        //  document.getElementById("newmd" + id + "closebtn").addEventListener("click", function () { thisrow.pop() });
        showmodal('نمایش', '', 6, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
        free = false;
        freene = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/gettwfgrid?fid=" + id + "&fname=" + fname + "&val=" + val, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = (result);
                NPParams["masterid"] = val;
                var dataall = { "id": -99, "parent": "MainLayout117", "ordering": 1, "placeid": 1, "itemTypeId": 13, "formid": "118", "hasq": "0" }
                dataall.formid = moduals.formid;
                dataall.noedit = 1;
                dataall.nodelete = 1;
                dataall.parent = "MainLayoutnewmd**-body".replace("**", id);
                DashBoard.additemLayout(dataall);

                $("#newmd" + id).preloader('remove');
            }
        });
    }
    static startEditJustShow(iddss, rowindex, tableid, dataall) {

        var id = iddss.toString().replace("Edit", "");
        createmodal('newmd' + id);
        //  document.getElementById("newmd" + id + "closebtn").addEventListener("click", function () { thisrow.pop() });
        showmodal('نمایش', '', 6, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
        free = false;
        freene = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + id, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                moduals.forEach(function (item) {
                    DashBoard.addLayouts('EditMainLayout' + item.id, item.classname, "newmd" + id, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, rowindex, item.idproperty, tableid, 1, 1);
                });
            }
        });
        $waitUntil(
            // element is an element whose height affected by a rule in file.css
            function () { return freene },
            function () {
                if (dataall && dataall.rowst == 3) {
                    Form.setValuesEdit(null, id, dataall);
                } else {

                    $.ajax({
                        beforeSend: function (request) {
                            request.setRequestHeader("Authorization", Token);
                        },
                        url: "../api/values/getDformforeditdata?id=" + id + "&mid=" + rowindex, success: function (result) {
                            if (result == ("NL")) {
                                window.location.replace("../Dashboard/Login");
                            }
                            var moduals = JSON.parse(result);
                            // if (dataall)

                            // else
                            Form.setValuesEdit(moduals, id);
                            $("#newmd" + id).preloader('remove');


                        }
                    });
                }
                // $("#modal-default").preloader({ text: "لطفا کمی صبر کنید.." });

                //  
                startlife();
            }
        );


    }
    static startEditFilter(id, rowindex, tableid, dataall, alldt) {
        createmodalfilter('newmd' + id, dataall);
        document.getElementById("newmd" + id + "closebtn").addEventListener("click", function () { thisrow.pop() });
        showmodal('فیلتر', '', 4, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
        free = false;
        //freene = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + id, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                moduals.forEach(function (item) {
                    DashBoard.addLayouts('EditMainLayout' + item.id, item.classname, "newmd" + id, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, rowindex, item.idproperty, tableid, 1, 2);
                });
            }
        });
        $waitUntil(
            function () { return free },
            function () { startlife(); $("#newmd" + id).preloader('remove'); if (alldt) DashBoard.Form.setValuesEdit(null, id, alldt[0]); });
    }
    static startNew(id, tabid) {
        createmodal('newmd' + id);
        showmodal('جدید', '', 6, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });

        freene = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + id, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                moduals.forEach(function (item) {
                    DashBoard.addLayouts('MainLayout' + item.id, item.classname, "newmd" + id, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, 0, item.idproperty, tabid);
                });



            }
        });
        $waitUntil(
            // element is an element whose height affected by a rule in file.css
            function () { return freene },
            function () {
                $("#newmd" + id).preloader('remove');
            });
    }
    static setValuesEdit(mudl, id, dataall) {

        var form = document.getElementById('MainLayoutnewmd' + id + '-body').firstChild;
        var vals = null;
        // var values = null;
        if (dataall)
            vals = dataall
        else
            vals = mudl[0];



        form.setAttribute("BGIDVAL", vals[form.getAttribute("BGID")])
        var childs = null;
        var dta = null;
        var childs = $("#" + form.id).children().children('[kg-bag]');
        // dta = form.children[1].nextElementSibling;
        dta = $("#" + form.id).children('[kg-dtz]')[0];
        childs.each(
            function (index, item) {
                if (Dash[item.id].usTypeID == 7) {

                    $waitUntil(

                        function () { return (combowait.length == 0) },
                        function () { Dash[item.id].setValue(vals[Dash[item.id].DBName]); });
                }
                else {
                    Dash[item.id].setValue(vals[Dash[item.id].DBName]);
                }
            }
        );
        var chil = dta.children[0].children[1].childNodes;
        chil.forEach(
            function (item) {
                if (vals) {

                    var dfdataa = [];
                    dfdataa = vals["ddata"];

                    if (dfdataa != undefined) {
                        var tbl = $("#" + item.children[0].children[0].children[2].id).DataTable();
                        tbl.ajax = null;
                        tbl.clear().draw();
                        tbl.ajax = null;
                        // tbl.ajax.reload();


                        tbl.rows.add(dfdataa[0][item.children[0].children[0].children[2].id]).draw();
                    }
                }


            });
    }
    static setValuesInlineEdit(mudl, id) {
        var form = document.getElementById('MainLayoutnewmd' + id + '-body').firstChild;
        var values = mudl;
        form.setAttribute("BGIDVAL", values[form.getAttribute("BGID")])
        var childs = null;
        var dta = null;
        if (form.children[0].className == "preloader") {
            var childs = form.children[1].childNodes;
            dta = form.children[1].nextElementSibling;
        } else {
            var childs = form.children[0].childNodes;
            dta = form.children[0].nextElementSibling;
        }
        childs.forEach(
            function (item) {
                var ival = {};
                switch (item.attributes.ustype.value) {
                    case "6":
                        //ival["Name"] = TextBox.getDBName(item.id);
                        //ival["Val"] = TextBox.getValue(item.id);
                        //ival["Type"] = 6;
                        DashBoard.TextBox.setValue(item.id, values[DashBoard.TextBox.getDBName(item.id)]);
                        break;
                    case "7":
                        //ival["Name"] = ComboBox.getDBName(item.id);
                        //ival["Val"] = ComboBox.getValue(item.id);
                        //ival["Type"] = 7;
                        DashBoard.ComboBox.setValue(item.id, values[DashBoard.ComboBox.getDBName(item.id)]);
                        break;
                    case "8":
                        //ival["Name"] = DatePicker.getDBName(item.id);
                        //ival["Val"] = DatePicker.getValue(item.id);
                        //ival["Type"] = 8;
                        DashBoard.DatePicker.setValue(item.id, values[DashBoard.DatePicker.getDBName(item.id)]);
                        break;
                    case "9":
                        //ival["Name"] = TimePicker.getDBName(item.id);
                        //ival["Val"] = TimePicker.getValue(item.id);
                        //ival["Type"] = 9;
                        DashBoard.TimePicker.setValue(item.id, values[DashBoard.TimePicker.getDBName(item.id)]);
                        break;
                    case "10":
                        //ival["Name"] = CheckBox.getDBName(item.id);
                        //ival["Val"] = CheckBox.getValue(item.id);
                        //ival["Type"] = 10;
                        DashBoard.CheckBox.setValue(item.id, values[DashBoard.CheckBox.getDBName(item.id)]);
                        break;
                    case "12":
                        //ival["Name"] = MaskTextBox.getDBName(item.id);
                        //ival["Val"] = MaskTextBox.getValue(item.id);
                        //ival["Type"] = 12;
                        DashBoard.MaskTextBox.setValue(item.id, values[DashBoard.MaskTextBox.getDBName(item.id)]);
                        break;
                }

            }
        );


        var chil = dta.children[0].children[1].childNodes;
        chil.forEach(
            function (item) {

                var dfdata = [];
                dfdata = values["ddata"];


                $("#" + item.children[0].children[0].children[2].id).DataTable().rows.add(dfdata[0][item.children[0].children[0].children[2].id]).draw();


                //$("#" + item.children[0].children[0].children[2].id).DataTable().data = dfdata[0][item.children[0].children[0].children[2].id];
                //$("#" + item.children[0].children[0].children[2].id).DataTable().data().draw();
                //dfdata.forEach(function (aaa) {





                //});

                //var innerival = {};





                //var dataaa = $("#" + item.children[0].children[0].children[2].id).DataTable().data();
                //if (dataaa.length > 0) {
                //    innerival[item.children[0].children[0].children[2].id] = dataaa;
                //    innerlival.push(innerival);
                //}


            });







    }
    static reloadTable(id) {
        var main = document.getElementById(id);
        DashBoard.Grid.reloaddata(main.getAttribute("targettableid"));

        // if (main.children[2].style.getPropertyValue("visibility") != "hidden") {


        //    Form.reloadinnerTable(id, );

        //   }


    }
    static reloadinnerTable(id, el) {
        var main = document.getElementById(id);
        var el = main.children[2]
        if (el.children[0].children[1].children[0])
            el.children[0].children[1].children[0].childNodes.forEach(function (item) {

                DashBoard.Grid.reloaddata(item.children[0].children[3].id);
                // console.log(item.children[0].children[2].id);


            });




    }
    static startDelete(id, rowindex, tableid, editmode) {
        var ids = id;
        showmodal('حذف', 'آیا مایل به حذف این رکورد هستید؟!', 7);
        var btn = document.getElementById('deletebtnmodal');
        btn.onclick = function (item) {
            $("#modal-defaultdelete").preloader({ text: "لطفا کمی صبر کنید.." });
            $("#" + ids).preloader({ text: "لطفا کمی صبر کنید.." });




            var jj = {};
            jj["Deleted"] = rowindex;


            //$.ajax({
            //    url: "../api/values/getFormDataComboData?id=" + item.formid, success: function (result) {
            //        var bigmoduals = JSON.parse(result);
            //    }
            //});





            $.ajax({
                type: 'POST',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                data: {
                    DBID: ids.replace("MainLayout", "").replace("Edit", ""),
                    values: JSON.stringify(jj),
                },
                url: "../api/values/DeleteDataDashBoard",
                success: function (data, status) {
                    if (status == 'success') {

                        if (data == true) {
                            hidemodal();
                            showmodal('عملیات موفق', 'حذف اطلاعات با موفقیت انجام شد', 1);

                            if (editmode) {
                                $('#' + tableid).DataTable().row(editmode).remove().draw()
                            } else {

                                DashBoard.Grid.reloaddata(tableid);
                            }


                        } else {
                            showmodal('عملیات ناموفق', 'عملیات با اشکال مواجه شد', 3);
                        }

                    } else {
                        showmodal('عملیات ناموفق', 'عملیات با اشکال مواجه شد', 3);
                    }

                }
                , error: function (dataa, status) {
                    showmodal('عملیات ناموفق', 'عملیات با اشکال مواجه شد', 3);
                    $("#modal-defaultdelete").preloader('remove');
                }
            });









        }
    }
    static startInlineEdit(id, rowindex, tableid, data) {
        createmodal('newmd' + id);
        document.getElementById("newmd" + id + "closebtn").addEventListener("click", function () { thisrow.pop() });
        showmodal('ویرایش', '', 6, 'newmd' + id);
        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
        freene = false;
        free = false;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDformforedit?id=" + id, success: function (result) {
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                moduals.forEach(function (item) {

                    DashBoard.addLayouts('MainLayout' + item.id, item.classname, "newmd" + id, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform, rowindex, item.idproperty, tableid, true);
                });


                //while (!free) {
                //    setTimeout(function () { var ss = ""; }, 50);
                //}
                //   setTimeout(function () {
                $waitUntil(
                    // element is an element whose height affected by a rule in file.css
                    function () { return freene },
                    function () {
                        $("#newmd" + id).preloader({ text: "لطفا کمی صبر کنید.." });
                        Form.setValuesInlineEdit(data, id);
                        $("#newmd" + id).preloader('remove');
                        startlife();/* css is applied on this element */
                    }
                );



                //$.ajax({
                //    url: "../api/values/getDformforeditdata?id=" + id + "&mid=" + rowindex, success: function (result) {
                //        var moduals = JSON.parse(result);




                //    }
                //});

                //  

                //   }, 1000);


            }
        });



    }

}

//______________________classes__________________________//
//____________input____________________//

function NOTRE(item) {
    this.dom = DashBoard.addnotready(item);
    this.dom = this.dom;
    this.usTypeID = item.itemTypeId;
    this.item = item;
}
NOTRE.prototype.reCreate = function () {

    if (eval(this.item.requirement) === true) {

        switch (this.item.itemTypeId) {
            case 13: // grid
                this.dom.parentNode.removeChild(this.dom);
                Dash["GridTable" + this.item.id] = new Grid(this.item);
                break;
            case 11: // button
                this.dom.parentNode.removeChild(this.dom);

                Dash["Btn" + this.item.id] = new Btn(this.item);
                break;
        }

    } else {
        switch (this.item.itemTypeId) {
            case 13: // grid
                this.dom.parentNode.removeChild(this.dom);
                Dash["GridTable" + this.item.id] = new NOTRE(this.item);
                break;
            case 11: // button
                this.dom.parentNode.removeChild(this.dom);
                Dash["Btn" + this.item.id] = new NOTRE(this.item);
                break;
        }
    }
}

function ReportGrid(item) {
    var dom = DashBoard.addreportgrid(item);
    this.item = item;
    this.data = null;
    this.grid = null;
}
ReportGrid.prototype.load = function () {
    var _this = this;
    var req = $.ajax({

        dataType: 'json',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", Token);
        },
        url: "api/values/getrepgriddata",
        type: 'GET',
        data: {
            fromdate: Dash.todate777.getValue(),
            todate: Dash.todate777.getValue(),
            bid: (Dash.Objective773.getValue() > 0 ? Dash.Objective773.getText() : ''),
            rid: (Dash.KeyResult774.getValue() > 0 ? Dash.KeyResult774.getText() : '')
        },
        success: function (val1, val2) {
            if (val2 == 'success') {
                cdata = JSON.parse(val1);

                var rowData = cdata;
                var columnDefs = [
                    //{ headerName: "Company Name", field: "CompanyName", rowGroup: true },
                    { headerName: "Data Level Title", field: "DataLevelTitle", rowGroup: true },
                    { headerName: "Objective Name", field: "ObjectiveName", rowGroup: true },
                    { headerName: "Key Result Name", field: "KeyRName" },
                    {
                        headerName: "Value", field: "Value",
                        cellStyle: function (params) {

                            if (params.value) {
                                if (params.value < params.data.RedBorderValue) {
                                    return { color: 'black', backgroundColor: 'red' };
                                } else if (params.value < params.data.YellowBorderValue) {
                                    return { color: 'black', backgroundColor: 'yellow' };
                                } else {
                                    return { color: 'black', backgroundColor: 'green' };
                                }
                            } else {

                                return null;
                            }

                        }
                    }
                ];
                var div = document.getElementById("repgrid" + _this.item.id);
                var gridOptions = {
                    columnDefs: columnDefs,
                    rowData: rowData
                };
                div.innerHTML = "";


                _this.grid = new agGrid.Grid(div, gridOptions);
                _this.grid.gridOptions.api.expandAll();


            }
        }
    });
}
ReportGrid.prototype.load2 = function () {
    var _this = this;
    var req = $.ajax({

        dataType: 'json',
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", Token);
        },
        url: "api/values/getrepgriddata2",
        type: 'GET',
        data: {
            fromdate: Dash.pdate823.getValue()
        },
        success: function (val1, val2) {
            if (val2 == 'success') {
                cdata = JSON.parse(val1);

                var rowData = cdata;
                var columnDefs = [
                    //{ headerName: "Company Name", field: "CompanyName", rowGroup: true },
                    //{ headerName: "Data Level Title", field: "DataLevelTitle", rowGroup: true },
                    { headerName: "Objective Name", field: "ObjectiveName", rowGroup: true },
                    { headerName: "Action Type Name", field: "ActionTypeName", rowGroup: true },
                    {
                        headerName: "Value", field: "Value",
                        cellStyle: function (params) {
                            if (params.value) {
                                if (params.value.toString()  === "انجام شده") {
                                    return { color: 'black', backgroundColor: 'green' };

                                }  else {
                                    return { color: 'black', backgroundColor: 'red' };
                                }
                            } else {
                                return null;
                            }

                        }
                    }
                ];
                var div = document.getElementById("repgrid" + _this.item.id);
                var gridOptions = {
                    columnDefs: columnDefs,
                    rowData: rowData
                };
                div.innerHTML = "";


                _this.grid = new agGrid.Grid(div, gridOptions);
                _this.grid.gridOptions.api.expandAll();


            }
        }
    });
}

function CHtmlBase() {
    this.id = '';
    this.dom = null;
    this.usType = '';
    this.usTypeID = 0;
}
CHtmlBase.prototype.setHtml = function (val) {
    var htm = $('#' + this.dom.id).find('[KG-HTML]')[0];
    htm.innerHTML = val;
}
function CHTML(item) {
    this.dom = DashBoard.addcustomhtml(item);
    this.id = this.dom.id;
    this.usType = 'CustomHTML';
    this.usTypeID = 21;
}
CHTML.prototype = new CHtmlBase();
function FileUploadBase() {
    this.id = '';
    this.dom = null;
    this.DBName = '';
    this.readOnly = false;
    this.disabled = false;
    this.foreced = false;
    this.usType = '';
    this.usTypeID = 0;
    this.dbid = '';
    this.uploader = null;
    this.files = [];
    this.Listeners = {};
}
function FileUpload(item) {
    this.dom = DashBoard.addfileupload(item);
    this.files = [];
    this.uploader = $(this.dom.children[1]).fileupload({
        url: item.url,
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", Token);
        },
    }).bind('fileuploaddone', function (e, data) {

        Dash[item.dbsource + item.id].files.push(data._response.result.files[0]);
        Dash[item.dbsource + item.id].setDeleteMethod();
        Dash[item.dbsource + item.id].setDownLoadMethod();

    });
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'FileUpload';
    this.usTypeID = 20;
    this.item = item;
}
FileUpload.prototype = new FileUploadBase();
FileUpload.prototype.getValue = function () {
    var filess = {};
    filess["files"] = this.files;
    return JSON.stringify(filess);
}
FileUpload.prototype.setValue = function (val) {
    // console.log(val);
    if (val && val != '') {
        var tttt = JSON.parse(val);
        this.files = tttt["files"];
        $(this.dom.children[1]).fileupload('option', 'done')
            .call(this.dom.children[1], $.Event('done'), { result: tttt });
        this.setDeleteMethod();
        this.setDownLoadMethod();
    }

    //  $(this.dom.children[1]).fileupload('done', { files: tttt });
}
FileUpload.prototype.setError = function (i) {
    var el = this.dom;
    switch (i) {
        case 0:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
            if (this.usTypeID == 7)
                el.children[2].style = " width:100%;";
            this.hasError = false;
            break;
        case 1:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
            this.hasError = false;
            break;
        case 2:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
            this.hasError = false;
            break;
        case 3:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
            if (this.usTypeID == 7)
                el.children[2].style = " border:1px solid #dd4b39 !important; width:100%;";
            this.hasError = true;
            break;
    }
}
FileUpload.prototype.validate = function () {
    var item = this.dom;
    if (item.attributes.disabled == undefined && item.attributes.forced != undefined && item.attributes.forced.value == "1" && (this.getValue() == "{\"files\":[]}" || this.getValue() == null || this.getValue() == undefined)) {
        this.setError(3);
        return false;
    }
    else {
        this.setError(0);
        return true;
    }

}
FileUpload.prototype.HasValue = function () {
    var item = this.dom;
    if ((this.getValue() == "{\"files\":[]}" || this.getValue() == null || this.getValue() == undefined)) {
        return false;
    }
    else {
        return true;
    }

}
FileUpload.prototype.setDeleteMethod = function () {
    var ffli = this;
    setTimeout(function () {
        $("[data-type*='KPDELETE']").click(function (e) {
            e.preventDefault();
            var $link = $(this);
            var req = $.ajax({
                dataType: 'json',
                beforeSend: function (request) {
                    request.setRequestHeader("Authorization", Token);
                },
                url: $link.data('url'),
                type: 'POST'
                ,
                success: function (val1, val2) {
                    if (val2 == 'success') {
                        $link.closest('tr').remove();
                        val1.files.forEach(function (el) {
                            ffli.files = ffli.files.filter(function (el2) { return el2.name != el.name })
                        })
                    }
                }
            });

        });
    }, 1000);

}
FileUpload.prototype.setDownLoadMethod = function () {
    var ffli = this;
    setTimeout(function () {
        $("[data-type*='KPDownLoad']").click(function (e) {
            e.preventDefault();
            var $link = $(this);
            var thiss = this;
            console.log($link);

            var x = new XMLHttpRequest();
            x.open('GET', $link.data('url') + "&uploadedfilename=" + $link.attr("uploadedfilename"), true);
            x.setRequestHeader('Authorization', Token);
            x.responseType = 'blob';
            x.onload = function (e) {
                download(e.target.response, $link.attr("uploadedfilename"), 'application/pdf');
            };
            x.send();




            //var req = $.ajax({
            //    dataType: 'json',
            //    beforeSend: function (request) {
            //        request.setRequestHeader("Authorization", Token);
            //    },
            //    url: $link.data('url') + "&uploadedfilename=" +$link.attr("uploadedfilename"),
            //    type: 'POST'
            //    ,
            //    success: function (val1, val2) {
            //        if (val2 == 'success') {
            //            console.log(val1);
            //            console.log(val2);
            //        }
            //    }
            //});

        });
    }, 1000);

}
function InputControl() {
    this.id = '';
    this.dom = null;
    this.DBName = '';
    this.readOnly = false;
    this.disabled = false;
    this.foreced = false;
    this.hasError = false;
    this.usType = '';
    this.usTypeID = 0;
    this.dbid = '';
    this.item = null;
    this.Listeners = {};
    this.hasloadeddata = false;
}
InputControl.prototype.getValue = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return input.value;
}
InputControl.prototype.getText = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return input.value;
}
InputControl.prototype.setValue = function (val) {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    input.value = val;
    this.hasloadeddata = true;
}
InputControl.prototype.setReadOnly = function (i) {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];

    if (i == 1) {
        input.setAttribute("readonly", "");
        this.readOnly = true;
    }
    else {
        input.removeAttribute("readonly");
        this.readOnly = false;
    }
}
InputControl.prototype.setForce = function (i) {





    var lab = $('#' + this.dom.id).find('[KG-Lab]')[0];

    if (i == 1) {
        lab.innerHTML += "<span style=\"Color:red;\"> *</span>";
        this.dom.setAttribute("forced", "1");
        this.foreced = true;
    }
    else {
        lab.innerHTML = lab.innerHTML.replace("<span style=\"Color:red;\"> *</span>", "");
        this.dom.removeAttribute("forced");
        this.foreced = false;
    }
}
InputControl.prototype.setDisabled = function (i) {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    if (i == 1) {
        input.setAttribute("disabled", "");
        this.disabled = true;
    }
    else {
        input.removeAttribute("disabled");
        this.disabled = false;
    }
}
InputControl.prototype.setError = function (i) {
    var el = this.dom;
    switch (i) {
        case 0:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "");
            if (this.usTypeID == 7)
                el.children[2].style = " width:100%;";
            this.hasError = false;
            break;
        case 1:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-success";
            this.hasError = false;
            break;
        case 2:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-warning";
            this.hasError = false;
            break;
        case 3:
            el.className = el.className.replace("has-success", "").replace("has-warning", "").replace("has-error", "") + " has-error";
            if (this.usTypeID == 7)
                el.children[2].style = " border:1px solid #dd4b39 !important; width:100%;";
            this.hasError = true;
            break;
    }
}
InputControl.prototype.validate = function () {
    var item = this.dom;
    if (item.attributes.disabled == undefined && item.attributes.forced != undefined && item.attributes.forced.value == "1" && (!this.getValue() || this.getValue() == "" || this.getValue() == null || this.getValue() == undefined)) {
        this.setError(3);
        return false;
    }
    else {
        this.setError(0);
        return true;
    }

}
InputControl.prototype.getDBName = function () {
    this.DBName = this.dom.attributes.dbid.value;
    return this.dom.attributes.dbid.value;
}
InputControl.prototype.HasValue = function () {
    var res = true;
    var val = this.getValue();
    switch (this.usTypeID) {
        case 17:
        case 18:
        case 7:
            if (!val) {
                res = false;
            }
            break;
        case 6:
        case 8:
        case 9:
        case 12:
        case 16:
            if (!this.hasloadeddata) {
                if (val && val != "") {

                }
                else {
                    res = false;
                }
            }

            break;
    }

    return res;
}
InputControl.prototype.setVisible = function (el) {
    if (!el) {
        $(this.dom).css("visibility", "hidden");
    } else {
        $(this.dom).css("visibility", "visible");
    }
}
InputControl.prototype.doEvents = function (name, el1, el2, el3) {
    if (this.item.disableevents) {
        return;
    }
    if (this.item["on" + name]) {
        var its = JSON.parse(this.item["on" + name]);
        if (its) {
            its.forEach(function (el) {
                while (el.toString().indexOf('P[') >= 0) {
                    var inval = el.substring(el.indexOf('P['), el.substring(el.indexOf('P[')).indexOf(']P') + el.indexOf('P[') + 2);
                    el =
                        el.replace(
                            inval
                            ,
                            getExtraVal(inval.replace('P[', '').replace(']P', ''), el1)
                        );
                }
                eval(el);
            });
        }
    }

}
function TBLWIZ(item) {
    this.dom = DashBoard.addtablewiz(item);
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'TableWiz';
    this.usTypeID = 22;
    this.item = item
};
TBLWIZ.prototype = new InputControl();
TBLWIZ.prototype.getValue = function () {
    var res = {};
    var dom = this.dom;
    var dvs = $(dom).find('[KG-ULF]').find('.nav-item');
    for (i = 0; i < dvs.length; i++) {
        var id = $(dvs[i]).attr('KG-ID');
        res[id] = {};
        var dform = document.getElementById($(dvs[i]).find('a:first').attr('href').substring(1));

        var innerdvs = $(dform).find('[KG-ULS]').find('.nav-item');
        for (j = 0; j < innerdvs.length; j++) {
            var innerid = $(innerdvs[j]).attr('KG-ID');
            var innerdform = document.getElementById($(innerdvs[j]).find('a:first').attr('href').substring(1));

            var data = Dash[$(innerdform).attr('KG-FormID')].getValues();
            res[id][innerid] = data;
        }
    }
    res.tableviztype = (Extra.AAA ? 2 : 1);
    return res;
}
TBLWIZ.prototype.validate = function () {
    var res = true;
    if (this.item.disabled == undefined && this.item.forced != undefined && this.item.forced.value == "1") {
        var dom = this.dom;
        var dvs = $(dom).find('[KG-ULF]').find('.nav-item');
        for (i = 0; i < dvs.length; i++) {
            var id = $(dvs[i]).attr('KG-ID');
            res[id] = {};
            var dform = document.getElementById($(dvs[i]).find('a:first').attr('href').substring(1));
            var innerdvs = $(dform).find('[KG-ULS]').find('.nav-item');
            for (j = 0; j < innerdvs.length; j++) {
                var innerdform = document.getElementById($(innerdvs[j]).find('a:first').attr('href').substring(1));
                if (!Dash[$(innerdform).attr('KG-FormID')].Validation()) {
                    //  console.log(innerdvs[j]);
                    $(innerdvs[j]).find('a:first').trigger("click");
                    res = false;
                }
                if (!res) break;
            }
            if (!res) {

                $(dvs[i]).find('a:first').trigger("click");
                break;
            }
        }
    }
    return res;
}
TBLWIZ.prototype.setValue = function (val) {
    // console.log('val');
    //  console.log(val);
    var meta = (JSON.parse(this.item.meta));
    //$(this.dom).preloader({ text: "در حال بارگذاری .." });
    if (val) {

        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/gettwfdata?id=" + val + "&formid=" + meta.formid + "&fieldid=" + this.item.id.replace("Edit", ""), success: function (result) {
                // $(this.dom).preloader('remove');
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                var moduals = JSON.parse(result);
                meta.groups.forEach(function (el) {

                    var innerels = el.FieldValues;
                    innerels.forEach(function (elm) {
                        var id = 'Form' + meta.formid + 'wiz-' + elm.Name + el.Name;
                        while (id.indexOf(' ') >= 0) {
                            id = id.replace(' ', '');
                        }
                        var vals = (moduals.filter(function (ell) { return (ell.DescriptionGroup == el.Name && ell.DescriptionField == elm.Name) }));

                        Dash[id].setValues(vals[0]);

                    });
                });

            }
        });
    } else {

    }
}
function MTbox(item) {
    this.dom = DashBoard.addmasktextbox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'MaskTextBox';
    this.usTypeID = 12;
    this.item = item;
}
MTbox.prototype = new InputControl();
function CBox(item) {
    this.dom = DashBoard.addcombobox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'ComboBox';
    this.usTypeID = 7;
    this.item = item;
    this.loadedval = 0;
}
CBox.prototype = new InputControl();
CBox.prototype.getValue = function () {
    return parseInt($("#select" + this.dbid).val());
}
CBox.prototype.getText = function () {
    if (this.getValue() != null)
        return $('#select2-select' + this.dbid + '-container')[0].title;
    else
        return "";
}
CBox.prototype.setValue = function (valuee) {

    this.loadedval = valuee;
    $("#select" + this.dbid).val(valuee).trigger("change");//
    this.hasloadeddata = true;
}
CBox.prototype.getLoadedValue = function () {
    return this.loadedval;
}
CBox.prototype.setDisabled = function (i) {
    readonly_select($('#select' + this.item.id)[0].nextSibling, i);
}
function TBox(item) {
    this.dom = DashBoard.addtextbox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'TextBox';
    this.usTypeID = 6;
    this.item = item;
}
TBox.prototype = new InputControl();
function PCheckBox(item) {
    this.dom = DashBoard.addcheckbox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'CheckBox';
    this.usTypeID = 10;
    this.item = item;
}
PCheckBox.prototype = new InputControl();
PCheckBox.prototype.getValue = function () {
    console.log('#' + this.dom.id);
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return input.checked;
}
PCheckBox.prototype.setValue = function (val) {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    input.checked = val;
    this.hasloadeddata = true;
}
function NumBox(item) {
    this.dom = DashBoard.addnumberbox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'NumberBox';
    this.usTypeID = 17;
    this.item = item
}
NumBox.prototype = new InputControl();
NumBox.prototype.getValue = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return parseInt(input.value);
}
NumBox.prototype.validate = function () {
    var item = this.dom;
    if (item.attributes.disabled == undefined && item.attributes.forced != undefined && item.attributes.forced.value == "1" && (this.getValue() == "" || this.getValue() == null || isNaN(this.getValue()) || this.getValue() == undefined) && (this.getValue() != 0)) {
        this.setError(3);
        return false;
    }
    else {
        this.setError(0);
        return true;
    }
}
function NumDecBox(item) {
    this.dom = DashBoard.addnumberdecimalbox(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'NumberDecimalBox';
    this.usTypeID = 18;
    this.item = item
}
NumDecBox.prototype = new InputControl();
NumDecBox.prototype.getValue = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return parseFloat(input.value);
}
function PDatePicker(item) {
    this.dom = DashBoard.adddatepicker(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'PDatePicker';
    this.usTypeID = 8;
    this.item = item
}
PDatePicker.prototype = new InputControl();
PDatePicker.prototype.getValue = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    if (this.item.gregorian) {
        return input.value;
    } else {
        return tabdil(input.value);
    }

}
PDatePicker.prototype.getText = function () {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    return tabdil(input.value);
}
PDatePicker.prototype.setValue = function (valuee) {
    var input = $('#' + this.dom.id).find('[KG-INP]')[0];
    if (this.item.gregorian) {
        input.value = (valuee);
    } else {
        input.value = tabdilB(valuee);
    }
    this.hasloadeddata = true;
}
function TimePicker(item) {
    this.dom = DashBoard.addtimepicker(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'TimePicker';
    this.usTypeID = 9;
    this.item = item
}
TimePicker.prototype = new InputControl();
function MultiTBox(item) {
    this.dom = DashBoard.addtextboxMline(item);
    if (item.seErr) {
        this.setError(item.seErr);
    }
    this.id = this.dom.id;
    this.dbid = item.id;
    this.DBName = item.dbsource;
    this.readOnly = item.readonly;
    this.disabled = item.enable;
    this.foreced = item.forced;
    this.hasError = false;
    this.usType = 'MultiTextBox';
    this.usTypeID = 16;
    this.item = item
}
MultiTBox.prototype = new InputControl();
//____________input____________________//
//____________Action____________________//
function ActionControl() {
    this.dom = null;
    this.id = '';
    this.usType = '';
    this.usTypeID = 0;
    this.item = null;
}
function Btn(item) {
    this.dom = DashBoard.addbutton(item);
    this.id = this.dom.id;
    this.usType = '11';
    this.usTypeID = 11;
    this.item = item
}
Btn.prototype = new ActionControl();
Btn.prototype.reCreate = function () {
    this.dom.parentNode.removeChild(this.dom);
    Dash["Btn" + this.item.id] = new Btn(this.item);
}
//____________Action____________________//
//____________Layout___________________//
function BoxControl(idd, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata) {
    this.dom = DashBoard.addBox(idd, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata);
    this.id = idd;
}
BoxControl.prototype.showLoader = function () {
    var loader = $('#' + this.dom.id).find('[KG-BoxLoader]')[0];
    if (loader) {
        return;
    } else {
        var overdiv = document.createElement("Div");
        overdiv.setAttribute("KG-BoxLoader", "")
        var icon = document.createElement("I");
        icon.className = "fa fa-refresh fa-spin";
        overdiv.className = "overlay";
        overdiv.appendChild(icon);
        this.dom.appendChild(overdiv);
    }
}
BoxControl.prototype.hideLoader = function () {
    var loader = $('#' + this.dom.id).find('[KG-BoxLoader]')[0];
    if (loader) {
        loader.remove();
    }
}
BoxControl.prototype.collaps = function () {
    var clp = $('#' + this.dom.id).find('[data-widget="collapse"]')[0];
    if (clp) {
        clp.click();
    }
}
BoxControl.prototype.close = function () {
    var clp = $('#' + this.dom.id).find('[data-widget="remove"]')[0];
    if (clp) {
        clp.click();
    }
}
BoxControl.prototype.load = function () {
    this.showLoader();
    var temp = this.id.replace('MainLayout', '').replace('Edit', '').replace("details", "");
    var box = this;
    $.ajax({
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", Token);
        },
        url: "../api/values/getLayoutItems?id=" + temp + "&draw=" + drawid, success: function (result) {
            var idss = '';
            if (result.startsWith('NotFound-')) {
                // DashBoard.LayoutBox.Removeloading('BoxMainLayout' + result.replace("NotFound-", ""));
                box.hideLoader();
            }
            else if (result == ("NL")) {
                window.location.replace("../Dashboard/Login");
            }
            else {
                if (result.startsWith(drawid)) {

                    var moduals = JSON.parse(result.substring(drawid.toString().length));
                    moduals.forEach(function (item) {
                        DashBoard.additemBox(item);
                        idss = item.parent;
                    });
                    if (idss != '')
                        box.hideLoader();
                    //  DashBoard.LayoutBox.Removeloading('Box' + idss);
                }
            }
        }
    });
}
function BaseFormControl() {

    this.dom = null;
    this.id = 0;
    this.masterid = null;
    this.editmode = null;
    this.idprop = null;
    this.parent = null;
    this.isreadmode = null;
    this.targetid = null;
}
BaseFormControl.prototype.load = function () {
    var iseditmode = this.editmode;
    var isreadmode = this.isreadmode;
    var masterid = this.masterid;

    var form = this.dom;
    var tid = this.id.replace('MainLayout', '').replace('Edit', '').replace("details", "");
    if (iseditmode != undefined && iseditmode >= 0) {
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            // url: "../api/values/getLayoutItemsForDetails?id=" + tid, success: function (result) {
            url: "../api/values/getLayoutItems?id=" + tid + "&draw=" + drawid, success: function (result) {
                if (result.startsWith('NotFound-')) {
                    // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
                    freene = true;
                }
                else if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                else {
                    if (result.startsWith(drawid)) {

                        var moduals = JSON.parse(result.substring(drawid.toString().length));
                        moduals.forEach(function (item) {
                            var flag = true;
                            if (masterid) {
                                item.parent = item.parent.replace("MainLayout", "MainLayoutdetails");
                            }
                            if (isreadmode) {
                                item.disableevents = true;
                                if (isreadmode == 1) {
                                    item.readonly = true;
                                    if ("11".indexOf(item.itemTypeId.toString()) >= 0) {
                                        flag = false;
                                    }
                                }
                                if (isreadmode == 2) {
                                    delete item.disable;
                                    delete item.readonly;

                                    if ("11,14,22".indexOf(item.itemTypeId.toString()) >= 0) {
                                        flag = false;
                                    }
                                }
                                delete item.forced;
                            }
                            if (flag)
                                DashBoard.additemfrom(item, iseditmode);
                        });
                        //   $("#modal-default").preloader('remove');
                        //   free = true;
                        freene = true;
                        startlife();
                    }
                }
                free = true;
            }
        });
    }
    else {
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getLayoutItems?id=" + tid + "&draw=" + drawid, success: function (result) {
                if (result.startsWith('NotFound-')) {
                    free = true;
                    // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
                }
                else if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                else {
                    if (result.startsWith(drawid)) {

                        var moduals = JSON.parse(result.substring(drawid.toString().length));
                        moduals.forEach(function (item) {
                            var flag = true;
                            if (masterid) {
                                item.parent = item.parent.replace("MainLayout", "MainLayoutdetails");
                            }
                            if (isreadmode) {
                                if (isreadmode == 1) {
                                    item.readonly = true;
                                }
                                delete item.forced;
                                if ("11".indexOf(item.type) >= 0) {
                                    flag = false;
                                }
                            }
                            if (flag)
                                DashBoard.additemfrom(item, iseditmode);
                        });
                        $("#" + this.parent).preloader('remove');
                        //  free = true;
                        startlife();
                    }
                }
            }
        });

        freene = true;
    }





}
BaseFormControl.prototype.Validation = function () {
    var result = true;
    var childs = $("#" + this.dom.id).find('[kg-bag]');
    childs.each(function (index, el) { if (!Dash[el.id].validate()) { result = false; } });
    return result;
}
BaseFormControl.prototype.getValues = function () {
    var json = [];
    var childs = $("#" + this.dom.id).children().children('[KG-Bag]');;
    childs.each(function (index, el) {
        if (!Dash[el.id].disabled && Dash[el.id].HasValue()) {
            var ival = {};
            ival["Name"] = Dash[el.id].DBName;
            ival["Val"] = Dash[el.id].getValue();
            ival["Type"] = Dash[el.id].usTypeID;
            json.push(ival);
        }
    });
    var jj = {};
    var details = [];

    var dtt = $("#" + this.dom.id).children('[kg-dtz]')[0];
    //
    if (dtt.style.getPropertyValue("visibility") != "hidden") {

        dtt.children[0].children[1].childNodes.forEach(
            function (ch) {

                var inner = {};
                var table = $("#" + ch.children[0].children[0].children[3].id.replace("_processing", "")).DataTable();
                var data = table.data().toArray().filter(function (el) { return el['rowst'] != 1 });
                if (data.length > 0) {
                    inner[ch.children[0].children[0].children[3].getAttribute("formid")] = data;
                    details.push(inner);
                }
            });
        if (details.length > 0) {
            jj["details"] = details;
        }
    }
    if (this.editmode > 0) {
        jj["Updated"] = json;
        var ss = [];
        var ival = {};
        // ival[main.getAttribute("BGID")] = ;
        ss.push(ival);
        jj["idprop"] = this.dom.getAttribute("BGIDVAL");
    } else {
        jj["Created"] = json;
    }
    return jj;
}
BaseFormControl.prototype.getTextValues = function () {
    var json = [];
    var childs = $("#" + this.dom.id).find('[kg-bag]');
    childs.each(function (index, el) {
        if (!Dash[el.id].disabled) {
            var ival = {};
            ival["Name"] = Dash[el.id].DBName;
            ival["Val"] = Dash[el.id].getText();
            ival["Type"] = Dash[el.id].usTypeID;
            json.push(ival);
        }
    });
    var jj = {};
    var details = [];
    var dtt = $("#" + this.dom.id).children('[kg-dtz]')[0];
    //
    if (dtt.style.getPropertyValue("visibility") != "hidden") {
        dtt.children[0].children[1].childNodes.forEach(
            function (ch) {
                var inner = {};
                var table = $("#" + ch.children[0].children[0].children[2].id).DataTable();
                var data = table.data().toArray().filter(function (el) { return el['rowst'] != 1 });
                if (data.length > 0) {
                    inner[ch.children[0].children[0].children[2].getAttribute("formid")] = data;
                    details.push(inner);
                }
            });
        if (details.length > 0) {
            jj["details"] = details;
        }
    }
    if (this.editmode > 0) {
        jj["Updated"] = json;
        var ss = [];
        var ival = {};
        // ival[main.getAttribute("BGID")] = ;
        ss.push(ival);
        jj["idprop"] = this.dom.getAttribute("BGIDVAL");
    } else {
        jj["Created"] = json;
    }
    return jj;
}
BaseFormControl.prototype.resetValues = function () {
    var main = this.dom;
    var childs = $("#" + this.dom.id).find('[kg-bag]');
    childs.each(
        function (index, item) {
            Dash[item.id].setValue("");
        }
    );



    if (main.children[0].className == "preloader") {
        if (main.children[2].style.getPropertyValue("visibility") != "hidden") {
            main.children[2].children[0].children[1].childNodes.forEach(
                function (ch) {

                    var table = $("#" + ch.children[0].children[0].children[3].id).DataTable();
                    table.clear().draw();

                });

        }
    } else {
        if (main.children[1].style.getPropertyValue("visibility") != "hidden") {
            main.children[1].children[0].children[1].childNodes.forEach(
                function (ch) {

                    var table = $("#" + ch.children[0].children[0].children[3].id).DataTable();
                    table.clear().draw();

                });

        }
    }
}
BaseFormControl.prototype.getFieldValues = function (mode) {
    var id = this.dom.id;
    var json = [];
    var ival = {};
    if (mode != undefined && mode == "2") {
        ival["rowst"] = 3;
    } else {

        ival["rowst"] = 2;
    }
    var main = document.getElementById(id);

    var dta = null;
    if (main.children[0].className == "preloader") {
        dta = main.children[1].nextElementSibling;
    } else {
        dta = main.children[0].nextElementSibling;
    }

    if (dta.style.getPropertyValue("visibility") != "hidden") {
        var innerlival = [];
        var innerival = {};

        var chil = dta.children[0].children[1].childNodes;
        chil.forEach(
            function (item) {

                var dataaa = $("#" + item.children[0].children[0].children[2].id).DataTable().data().toArray();
                if (dataaa.length > 0) {
                    innerival[item.children[0].children[0].children[2].id] = dataaa;

                }


            });


        innerlival.push(innerival);
        if (innerlival.length > 0)
            ival["ddata"] = innerlival;
        else
            ival["ddata"] = "";
    } else {

        ival["ddata"] = "";


    }
    var childs = $("#" + this.dom.id).find('[kg-bag]');
    childs.each(function (index, el) {
        if (!Dash[el.id].disabled) {
            ival[Dash[el.id].DBName] = Dash[el.id].getValue();
        }
    });

    try {

        var tbll = this.targetid.replace('GridTable', 'Grid').replace('Grid', 'GridTable').replace('Details', '');
        Dash[tbll].table.settings().init().columns.forEach(
            function (el) {
                if (el.sClass && el.sClass.indexOf('addtolocaltag ') >= 0) {
                    ival[el.data] = '';
                }
            }
        )
    } catch (el) { }



    ival[main.getAttribute("bgid")] = (main.getAttribute("bgidval") != undefined && main.getAttribute("bgidval") != null) ? parseInt(main.getAttribute("bgidval")) : 0;
    return ival;
}
BaseFormControl.prototype.Customload = function (data, lid, el, ids, elm) {
    var iseditmode = this.editmode;
    var isreadmode = this.isreadmode;
    var form = this.dom;
    var tid = lid.DashBoardLayoutID;
    var moduals = data;
    moduals.forEach(function (itemm) {

        var flag = true;
        if (isreadmode) {
            if (isreadmode == 1) {
                itemm.readonly = true;
                if ("11".indexOf(itemm.itemTypeId.toString()) >= 0) {
                    flag = false;
                }
            }
            if (isreadmode == 2) {
                delete itemm.enable;
                delete itemm.readonly;
                if ("11,14,22".indexOf(itemm.itemTypeId.toString()) >= 0) {
                    flag = false;
                }
            }
            delete itemm.forced;

        }

        if (itemm.dbsource === "pdate") {
            itemm["defualtvalue"] = elm.TargetDate;
        }
        if (itemm.dbsource === "TargetValue") {
            itemm["defualtvalue"] = elm.TargetValue;
        }
        if (itemm.dbsource === "BaseValue") {
            itemm["defualtvalue"] = elm.BaseValue;
        }



        var itemss = itemm;
        itemss.parent = form.id;
        itemss.pureid = itemm.id;
        itemss.id = itemm.id + el.Val + ids;
        while (itemss.id.indexOf(' ') >= 0) {
            itemss.id = itemss.id.replace(' ', '');
        }
        if (!itemss.enable && !itemss.readonly) {
            itemss.seErr = 2;
        }
        itemss.parentdata = { "elname": el.Name, "elval": el.value, "elmn": ids, "elm": elm, "id": itemm.id };
        if (flag)
            DashBoard.additemfrom(itemss, iseditmode);
    });

}
BaseFormControl.prototype.setValues = function (vals) {
    var form = this.dom;

    form.setAttribute("BGIDVAL", vals[form.getAttribute("BGID")])
    var childs = null;
    var dta = null;
    var childs = $("#" + form.id).children().children('[kg-bag]');
    // dta = form.children[1].nextElementSibling;
    dta = $("#" + form.id).children('[kg-dtz]')[0];
    childs.each(
        function (index, item) {


            if (Dash[item.id].usTypeID == 7) {
                $waitUntil(

                    function () { return (combowait.length == 0) },
                    function () { Dash[item.id].setValue(vals[Dash[item.id].DBName]); });
            }
            else {

                Dash[item.id].setValue(vals[Dash[item.id].DBName]);
            }
        }
    );
    var chil = dta.children[0].children[1].childNodes;
    chil.forEach(function (item) {
        if (vals) {

            var dfdataa = [];
            dfdataa = vals["ddata"];

            if (dfdataa != undefined) {
                var tbl = $("#" + item.children[0].children[0].children[2].id).DataTable();
                tbl.ajax = null;
                tbl.clear().draw();
                tbl.ajax = null;
                // tbl.ajax.reload();


                tbl.rows.add(dfdataa[0][item.children[0].children[0].children[2].id]).draw();
            }
        }
    });
}

function FormControl(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, mmasterid, otherOption) {

    this.dom = DashBoard.addForm(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, mmasterid, otherOption);
    this.id = id;
    this.masterid = mmasterid;
    this.editmode = iseditmode;
    this.idprop = idprop
    this.parent = parent
    this.isreadmode = isreadmode;
    this.targetid = targetid;
}
FormControl.prototype = new BaseFormControl();
FormControl.prototype.Customload2 = function (tabname, moduls, parentid) {
    console.log(tabname);
    console.log(moduls);
    moduls.forEach(function (el) {
        if (el.TabName == tabname) {
            var item = {};
            item.id = "";
            item.title = el.sCaption;
            item.parent = parentid;
            item.placeid = 1;
            item.itemTypeId = 6;
            item.dbsource = "";
            item.hasq = 0;
            item.placeholder = "";
            item.width = 500;
            DashBoard.itemcreate(item);
        }

    });


}
function FormDetails(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, masterid) {


    this.dom = DashBoard.adddetailsForm(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, masterid);
    this.id = id;
    this.editmode = iseditmode;
    this.idprop = idprop
    this.parent = parent
    this.isreadmode = isreadmode;
    this.targetid = targetid;
    if (masterid) {
        this.hasMaster = true;
        this.masterid = masterid
    } else {
        this.hasMaster = false;
    }
}
FormDetails.prototype = new BaseFormControl();

function TabFormControl(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, isreadmode, mmasterid) {

    this.dom = DashBoard.addTabForm(id, classname, parent, isbox, boxtype, title, header, footer, infooter, icn, isform, iseditmode, idprop, targetid, hasdata, mmasterid);
    this.id = id;
    this.isform = isform;
    this.masterid = mmasterid;
    this.editmode = iseditmode;
    this.idprop = idprop
    this.parent = parent
    this.isreadmode = isreadmode;
    this.targetid = targetid;
}
TabFormControl.prototype = new BaseFormControl();
TabFormControl.prototype.load = function () {
    // console.log(this);
    var _temp = this;
    var iseditmode = this.editmode;
    var isreadmode = this.isreadmode;
    var masterid = this.masterid;

    var form = this.dom;
    var tid = this.id.replace('MainLayout', '').replace('Edit', '').replace("details", "");
    if (iseditmode != undefined && iseditmode >= 0) {
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            // url: "../api/values/getLayoutItemsForDetails?id=" + tid, success: function (result) {
            url: "../api/Focus/GetFileds?masterid=" + this.isform + "&draw=" + drawid, success: function (result) {
                if (result.startsWith('NotFound-')) {
                    // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
                    freene = true;
                }
                else if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                else {
                    if (result.startsWith(drawid)) {

                        var moduals = JSON.parse(result.substring(drawid.toString().length));
                        //test.distinct("$.TabName").toArray();
                        var tabs = Enumerable.from(moduals)
                            .distinct("$.TabName").toArray();


                        tabs.forEach(function (el) {

                            console.log(el);
                            console.log(_temp);


                            var main = document.getElementById(_temp.id);


                            var lii = document.createElement("li");

                            lii.innerHTML = "<a href=\"#" + el.TabName + "Tab\" data-toggle=\"tab\" aria-expanded=\"true\">" + el.TabCaption + "</a>";
                            var bool = false;
                            if (main && main.children[0].children[0].children[0].children.length == 1) {
                                lii.className = " active ";
                                bool = true;
                            }
                            if (main)
                                main.children[0].children[0].children[0].insertBefore(lii, main.children[0].children[0].children[0].lastChild);





                            var dd = document.createElement("div");
                            if (bool)
                                dd.className = "tab-pane active ";
                            else
                                dd.className = "tab-pane ";
                            dd.id = el.TabName + "Tab";
                            dd.setAttribute("type", "grid");
                            dd.setAttribute("style", "position: relative; padding-top: 10px;");

                            if (main) {

                                main.children[0].children[0].children[1].appendChild(dd);
                                main.children[0].setAttribute("style", "margin-top: 20px;");
                            }

                            Dash[el.TabName + "TabInnerForm"] = new FormControl(el.TabName + "Form", "form-inline", dd.id, 0, null, "", 0, 0, 0, " ", 1, null, null, null, null, null, null, { hasmaster: 1 });
                            Dash[el.TabName + "TabInnerForm"].Customload2(el.TabName, moduals, el.TabName + "Form")





                        });

                        freene = true;
                        startlife();
                    }
                }
                free = true;
            }
        });
    }
    else {
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/Focus/GetFileds?masterid=" + this.isform + "&draw=" + drawid, success: function (result) {
                if (result.startsWith('NotFound-')) {
                    free = true;
                    // LayoutBox.Removeloading('Box' + result.replace("NotFound-", ""));
                }
                else if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                else {
                    if (result.startsWith(drawid)) {

                        var moduals = JSON.parse(result.substring(drawid.toString().length));
                        console.log(moduals);
                        //moduals.forEach(function (item) {
                        //    var flag = true;
                        //    if (masterid) {
                        //        item.parent = item.parent.replace("MainLayout", "MainLayoutdetails");
                        //    }
                        //    if (isreadmode) {
                        //        if (isreadmode == 1) {
                        //            item.readonly = true;
                        //        }
                        //        delete item.forced;
                        //        if ("11".indexOf(item.type) >= 0) {
                        //            flag = false;
                        //        }
                        //    }
                        //    if (flag)
                        //        DashBoard.additemfrom(item, iseditmode);
                        //});
                        //$("#" + this.parent).preloader('remove');
                        ////  free = true;
                        startlife();
                    }
                }
            }
        });

        freene = true;
    }



}
//______________Layout___________________//
//_______________Grid___________________//
function GridTableModel() {
    this.id = '';
    this.formid = '';
    this.gridid = '';
    this.dom = null;
    this.DBName = '';
    this.noedit = false;
    this.nodelete = false;
    this.usType = '';
    this.usTypeID = 0;
    this.hasMaster = false;
    this.masterid = '';
    this.runat = '';
    this.item = null;
    this.table = null;
}
GridTableModel.prototype.reload = function () {
    var btn = $('#' + this.dom.id).find('[KG-GRE]')[0];
    $(btn).trigger('click');
}
GridTableModel.prototype.search = function (el) {
    var tbl = this.table;
    var val = "";
    if (el)
        tbl.search(el).draw();
    else
        tbl.search("").draw();
}
GridTableModel.prototype.setFilter = function (item) {
    var tbl = this.table;
    tbl.settings().init().columns.forEach(function (el) {
        if (el.data)
            if (item.Updated.find(function (ee) { return ee.Name == el.name }))
                if (item.Updated.find(function (ee) { return ee.Name == el.name }).Val != null && item.Updated.find(function (ee) { return ee.Name == el.name }) != "")
                    tbl.column(el.name + ":name").search(item.Updated.find(function (ee) { return ee.Name == el.name }).Val);
                else
                    tbl.column(el.name + ":name").search('');
    });
    tbl.search(this.getSearchValue()).draw();
}
GridTableModel.prototype.getFilter = function () {
    var bli = [];
    var vals = {};
    var tbl = this.table;
    tbl.settings().init().columns.forEach(function (el) {
        vals[el.name] = tbl.column(el.name + ":name").search();
    });
    bli.push(vals);
    return bli;
}
GridTableModel.prototype.cleareFilter = function () {
    var tbl = this.table;
    this.setSearchValue("");
    tbl.settings().init().columns.forEach(function (el) {
        tbl.column(el.name + ":name").search('');
    });
    tbl.search(this.getSearchValue()).draw();
}
GridTableModel.prototype.getSelectedRows = function () {
    return $("#" + this.gridid).DataTable().rows('.selected').data();
}
GridTableModel.prototype.getSearchValue = function () {
    var search = $(this.dom).find("[type='search']")[0];
    return search.value;
}
GridTableModel.prototype.setSearchValue = function (el) {
    var search = $(this.dom).find("[type='search']")[0];
    search.value = el;
}
GridTableModel.prototype.reCreate = function () {
    if (eval(this.item.requirement) == true) {
        this.dom.parentNode.removeChild(this.dom);
        Dash["GridTable" + this.item.id] = new Grid(this.item);
    }
}
function Grid(item) {
    this.dom = DashBoard.addgrid(item);
    this.id = 'TableGrid' + item.id;
    this.gridid = "Grid" + item.id;
    this.usType = "Grid";
    this.usTypeID = 13;
    this.noedit = item.noedit;
    this.nodelete = item.nodelete;
    this.formid = item.formid;
    this.item = item;
    if (item.masterid) {
        this.hasMaster = true;
        this.masterid = item.masterid
    } else {
        this.hasMaster = false;
    }
    if (item.serverside)
        this.runat = 'server';
    else
        this.runat = 'local';
}
Grid.prototype = new GridTableModel();
function DGrid(item, isEditMode) {
    this.dom = DashBoard.adddetailgrid(item, isEditMode);
    this.id = 'TableDetailsGrid' + item.id;
    this.gridid = "DetailsGrid" + item.id;
    this.usType = "DetailsGrid";
    this.usTypeID = 14;
    this.noedit = item.noedit;
    this.nodelete = item.nodelete;
    this.formid = item.formid;
    this.item = item;
    if (item.masterid) {
        this.hasMaster = true;
        this.masterid = item.masterid
    } else {
        this.hasMaster = false;
    }
    if (item.serverside)
        this.runat = 'server';
    else
        this.runat = 'local';
}
DGrid.prototype = new GridTableModel();
//____________Grid___________________//
//______________________classes__________________________//
//______________________Functions___________________________//
function $waitUntil(check, onComplete, delay, timeout) {
    // if the check returns true, execute onComplete immediately
    if (check()) {
        onComplete();
        return;
    }
    if (!delay) delay = 100;
    var timeoutPointer;
    var intervalPointer = setInterval(function () {
        if (!check()) return; // if check didn't return true, means we need another check in the next interval
        // if the check returned true, means we're done here. clear the interval and the timeout and execute onComplete
        clearInterval(intervalPointer);
        if (timeoutPointer) clearTimeout(timeoutPointer);
        onComplete();
    }, delay);
    // if after timeout milliseconds function doesn't return true, abort
    if (timeout) timeoutPointer = setTimeout(function () {
        clearInterval(intervalPointer);
    }, timeout);
}
function gettarget(node) {

    while (node.nodeName != 'LI') {

        node = node.parentNode;
    }
    return node;
}
function createmoduals(result) {
    allmande = JSON.parse(result);
    var moduals = allmande.moduls;

    moduals.forEach(function (item) {
        if (item.parentmoduleid) {
            DashBoard.addMenuItemLevel(item.dmoduledefaulttitle, item.iconclassname, 'leftSideMenu' + item.parentmoduleid, item.dashboardmoduleid);
        } else {
            DashBoard.addMenuItem(item.dmoduledefaulttitle, item.iconclassname, item.dashboardmoduleid);
        }
    });
    getEntitys();
    //setplagin();
}
function getEntitys() {
    //var see = document.getElementById('SearchEN');

    //var ssw = see.value;
    var ents = allmande.entitys;
    ents.forEach(function (item) {
        //if (ssw && ssw != "" && item.dentitydefaulttitle.indexOf(ssw) < 0)
        //    return;
        DashBoard.addMenuItemLevelEntity(item.dentitydefaulttitle, item.icon, 'leftSideMenu' + item.parent, item.id, item.description);
    });
}
function getEntityItems(node) {


    var ids = gettarget(node);
    document.getElementById('PageHeader').innerHTML = node.innerHTML;
    document.getElementById('PageHeaderdes').innerHTML = ids.title;
    getEntitylayouts(ids.id.replace('leftSideMenuEn', ''));



}
function getEntitylayouts(id) {
    drawid++;
    ExtraReady = false;
    if (!isloaded) {
        isloaded = true;
        $.ajax({
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", Token);
            },
            url: "../api/values/getDEntityExtraData?id=" + id, success: function (result) {
                isloaded = false;
                resetpage();
                if (result == ("NL")) {
                    window.location.replace("../Dashboard/Login");
                }
                if (result == "{}") {
                    ExtraReady = false;
                }
                try {
                    Extra = JSON.parse(result);
                    ExtraReady = true;
                } catch (el) {
                    console.log("ExtraLoadErr => " + el);
                }
                $.ajax({
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    },
                    url: "../api/values/getDModualEntityLyouts?id=" + id + "&draw=" + drawid, success: function (result) {
                        if (result == ("NL")) {
                            window.location.replace("../Dashboard/Login");
                        }
                        if (result.startsWith(drawid)) {

                            var moduals = JSON.parse(result.substring(drawid.toString().length));
                            moduals.forEach(function (item) {
                                DashBoard.addLayouts('MainLayout' + item.id, item.classname, item.parent, item.isbox, item.boxtypeid, item.title, item.hasheader, item.hasfooter, item.onparentfooter, item.icn, item.isform);
                            });
                            startlife();
                        }
                    }
                });
            }
        });
    }

}
function resetpage() {
    document.getElementById('MainContent').innerHTML = "";
}
function setplugin() {
    Chart.plugins.register({
        afterDatasetsDraw: function (chart, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chart.ctx;

            chart.data.datasets.forEach(function (dataset, i) {
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
}
function showmodal(header, body, type, idd) {


    if (idd) {

        document.getElementById(idd + '-header').innerText = header;
        document.getElementById('MainLayout' + idd + '-body').innerHTML = body;
        $('#' + idd).modal({
            keyboard: false,
            backdrop: 'static'
        });
        return;

    }


    switch (type) {
        case 1:

            document.getElementById('msmodal-header').innerText = header;
            document.getElementById('msmodal-body').innerHTML = body;
            $('#modal-success').modal();
            break;
        case 2:
            document.getElementById('mwmodal-header').innerText = header;
            document.getElementById('mwmodal-body').innerHTML = body;
            $('#modal-warning').modal();
            break;
        case 3:
            document.getElementById('mddmodal-header').innerText = header;
            document.getElementById('mddmodal-body').innerHTML = body;
            $('#modal-danger').modal();
            break;
        case 4:
            document.getElementById('mimodal-header').innerText = header;
            document.getElementById('mimodal-body').innerHTML = body;
            $('#modal-info').modal();
            break;
        case 5:
            document.getElementById('mpmodal-header').innerText = header;
            document.getElementById('mpmodal-body').innerHTML = body;
            $('#modal-primary').modal();
            break;
        case 6:
            document.getElementById('mdmodal-header').innerText = header;
            document.getElementById('MainLayoutmdmodal-body').innerHTML = body;
            $('#modal-default').modal();
            break;
        case 7:
            document.getElementById('mdmodal-headerdelete').innerText = header;
            document.getElementById('MainLayoutmdmodal-bodydelete').innerHTML = body;
            $('#modal-defaultdelete').modal();
            break;
        case 8:
            document.getElementById('msmodal-header2').innerText = header;
            document.getElementById('msmodal-body2').innerHTML = body;
            $('#modal-success2').modal({
                keyboard: false,
                backdrop: 'static'
            });
            break;
    }

}
function hidemodal(idd) {
    if (idd) {
        $('#' + idd).modal('hide');
    }


    $('#modal-success').modal('hide');
    $('#modal-success2').modal('hide');
    $('#modal-default').modal('hide');
    $('#modal-primary').modal('hide');
    $('#modal-info').modal('hide');
    $('#modal-warning').modal('hide');
    $('#modal-danger').modal('hide');
    $('#modal-defaultdelete').modal('hide');

}
function enabledrag() {
    $('.connectedSortable').sortable({
        placeholder: 'sort-highlight',
        connectWith: '.connectedSortable',
        handle: '.box-header, .nav-tabs2',
        forcePlaceholderSize: true,
        zIndex: 999999
    });
    $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom2').css('cursor', 'move');

    // jQuery UI sortable for the todo list
    $('.todo-list').sortable({
        placeholder: 'sort-highlight',
        handle: '.handle',
        forcePlaceholderSize: true,
        zIndex: 999999
    });

}
function getTabledata2(tableid, id, mds, bmds, item) {

    var iddd = id;
    var i = 1;
    var cc = [{
        data: null,
        width: 17,
        render: function (data, type, row) {

            return "";
        },
        orderable: false,
        className: 'select-checkbox notToggleVis',


    }, {
        data: null,
        width: 30,
        render: function (data, type, row) {

            return "";
        },
        orderable: false,
        className: 'select-rowindex center notToggleVis',
    }];
    var btns = [];
    mds.forEach(function (item) {
        if (item.pval) {
            var its = {};
            switch (item.itemtypeid) {
                case 22:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["render"] = function (val, type, row) {
                        try {

                            return (type === 'display' ? '<a href="" KP-HVAL="' + item.pval + '" KG-formid="' + id + '" class="editor_Show_Wiz"><span class="glyphicon glyphicon-list-alt"></span>  نمایش </a>' : val);
                        } catch (el) {
                            return 'مشکل نمایش دیتا';
                        }
                    };
                    break;
                case 7:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["render"] = function (val, type, row) {
                        try {
                            return getgoodsize(type === 'display' ? ((val || val == 0) ? bmds[item.pval].filter(function (el) { return el.id == val })[0].text : "") : val);
                        } catch (el) {
                            return 'مشکل نمایش دیتا';
                        }
                    };
                    break;
                case 10:
                    its["name"] = item.pval;
                    its["data"] = item.pval;
                    its["render"] = function (val, type, row) {

                        return type === 'display' ? val ? '✔' : '❌' : val;
                    };
                    break;
                case 0:
                    its["data"] = item.pval;
                    its["className"] = "notToggleVis unselectable sorting ";
                    its["name"] = item.pval;
                    its["visible"] = false;
                    $('#' + tableid)[0].setAttribute("DBPR", item.pval);
                    break;
                case -1:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["className"] = "addtolocaltag notToggleVis unselectable sorting ";
                    its["visible"] = false;
                    $('#' + tableid)[0].setAttribute("DBPR", item.pval);
                default:
                    its["name"] = item.pval;
                    its["data"] = item.pval;
                    its["render"] = function (val, type, row) {
                        return getgoodsize(val);
                    };
                    break;
            }
            cc.push(its);
        }
    });


    if (item.relatedLoader) {
        var meta = JSON.parse(item.relatedLoader);
        meta.forEach(function (ell) {
            var inner = cc.filter(function (eli) { return (eli.data && eli.data == ell.name) })[0];
            if (inner) {
                inner["render"] = function (val, type, row) {
                    try {
                        return getgoodsize(type === 'display' ? ((val || val == 0) ? bmds[inner.data].filter(function (el) { return (el.id == val && el[ell.conn] == row[ell.val]) })[0].text : "") : val);
                    } catch (el) {
                        return 'مشکل نمایش دیتا';
                    }
                };
            }

            cc[cc.indexOf(inner)] = inner;
        });

    }

    var buttons = {};
    buttons["data"] = null;
    buttons["className"] = "center";
    buttons["width"] = 270;
    var btnsett = '<a href="" class="editor_edit"><span class="glyphicon glyphicon-edit"></span> ویرایش </a>  /  <a href="" class="editor_remove"><span class="glyphicon glyphicon-floppy-remove"></span> حذف </a>  /  <a href="" class="editor_Show"><span class="glyphicon glyphicon-eye-open"></span>  نمایش </a> ';
    if (item.noedit)
        btnsett = btnsett.replace('<a href="" class="editor_edit"><span class="glyphicon glyphicon-edit"></span> ویرایش </a>  /', '');
    if (item.nodelete)
        btnsett = btnsett.replace('<a href="" class="editor_remove"><span class="glyphicon glyphicon-floppy-remove"></span> حذف </a>  /', '');
    if (item.noshow)
        btnsett = btnsett.replace('<a href="" class="editor_Show"><span class="glyphicon glyphicon-eye-open"></span>  نمایش </a>', '');
    buttons["defaultContent"] = btnsett;
    if (item.condicion) {
        buttons["render"] = function (val, type, row) {
            var renval = btnsett;
            var cons = JSON.parse(item.condicion);
            if (cons["edit"]) {
                var ok = true;
                cons["edit"].forEach(function (ellk) {
                    var el = ellk;
                    while (el.v.toString().indexOf("'") >= 0) {
                        el.v = el.v.replace("'", "");
                    }
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_edit', 'editor_closed unselectable');
            }
            if (cons["delete"]) {
                var ok = true;
                cons["delete"].forEach(function (ellk) {
                    var el = ellk;
                    while (el.v.toString().indexOf("'") >= 0) {
                        el.v = el.v.replace("'", "");
                    }
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_remove', 'editor_closed unselectable');
            }
            if (cons["show"]) {
                var ok = true;
                cons["show"].forEach(function (ellk) {
                    var el = ellk;
                    while (el.v.toString().indexOf("'") >= 0) {
                        el.v = el.v.replace("'", "");
                    }
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_Show', 'editor_closed unselectable');
            }
            return renval;
        };
    }
    cc.push(buttons);
    btns.push({
        "extend": 'colvis'
        , "columns": ':not(.notToggleVis)', "background": false,
        "text": "نمایش ستون ها"
    });
    if (item.btnset) {
        var btnss = JSON.parse(item.btnset);
        for (var i = 0; i < btnss.length; i++) {
            var inner = {};
            if (btnss[i].extend) {
                btns.push({
                    "extend": 'excel',
                    "text": "دریافت فایل اکسل"
                });
            }
            else {
                inner["text"] = btnss[i].text;
                inner["action"] = new Function("e", "dt", "node", "config", btnss[i].action);// function (e, dt, node, config) { btnss[i].action };
                btns.push(inner);
            }


        }
    }

    if (item.serverside) {
        Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
            {
                processing: true,
                serverSide: true,
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var page = Dash["GridTable" + item.id].table.page.info().page;
                    var length = Dash["GridTable" + item.id].table.page.info().length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(1)', nRow).html(index);
                },
                dom: 'lBfrtip',//
                ordering: true,
                language: {
                    "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
                    "sInfo": " <a href=\"\" kg-gre=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   ",
                    "sInfoEmpty": "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ رکورد",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش ...",
                    "processing": "در حال پردازش  ...",
                    "sSearch": "جستجو  :",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "ابتدا",
                        "sLast": "انتها",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    }
                },
                select: {
                    style: 'os',
                    info: false,
                    selector: 'td:first-child',
                    blurable: true,
                    style: 'multi'
                },
                order: [],
                ajax: {
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    }, url: "/api/values/getFormDataPaging?id=" + id, type: "POST"
                    , data: NPParams
                },
                columns: cc,
                buttons: btns
            }
        );
    }
    else {
        Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
            {

                autoFill: true,
                dom: 'lBfrtip',
                ordering: true,
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var page = Dash["GridTable" + item.id].table.page.info().page;
                    var length = Dash["GridTable" + item.id].table.page.info().length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(1)', nRow).html(index);
                },
                deferRender: true,
                language: {
                    "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
                    "sInfo": " <a href=\"\" kg-gre=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   ",
                    "sInfoEmpty": "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ رکورد",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش...",
                    "sSearch": "جستجو  :",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "ابتدا",
                        "sLast": "انتها",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    }
                },
                select: {
                    style: 'os',
                    info: false,
                    selector: 'td:first-child',
                    blurable: true,
                    style: 'multi'
                },
                order: [],
                ajax: {
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    }, url: "/api/values/getFormData?id=" + id, type: "GET"
                    , data: NPParams
                },
                columns: cc,
                buttons: btns,

            }
        );
    }

    var ee = $('#' + Dash["GridTable" + item.id].gridid + '_filter');
    var wrapper = document.getElementById('Grid' + item.id + "_wrapper");
    if (wrapper) {
        wrapper.className = wrapper.className + " animated zoomInUp ";
    }

    // ee[0].innerHTML = ee[0].innerHTML + '<a href="" kg-gre="" class="GridRefresher"><i class="glyphicon glyphicon-filter" style="font-size:24px;cursor: pointer;" class="fa"></i></a>'
    ee[0].innerHTML = ee[0].innerHTML + '<a href="" class="editor_filter"><span class="glyphicon glyphicon-filter"></span></a> <a href="" class="editor_filterclear"><span class="glyphicon glyphicon-ban-circle"></span></a>';




    var search = ee.find("[type='search']")[0];
    if (search) {
        //  $(search).keyup(_.debounce(Dash["GridTable" + item.id].search(search.value), 5000));
        //                  //timer identifier
        $(search).donetyping(function () { Dash["GridTable" + item.id].search(search.value) });

        //on keydown, clear the countdown 
        //$(search).on('keydown', function () {
        //    clearTimeout(typingTimer);
        //});
    }

    //
    //if (item.columnfilter == 10) {
    //    $('#' + tableid + ' thead tr').clone(true).appendTo('#' + tableid + ' thead');
    //    $('#' + tableid + ' thead tr:eq(1) th').each(function (i) {
    //        var title = $(this).text();
    //        $(this).html('<input type="text" placeholder="سرچ ' + title + '" />');
    //        $('input', this).on('keyup change', function () {
    //            if (Dash["GridTable" + item.id].table.column(i).search() !== this.value) {
    //                Dash["GridTable" + item.id].table
    //                    .column(i)
    //                    .search(this.value)
    //                    .draw();
    //            }
    //        });
    //    });
    //}
    //if (!item.serverside) {
    //    Dash["GridTable" + item.id].table.on('order.dt search.dt', function () {
    //        Dash["GridTable" + item.id].table.column(1, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
    //            cell.innerHTML = '<span class="gri">' + (i + 1) + '</span>';
    //        });
    //    }).draw();
    //}
    $('#' + tableid).on('click', 'a.editor_Show_Wiz', function (e) {
        e.preventDefault();

        var tr = $(this).closest('tr')[0];
        var rowindex = 0;
        if (tr.className == "child")
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();//[$("#" + tableid)[0].getAttribute("dbpr")];
        else
            rowindex = $('#' + tableid).DataTable().row(tr).data();//[$("#" + tableid)[0].getAttribute("dbpr")];


        var fid = (this.attributes['kg-formid'].value);
        var fname = (this.attributes['kp-hval'].value);
        var val = (rowindex[this.attributes['kp-hval'].value]);

        DashBoard.Form.starttwzform(iddd, tableid, fid, fname, val);


    });
    //
    $('#' + tableid).on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        var rowindex = 0;
        if (tr.className == "child")
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();//[$("#" + tableid)[0].getAttribute("dbpr")];
        else
            rowindex = $('#' + tableid).DataTable().row(tr).data();//[$("#" + tableid)[0].getAttribute("dbpr")];

        DashBoard.Form.startEdit(iddd, rowindex[$("#" + tableid)[0].getAttribute("dbpr")], tableid);
    });
    $('#' + tableid).on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        var rowindex = 0;
        if (tr.className == "child")
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data()[$("#" + tableid)[0].getAttribute("dbpr")];
        else
            rowindex = $('#' + tableid).DataTable().row(tr).data()[$("#" + tableid)[0].getAttribute("dbpr")];

        DashBoard.Form.startDelete(iddd, rowindex, tableid);
    });
    $('#Table' + tableid).on('click', 'a.GridRefresher', function (e) {
        e.preventDefault();
        $("#" + tableid).preloader({ text: "در حال بارگذاری .." });
        $('#' + tableid).DataTable().ajax.reload(function () { $("#" + tableid).preloader('remove'); }, false);
    });
    $('#' + tableid).on('click', 'a.editor_Show', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        var rowindex = 0;
        if (tr.className == "child")
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();//[$("#" + tableid)[0].getAttribute("dbpr")];
        else
            rowindex = $('#' + tableid).DataTable().row(tr).data();//[$("#" + tableid)[0].getAttribute("dbpr")];

        DashBoard.Form.startEditJustShow(iddd, rowindex[$("#" + tableid)[0].getAttribute("dbpr")], tableid);
    });//
    $('#Table' + tableid).on('click', 'a.editor_filter', function (e) {
        e.preventDefault();
        DashBoard.Form.startEditFilter(iddd, 1, tableid, "GridTable" + item.id, Dash["GridTable" + item.id].getFilter());
    });
    $('#Table' + tableid).on('click', 'a.editor_filterclear', function (e) {
        e.preventDefault();
        Dash["GridTable" + item.id].cleareFilter();
    });
}
function getTabledata22(tableid, id, mds, bmds, item, iseditmode, hasdata) {
    var iddd = id;
    var i = 1;
    var cc = [{
        data: null,
        width: 17,
        render: function (data, type, row) {

            return "";
        },
        orderable: false,
        className: 'select-checkbox notToggleVis',


    }, {
        data: null,
        width: 30,
        render: function (data, type, row) {

            return "";
        },
        orderable: false,
        className: 'select-rowindex center notToggleVis',
    }];
    var btns = [];
    mds.forEach(function (item) {

        if (item.pval) {
            var its = {};
            switch (item.itemtypeid) {
                case 22:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["render"] = function (val, type, row) {
                        try {

                            return (type === 'display' ? '<a href="" KP-HVAL="' + item.pval + '" KG-formid="' + id + '" class="editor_Show_Wiz"><span class="glyphicon glyphicon-list-alt"></span>  نمایش </a>' : val);
                        } catch (el) {
                            return 'مشکل نمایش دیتا';
                        }
                    };
                    break;
                case 7:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["render"] = function (val, type, row) {
                        try {
                            return getgoodsize(type === 'display' ? ((val || val == 0) ? bmds[item.pval].filter(function (el) { return el.id == val })[0].text : "") : val);
                        } catch (el) {
                            return 'مشکل نمایش دیتا';
                        }
                    };
                    break;
                case 10:
                    its["name"] = item.pval;
                    its["data"] = item.pval;
                    its["render"] = function (val, type, row) {

                        return type === 'display' ? val ? '✔' : '❌' : val;
                    };
                    break;
                case 0:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["className"] = "notToggleVis unselectable sorting ";
                    its["visible"] = false;
                    $('#' + tableid)[0].setAttribute("DBPR", item.pval);
                    break;
                case -1:
                    its["data"] = item.pval;
                    its["name"] = item.pval;
                    its["className"] = "addtolocaltag notToggleVis unselectable sorting ";
                    its["visible"] = false;
                    $('#' + tableid)[0].setAttribute("DBPR", item.pval);
                    break;
                default:
                    its["name"] = item.pval;
                    its["data"] = item.pval;
                    its["render"] = function (val, type, row) {
                        return getgoodsize(val);
                    };
                    break;
            }
            cc.push(its);
        }

    });

    if (item.relatedLoader) {
        var meta = JSON.parse(item.relatedLoader);
        meta.forEach(function (ell) {
            var inner = cc.filter(function (eli) { return (eli.data && eli.data == ell.name) })[0];
            if (inner) {
                inner["render"] = function (val, type, row) {
                    try {
                        return getgoodsize(type === 'display' ? ((val || val == 0) ? bmds[inner.data].filter(function (el) { return (el.id == val && el[ell.conn] == row[ell.val]) })[0].text : "") : val);
                    } catch (el) {
                        return 'مشکل نمایش دیتا';
                    }
                };
            }

            cc[cc.indexOf(inner)] = inner;
        });

    }

    var buttons = {};
    buttons["data"] = null;
    buttons["className"] = "center";
    buttons["width"] = 270;
    var btnsett = '<a href="" class="editor_edit"><span class="glyphicon glyphicon-edit"></span> ویرایش </a>  /  <a href="" class="editor_remove"><span class="glyphicon glyphicon-floppy-remove"></span> حذف </a>  /  <a href="" class="editor_Show"><span class="glyphicon glyphicon-eye-open"></span>  نمایش </a> ';
    if (item.noedit)
        btnsett = btnsett.replace('<a href="" class="editor_edit"><span class="glyphicon glyphicon-edit"></span> ویرایش </a>  /', '');
    if (item.nodelete)
        btnsett = btnsett.replace('<a href="" class="editor_remove"><span class="glyphicon glyphicon-floppy-remove"></span> حذف </a>  /', '');
    if (item.noshow)
        btnsett = btnsett.replace('<a href="" class="editor_Show"><span class="glyphicon glyphicon-eye-open"></span>  نمایش </a>', '');
    buttons["defaultContent"] = btnsett;
    if (item.condicion) {
        buttons["render"] = function (val, type, row) {
            var renval = btnsett;
            var cons = JSON.parse(item.condicion);
            if (cons["edit"]) {
                var ok = true;
                cons["edit"].forEach(function (el) {
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_edit', 'editor_closed unselectable');
            }
            if (cons["delete"]) {
                var ok = true;
                cons["delete"].forEach(function (el) {
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_remove', 'editor_closed unselectable');
            }
            if (cons["show"]) {
                var ok = true;
                cons["show"].forEach(function (el) {
                    switch (el.p) {
                        case "=":
                            ok = el.t == 1 ? (row[el.n] != el.v ? false : ok) : (row[el.n] == el.v ? true : ok);
                            break;
                        case "!":
                            ok = el.t == 1 ? (row[el.n] == el.v ? false : ok) : (row[el.n] != el.v ? true : ok);
                            break;
                        case "<":
                            ok = el.t == 1 ? (row[el.n] >= el.v ? false : ok) : (row[el.n] < el.v ? true : ok);
                            break;
                        case ">":
                            ok = el.t == 1 ? (row[el.n] <= el.v ? false : ok) : (row[el.n] > el.v ? true : ok);
                            break;
                        case "C":
                            ok = el.t == 1 ? (el.v.toString().indexOf(',' + row[el.n] + ',') < 0 ? false : ok) : (el.v.toString().indexOf(',' + row[el.n] + ',') >= 0 ? true : ok);
                            break;
                    }
                });
                if (!ok)
                    renval = renval.replace('editor_Show', 'editor_closed unselectable');
            }

            if (type === 'display') {
                if (row.rowst == 4) {
                    renval = renval.replace('حذف', 'انصراف از حذف');
                }
            }

            return renval;
        };
    }
    else {
        buttons["render"] = function (val, type, row) {
            var renval = btnsett;
            if (type === 'display') {
                if (row.rowst == 4) {
                    return renval.replace('حذف', 'انصراف از حذف');
                }
            }
            return renval;
        }

    }
    cc.push(buttons);

    if (item.btnset) {
        var btnss = JSON.parse(item.btnset);
        for (var i = 0; i < btnss.length; i++) {
            var inner = {};
            if (btnss[i].extend) {
                btns.push({
                    "extend": 'excel',
                    "text": "دریافت فایل اکسل"
                });
            }
            else {
                inner["text"] = btnss[i].text;
                inner["action"] = new Function("e", "dt", "node", "config", btnss[i].action);// function (e, dt, node, config) { btnss[i].action };
                btns.push(inner);
            }

        }
    }

    btns.push({
        "extend": 'colvis'
        , "columns": ':not(.notToggleVis)', "background": false,
        "text": "نمایش ستون ها"
    });



    if (item.serverside) {
        Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
            {
                processing: true,
                serverSide: true,
                dom: 'lBfrtip',
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var page = Dash["GridTable" + item.id].table.page.info().page;
                    var length = Dash["GridTable" + item.id].table.page.info().length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(1)', nRow).html(index);
                },
                ajax: (iseditmode && iseditmode > 0) ? {
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    }, url: "/api/values/getFormDatadetailPaging?id=" + item.formid + "&masterid=" + iseditmode, type: "POST" //***
                    , data: NPParams
                } : null,
                ordering: true,
                deferRender: true,
                language: {
                    "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
                    "sInfo": (iseditmode && iseditmode > 0) ? " <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   " : "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
                    "sInfoEmpty": (iseditmode && iseditmode > 0) ? "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد" : "نمایش 0 تا 0 از 0 رکورد",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ رکورد",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش...",
                    "sSearch": "جستجو  :",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "ابتدا",
                        "sLast": "انتها",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    }
                },
                select: {
                    style: 'os',
                    info: false,
                    selector: 'td:first-child',
                    blurable: true,
                    style: 'multi'
                },
                order: [],
                columns: cc,
                buttons: btns
            });

        //Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
        //  {

        //      fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //          var page = Dash["GridTable" + item.id].table.page.info().page;
        //          var length = Dash["GridTable" + item.id].table.page.info().length;
        //          var index = (page * length + (iDisplayIndex + 1));
        //          $('td:eq(1)', nRow).html(index);
        //      },
        //      dom: 'lBfrtip',//
        //      ordering: true,
        //      language: {
        //          "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
        //          "sInfo": " <a href=\"\" kg-gre=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   ",
        //          "sInfoEmpty": "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد",
        //          "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
        //          "sInfoPostFix": "",
        //          "sInfoThousands": ",",
        //          "sLengthMenu": "نمایش _MENU_ رکورد",
        //          "sLoadingRecords": "در حال بارگزاری...",
        //          "sProcessing": "در حال پردازش ...",
        //          "processing": "در حال پردازش  ...",
        //          "sSearch": "جستجو  :",
        //          "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
        //          "oPaginate": {
        //              "sFirst": "ابتدا",
        //              "sLast": "انتها",
        //              "sNext": "بعدی",
        //              "sPrevious": "قبلی"
        //          },
        //          "oAria": {
        //              "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
        //              "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
        //          }
        //      },
        //      select: {
        //          style: 'os',
        //          info: false,
        //          selector: 'td:first-child',
        //          blurable: true,
        //          style: 'multi'
        //      },
        //      order: [],
        //      ajax: { url: "/api/values/getFormDataPaging?id=" + id, type: "POST" },
        //      columns: cc,
        //      buttons: btns
        //  }
        //     );
    }
    else {

        Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
            {
                dom: 'lBfrtip',
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    var page = Dash["GridTable" + item.id].table.page.info().page;
                    var length = Dash["GridTable" + item.id].table.page.info().length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(1)', nRow).html(index);
                },



                ajax: (iseditmode && iseditmode > 0) ? {
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", Token);
                    }, url: "/api/values/getFormDatadetail?id=" + item.formid + "&masterid=" + iseditmode, type: "GET" //***
                    , data: NPParams
                } : null,
                ordering: true,
                deferRender: true,
                language: {
                    "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
                    "sInfo": (iseditmode && iseditmode > 0) ? " <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   " : "نمایش _START_ تا _END_ از _TOTAL_ رکورد",
                    "sInfoEmpty": (iseditmode && iseditmode > 0) ? "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد" : "نمایش 0 تا 0 از 0 رکورد",
                    "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ",",
                    "sLengthMenu": "نمایش _MENU_ رکورد",
                    "sLoadingRecords": "در حال بارگزاری...",
                    "sProcessing": "در حال پردازش...",
                    "sSearch": "جستجو  :",
                    "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
                    "oPaginate": {
                        "sFirst": "ابتدا",
                        "sLast": "انتها",
                        "sNext": "بعدی",
                        "sPrevious": "قبلی"
                    },
                    "oAria": {
                        "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
                        "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
                    }
                },
                select: {
                    style: 'os',
                    info: false,
                    selector: 'td:first-child',
                    blurable: true,
                    style: 'multi'
                },
                order: [],
                columns: cc,
                buttons: btns
            });


        //Dash["GridTable" + item.id].table = $('#' + tableid).DataTable(
        //  {
        //      dom: 'lBfrtip',
        //      ordering: true,
        //      fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        //          var page = Dash["GridTable" + item.id].table.page.info().page;
        //          var length = Dash["GridTable" + item.id].table.page.info().length;
        //          var index = (page * length + (iDisplayIndex + 1));
        //          $('td:eq(1)', nRow).html(index);
        //      },
        //      deferRender: true,
        //      language: {
        //          "sEmptyTable": "هیچ داده ای در جدول وجود ندارد",
        //          "sInfo": " <a href=\"\" kg-gre=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp;  نمایش _START_ تا _END_ از _TOTAL_ رکورد   ",
        //          "sInfoEmpty": "  <a href=\"\" class=\"GridRefresher\"><i style=\"font-size:24px;cursor: pointer;\" class=\"fa\">&#xf021;</i></a> &nbsp;&nbsp; نمایش 0 تا 0 از 0 رکورد",
        //          "sInfoFiltered": "(فیلتر شده از _MAX_ رکورد)",
        //          "sInfoPostFix": "",
        //          "sInfoThousands": ",",
        //          "sLengthMenu": "نمایش _MENU_ رکورد",
        //          "sLoadingRecords": "در حال بارگزاری...",
        //          "sProcessing": "در حال پردازش...",
        //          "sSearch": "جستجو  :",
        //          "sZeroRecords": "رکوردی با این مشخصات پیدا نشد",
        //          "oPaginate": {
        //              "sFirst": "ابتدا",
        //              "sLast": "انتها",
        //              "sNext": "بعدی",
        //              "sPrevious": "قبلی"
        //          },
        //          "oAria": {
        //              "sSortAscending": ": فعال سازی نمایش به صورت صعودی",
        //              "sSortDescending": ": فعال سازی نمایش به صورت نزولی"
        //          }
        //      },
        //      select: {
        //          style: 'os',
        //          info: false,
        //          selector: 'td:first-child',
        //          blurable: true,
        //          style: 'multi'
        //      },
        //      order: [],
        //      ajax: "/api/values/getFormData?id=" + id,
        //      columns: cc,
        //      buttons: btns
        //  }
        //     );
    }














    var ee = $('#' + Dash["GridTable" + item.id].gridid + '_filter');


    // ee[0].innerHTML = ee[0].innerHTML + '<a href="" kg-gre="" class="GridRefresher"><i class="glyphicon glyphicon-filter" style="font-size:24px;cursor: pointer;" class="fa"></i></a>'
    ee[0].innerHTML = ee[0].innerHTML + '<a href="" class="editor_filter"><span class="glyphicon glyphicon-filter"></span></a> <a href="" class="editor_filterclear"><span class="glyphicon glyphicon-ban-circle"></span></a>';




    var search = ee.find("[type='search']")[0];
    if (search) {
        //  $(search).keyup(_.debounce(Dash["GridTable" + item.id].search(search.value), 5000));
        //                  //timer identifier
        $(search).donetyping(function () { Dash["GridTable" + item.id].search(search.value) });

        //on keydown, clear the countdown 
        //$(search).on('keydown', function () {
        //    clearTimeout(typingTimer);
        //});
    }







    $('#Table' + tableid).on('click', 'a.GridRefresher', function (e) {
        e.preventDefault();
        $("#" + tableid).preloader({ text: "در حال بارگذاری .." });
        $('#' + tableid).DataTable().ajax.reload(function () { $("#" + tableid).preloader('remove'); }, false);
    });

    $('#' + tableid).on('click', 'a.editor_edit', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        tr.id = "trs" + counter.toString();
        counter++;


        var rowindex = null;
        if (tr.className == "child") {
            thisrow.push(tr.previousElementSibling.id);
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();

            if ($('#' + tableid).DataTable().row(tr.previousElementSibling).data()[$("#" + tableid)[0].getAttribute("dbpr")] > 0) {
                //  thisrow[counter++] = tr;
                DashBoard.Form.startEdit(iddd, $('#' + tableid).DataTable().row(tr.previousElementSibling).data()[$("#" + tableid)[0].getAttribute("dbpr")], tableid, $('#' + tableid).DataTable().row(tr).data());
            } else {

                //   thisrow[counter++] = tr;
                DashBoard.Form.startInlineEdit(iddd, 0, tableid, rowindex);
            }
        }
        else {
            thisrow.push(tr.id);
            rowindex = $('#' + tableid).DataTable().row(tr).data();




            if ($('#' + tableid).DataTable().row(tr).data()[$("#" + tableid)[0].getAttribute("dbpr")] > 0) {
                /// thisrow[counter++] = tr;
                DashBoard.Form.startEdit(iddd, $('#' + tableid).DataTable().row(tr).data()[$("#" + tableid)[0].getAttribute("dbpr")], tableid, $('#' + tableid).DataTable().row(tr).data());
            } else {

                //  thisrow[counter++] = tr;
                DashBoard.Form.startInlineEdit(iddd, 0, tableid, rowindex);
            }
        }





    });
    $('#' + tableid).on('click', 'a.editor_remove', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        var rowindex = null;






        if (tr.className == "child") {

            if ($('#' + tableid).DataTable().row(tr.previousElementSibling).data()[$("#" + tableid)[0].getAttribute("dbpr")] > 0) {

                //  Form.startDelete(iddd, $('#' + tableid).DataTable().row(tr.previousElementSibling).data()[$("#" + tableid)[0].getAttribute("dbpr")], tableid, tr.previousElementSibling);

                if ($($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes())[0].getAttribute("deleted")) {
                    var data = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();
                    data["rowst"] = 3;
                    $('#' + tableid).DataTable().row(tr.previousElementSibling).data(data).draw(false);
                    $($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes())[0].removeAttribute("deleted")

                    $($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes()).removeClass('highlight2');
                } else {
                    var data = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();
                    data["rowst"] = 4;
                    $('#' + tableid).DataTable().row(tr.previousElementSibling).data(data).draw(false);
                    $($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes())[0].setAttribute("deleted", "1")
                    this.innerText = 'انصراف از حذف';
                    $($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes()).addClass('highlight2');
                }



                //$($('#' + tableid).DataTable().row(tr.previousElementSibling).nodes()).addClass('highlight2');
                //this.setAttribute("deleted", "1")
                //this.innerText = 'انصراف از حذف';







            } else {

                rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).remove().draw();

                $("#modal-defaultdelete").preloader('remove');
                hidemodal();
                showmodal('عملیات موفق', 'حذف اطلاعات با موفقیت انجام شد', 1);

            }
        }






        else {


            if ($('#' + tableid).DataTable().row(tr).data()[$("#" + tableid)[0].getAttribute("dbpr")] > 0) {


                if ($($('#' + tableid).DataTable().row(tr).nodes())[0].getAttribute("deleted")) {
                    var data = $('#' + tableid).DataTable().row(tr).data();
                    data["rowst"] = 3;

                    $($('#' + tableid).DataTable().row(tr).nodes()).removeClass('highlight2');
                    $('#' + tableid).DataTable().row(tr).data(data).draw(false);
                    $($('#' + tableid).DataTable().row(tr).nodes())[0].removeAttribute("deleted")
                } else {
                    var data = $('#' + tableid).DataTable().row(tr).data();
                    data["rowst"] = 4;
                    $('#' + tableid).DataTable().row(tr).data(data).draw(false);
                    $($('#' + tableid).DataTable().row(tr).nodes())[0].setAttribute("deleted", "1")
                    this.innerText = 'انصراف از حذف';
                    $($('#' + tableid).DataTable().row(tr).nodes()).addClass('highlight2');
                }
                //  Form.startDelete(iddd, $('#' + tableid).DataTable().row(tr).data()[$("#" + tableid)[0].getAttribute("dbpr")], tableid,tr);
                //var btn = this;
                //console.log(btn);
                //btn.setAttribute("deleted", "1")
                //btn.innerHTML = 'انصراف از حذف';
                //var data = $('#' + tableid).DataTable().row(tr).data();
                //data["rowst"] = 4;
                //$('#' + tableid).DataTable().row(tr).data(data).draw(false);
                //$($('#' + tableid).DataTable().row(tr).nodes()).addClass('highlight2');



            } else {

                rowindex = $('#' + tableid).DataTable().row(tr).remove().draw(false);

                $("#modal-defaultdelete").preloader('remove');
                hidemodal();
                showmodal('عملیات موفق', 'حذف اطلاعات با موفقیت انجام شد', 1);

            }
        }











        //showmodal('حذف', 'آیا مایل به حذف این رکورد هستید؟!', 7);
        //var btn = document.getElementById('deletebtnmodal');
        //btn.onclick = function (item) {
        //    $("#modal-defaultdelete").preloader({ text: "لطفا کمی صبر کنید.." });
        //    // $("#" + ids).preloader({ text: "لطفا کمی صبر کنید.." });

        //    //var jj = {};
        //    //jj["Deleted"] = rowindex;








        //    //$.ajax({
        //    //    url: "../api/values/getFormDataComboData?id=" + item.formid, success: function (result) {
        //    //        var bigmoduals = JSON.parse(result);
        //    //    }
        //    //});






        //}


    });


    $('#' + tableid).on('click', 'a.editor_Show', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr')[0];
        var rowindex = 0;
        if (tr.className == "child")
            rowindex = $('#' + tableid).DataTable().row(tr.previousElementSibling).data();//[$("#" + tableid)[0].getAttribute("dbpr")];
        else
            rowindex = $('#' + tableid).DataTable().row(tr).data();//[$("#" + tableid)[0].getAttribute("dbpr")];

        DashBoard.Form.startEditJustShow(iddd, rowindex[$("#" + tableid)[0].getAttribute("dbpr")], tableid);
    });//
    $('#Table' + tableid).on('click', 'a.editor_filter', function (e) {
        e.preventDefault();
        DashBoard.Form.startEditFilter(iddd, 1, tableid, "GridTable" + item.id, Dash["GridTable" + item.id].getFilter());
    });
    $('#Table' + tableid).on('click', 'a.editor_filterclear', function (e) {
        e.preventDefault();
        Dash["GridTable" + item.id].cleareFilter();
    });
    //if (iseditmode && iseditmode >0) {
    //    $.ajax({
    //        url: "../api/values/getGridHeaders?id=" + item.formid, success: function (result) {
    //            var moduals = JSON.parse(result);
    //        }
    //    });
    //}
    freene = true;
}
function createmodal(id, tid) {

    main = document.getElementById('MainContent');
    var flag = document.getElementById('modalflag');


    var md = document.getElementById(id);
    if (md != null) {
        md.parentElement.removeChild(md);
    }



    var modal = document.createElement("Div");
    modal.className = "modal fade";
    modal.id = id;
    modal.style.overflow = "scroll";
    modal.setAttribute("tid", tid);


    modal.innerHTML = "  <div class=\"modal-dialog\" style=\"width:98% !important;max-width:1060px !important\">    <div class=\"modal-content\">   <div class=\"modal-header\">    <button type=\"button\" class=\"close pull-left\" data-dismiss=\"modal\" aria-label=\"Close\">  <span aria-hidden=\"true\">×</span>   </button>    <h4 id=\"" + id + "-header\" class=\"modal-title\">Default Modal</h4>  </div>      <div style=\"min-height: 100px;overflow: hidden;\" id=\"MainLayout" + id + "-body\" class=\"modal-body\">     <p>One fine body…</p>   </div>    <div class=\"modal-footer\">   <button id=\"" + id + "closebtn\" type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">بستن</button>    </div>  </div>  </div>  ";


    main.parentElement.insertBefore(modal, flag);



}
function createmodalfilter(id, bid) {

    main = document.getElementById('MainContent');
    var flag = document.getElementById('modalflag');


    var md = document.getElementById(id);
    if (md != null) {
        md.parentElement.removeChild(md);
    }



    var modal = document.createElement("Div");
    modal.className = "modal fade";
    modal.id = id;
    modal.style.overflow = "scroll";



    modal.innerHTML = "  <div KG-FIL=\"\" class=\"modal-dialog\" style=\"width:98% !important;max-width:1060px !important\">    <div  KG-FIL=\"\" class=\"modal-content\">   <div class=\"modal-header\">    <button type=\"button\" class=\"close pull-left\" data-dismiss=\"modal\" aria-label=\"Close\">  <span aria-hidden=\"true\">×</span>   </button>    <h4 id=\"" + id + "-header\" class=\"modal-title\">Default Modal</h4>  </div>      <div style=\"min-height: 100px;overflow: hidden;\" id=\"MainLayout" + id + "-body\" KG-FIL=\"\" class=\"modal-body\">     <p>One fine body…</p>   </div>    <div class=\"modal-footer\">   <button id=\"" + id + "closebtn\" type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">بستن</button> <button id=\"" + id + "filterbtn\" type=\"button\" class=\"btn btn-info pull-left\"  onClick=\"getfilter(this)\" kg-kid=\"" + id + "\"  kg-bgidbtn=\"" + bid + "\">اعمال</button>    </div>  </div>  </div>  ";


    main.parentElement.insertBefore(modal, flag);



}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isNumberDecimal(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
function readonly_select(objs, action) {
    if (action) {
        $(".disabled-select", objs).remove();
        var dd = document.createElement("div");
        dd.className = "disabled-select"
        objs.prepend(dd);
    }
    else
        $(".disabled-select", objs).remove();
}
function logOut() {
    //  $(".wrapper").preloader({ text: "در حال بارگذاری .." });
    //  var jqxhr = $.post("../Desktop/LogOutDoYes?type=2")
    //.done(function () {

    //})
    //.fail(function () {

    //})
    //.always(function () {
    //    $(".wrapper").preloader("remove");
    //    window.location.replace("../Dashboard/Login");
    //});
    sessionStorage.clear();
    location.reload();
}
function getfilter(item) {//Dash['FormEditMainLayout' + //.formid]
    // console.log(Dash['FormEditMainLayout' + Dash[item.getAttribute("kg-bgidbtn")].formid].getValues());
    Dash[item.getAttribute("kg-bgidbtn")].setFilter(Dash['FormEditMainLayout' + Dash[item.getAttribute("kg-bgidbtn")].formid].getValues());
    $('#' + item.id.replace('filter', 'close')).trigger('click');
}
function notify(item, onshow, onshown, onclose, onclosed,delayc) {
    if (!item.type)
        item.type = 1;//glyphicon glyphicon-ok-sign
    switch (item.type) {
        case 1:
            $.notify({
                // options
                icon: item.icon != undefined ? item.icon : 'glyphicon glyphicon-ok-sign',
                title: item.title,
                message: item.message,
            }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "success",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: delayc ? delayc:  5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: onshow,
                    onShown: onshown,
                    onClose: onclose,
                    onClosed: onclosed,
                    icon_type: 'class',
                    template: '<div style=\"float:right;direction:rtl;\" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<button type="button"  aria-hidden="true" class="close pull-left" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span style="margin-right: 20px;" data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });
            break;
        case 2:
            $.notify({
                // options
                icon: item.icon != undefined ? item.icon : 'glyphicon glyphicon-warning-sign',
                title: item.title,
                message: item.message,
            }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "warning",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: delayc ? delayc : 5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: onshow,
                    onShown: onshown,
                    onClose: onclose,
                    onClosed: onclosed,
                    icon_type: 'class',
                    template: '<div style=\"float:right;direction:rtl;\" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<button type="button"  aria-hidden="true" class="close pull-left" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span style="margin-right: 20px;" data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });
            break;
        case 3:
            $.notify({
                // options
                icon: item.icon != undefined ? item.icon : 'glyphicon glyphicon-remove-sign',
                title: item.title,
                message: item.message,
            }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "danger",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: delayc ? delayc : 5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: onshow,
                    onShown: onshown,
                    onClose: onclose,
                    onClosed: onclosed,
                    icon_type: 'class',
                    template: '<div style=\"float:right;direction:rtl;\" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<button type="button"  aria-hidden="true" class="close pull-left" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span style="margin-right: 20px;" data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });
            break;
        case 4:
            $.notify({
                // options
                icon: item.icon != undefined ? item.icon : 'glyphicon glyphicon-info-sign',
                title: item.title,
                message: item.message,
            }, {
                    // settings
                    element: 'body',
                    position: null,
                    type: "info",
                    allow_dismiss: true,
                    newest_on_top: false,
                    showProgressbar: false,
                    placement: {
                        from: "bottom",
                        align: "left"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 1031,
                    delay: delayc ? delayc : 5000,
                    timer: 1000,
                    url_target: '_blank',
                    mouse_over: null,
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    },
                    onShow: onshow,
                    onShown: onshown,
                    onClose: onclose,
                    onClosed: onclosed,
                    icon_type: 'class',
                    template: '<div style=\"float:right;direction:rtl;\" data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<button type="button"  aria-hidden="true" class="close pull-left" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span style="margin-right: 20px;" data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });
            break;
    }

}
var tabdil = function (m) {
    if (m) {

        var num = JSON.parse('{"۰":"0","۱":"1","۲":"2","۳":"3","۴":"4","۵":"5","۶":"6","۷":"7","۸":"8","۹":"9"}');
        return m.replace(/./g, function (c) {
            return (typeof num[c] === "undefined") ?
                c :
                num[c];
        })
    } else {
        return m;
    }
}
var tabdilB = function (m) {
    if (m) {

        var num = JSON.parse('{"0":"۰","1":"۱","2":"۲","3":"۳","4":"۴","5":"۵","6":"۶","7":"۷","8":"۸","9":"۹"}');
        return m.replace(/./g, function (c) {
            return (typeof num[c] === "undefined") ?
                c :
                num[c];
        })
    } else {
        return m;
    }
}
var getgoodsize = function (val) {
    if (val && val != null) {
        var vdate = $(window).width() <= 450 ? 25 : 50;
        return (val.length > (vdate) ? val.substring(0, vdate) + " ... " : val);
    }
    else
        return val;
}
function getExtra(exp) {
    var data;
    try {
        data = eval(exp);
        return data ? data : "";
    } catch (ex) {
        console.log("extra eval Err =>" + ex);
        return "";
    }
}
function getExtraItem(exp, item) {
    var data = exp.split('~');
    try {
        var dt = item;
        for (var i = 0; i < data.length; i++) {
            dt = dt[data[i]];
        }
        return dt ? dt : "";
    } catch (ex) {
        console.log("extra eval Err =>" + ex);
        return "";
    }
}
function getExtraVal(exp, item) {
    console.log(item);
    var data = exp.split('~');
    try {
        var dt = item;
        for (var i = 0; i < data.length; i++) {
            dt = dt[data[i]];
        }
        return dt ? dt : "";
    } catch (ex) {
        console.log("extra eval Err =>" + ex);
        return "";
    }
}
function getparams() {
    let params = [];

}
function createmetaforwiz(datastr) {

    var data = JSON.parse(datastr);

    if (Extra.AAA) {
        data["groups"] = [];
        var innerformobjs = Extra[data.objectivdata];
        var innerformkeys = Extra[data.keyresultdata];
        innerformobjs.forEach(function (el) {
            var item = { Name: el.ObjectiveName, value: el.ObjectiveID, DES: el.ObjectiveComment, FieldValues: [] };
            innerformkeys.forEach(function (el2) {
                if (el2.ObjectiveID.toString() === item.value.toString()) {
                    var initem = {
                        Name: el2.ActionTypeName, DES: el2.Description, VAL: el2.ActionTypeID , TargetDate: null, BaseValue: null, TargetValue: null
                    };
                    item.FieldValues.push(initem);
                }
            });
            data.groups.push(item);
        });
    } else {
        data["groups"] = [];
        var innerformobjs = Extra[data.objectivdata];
        var innerformkeys = Extra[data.keyresultdata];
        innerformobjs.forEach(function (el) {
            var item = { Name: el.ObjectiveName, value: el.ObjectiveID, DES: el.ObjectiveComment, FieldValues: [] };
            innerformkeys.forEach(function (el2) {
                if (el2.ObjectiveID.toString() === item.value.toString()) {
                    var initem = {
                        Name: el2.KeyRName, DES: el2.TargetDate, VAL: el2.KeyRID, TargetDate: el2.TargetDate, BaseValue: el2.Base_Value, TargetValue: el2.Target_Value

                    };
                    item.FieldValues.push(initem);
                }
            });
            data.groups.push(item);
        });

    }
    console.log(data);
    return data;
}
//______________________Functions___________________________//