﻿var companyid;
var session;
var grid;
var dataManger;
var year;
var loginid;
var currentrow = 0;

var startApp = function (objWrapperResult) {
  if (objWrapperResult) {
    companyid = objWrapperResult.data.CompanyId;
    session = objWrapperResult.data.SessionId;
    year = objWrapperResult.data.YearId;
    loginid = objWrapperResult.data.LoginId;
  }
  start();
};

const validlength = {
  get vlength() {
    if ($("#serial-number-type-selector").val() == 1) {
      return 13;
    } else {
      return 12;
    }
  },
};

$("#from-date-selector").ejDatePicker({
  value: new Date(),
  dateFormat: "dd/MM/yyyy",
  open: "onOpen",
  close: "onClose",
  change: "onMonthsInputChange",
  width: "50%",
});

$("#to-date-selector").ejDatePicker({
  value: new Date(),
  dateFormat: "dd/MM/yyyy",
  open: "onOpen",
  close: "onClose",
  change: "onMonthsInputChange",
  width: "50%",
});

$("#months-inp").ejNumericTextbox({
  minValue: 0,
  width: "100%",
});

function onMonthsInputChange() {
  var str = $("#to-date-selector").data("ejDatePicker").getValue().split("/");
  var sti = $("#from-date-selector").data("ejDatePicker").getValue().split("/");

  const _toDate = new Date(str[2], str[1], str[0]);
  const _fromDate = new Date(sti[2], sti[1], sti[0]);

  //$("#months-inp").ejNumericTextbox({ value: monthDiff(_fromDate, _toDate) });
  console.clear();
  console.log(monthDiff(_fromDate, _toDate));
  //$("#months-inp").val(monthDiff(_fromDate, _toDate));
  $("#months-inp")
    .ejNumericTextbox("instance")
    .option({ value: monthDiff(_fromDate, _toDate) });
}

var start = function () {
  var companyOptions = [{ value: "ilia", label: "Ilia" }];
  var vendorOptions = [{ value: "liya", label: "Liya" }];
  var outletOptions = [{ value: "uae", label: "UAE" }];
  var itemOptions = [{ value: "cellphone", label: "Cell Phone" }];
  var warrantyProviderOptions = [{ value: "ilia", label: "Ilia" }];

  function optionsPopulator(x) {
    return `<option value="${x.value}">${x.label}</option>`;
  }

  $("#company-selector").html(companyOptions.map(optionsPopulator).join(""));
  $("#vendor-selector").html(vendorOptions.map(optionsPopulator).join(""));
  $("#outlet-selector").html(outletOptions.map(optionsPopulator).join(""));
  $("#item-selector").html(itemOptions.map(optionsPopulator).join(""));
  $("#warranty-provider-selector").html(
    warrantyProviderOptions.map(optionsPopulator).join("")
  );
  var onClick = function (args, e2) {
    //
    console.log(args);
    console.log(e2);
  };
  var Data = [];
  $("#MasterGrid").ejGrid({
    cellEdit: function (args) {
      if (args.columnObject.headerText == "Delete") {
        var gridObj = $("#MasterGrid").data("ejGrid");
        console.log(args);
        var dta = gridObj._dataManager.dataSource.json.filter(function (er) {
          return er.Number != args.rowData.Number;
        });
        var inx = 1;
        dta.forEach(function (el) {
          el.Number = inx++;
        });
        gridObj._dataManager.dataSource.json = dta;
        gridObj.refreshContent();
      } else {
        setTimeout(function () {
          $("#MasterGridText").on("keyup", function () {
            if ($("#MasterGridText").val().length == validlength.vlength) {
              if (
                args.rowData.Number ==
                $("#MasterGrid").data("ejGrid")._dataManager.dataSource.json
                  .length
              ) {
                $("#MasterGrid").data("ejGrid").editCell(0, "Text");
                $("#MasterGrid").data("ejGrid").batchSave();
                $("#MasterGrid").data("ejGrid").editCell(0, "Text");
              } else {
                $("#MasterGrid")
                  .data("ejGrid")
                  .editCell(args.rowData.Number, "Text");
                $("#MasterGrid").data("ejGrid").batchSave();
                $("#MasterGrid")
                  .data("ejGrid")
                  .editCell(args.rowData.Number, "Text");
              }
            }
          });
        }, 300);
      }
    },
    // actionComplete: function (args) {
    //   console.log("edit:", args);
    //
    // },
    selectionSettings: { selectionMode: ["cell"] },
    dataSource: Data,
    allowPaging: false,
    allowSorting: true,
    editSettings: {
      allowEditing: true,
      allowAdding: true,
      allowDeleting: true,
      editMode: "batch",
    },
    columns: [
      {
        field: "Number",
        isPrimaryKey: true,
        headerText: "No.",
        allowEditing: false,
        textAlign: ej.TextAlign.Left,
        type: "number",
        validationRules: { required: true },
        width: 25,
      },
      {
        field: "Text",
        headerText: "Serial/IMEI",
        type: "text ",
        editType: "stringedit",
        validationRules: { required: true },
        width: 100,
      },
      // {
      //   headerText: "Employee Details",
      //   allowEditing: false,
      //   commands: [
      //     {
      //       type: "details",
      //       buttonOptions: {
      //         text: "Details",
      //         click: "onClick",
      //       },
      //     },
      //   ],
      // },
      {
        headerText: "Delete",
        commands: [
          { type: ej.Grid.UnboundType.Edit, buttonOptions: { text: "Delete" } },
        ],
        isUnbound: true,
        width: 30,
      },
    ],
  });

  $("#load-btn").click(function () {
    var _length =
      $("#MasterGrid").data("ejGrid")._dataManager.dataSource.json.length;
    var selectedLength = $("#qty-inp").val();
    if (_length < selectedLength) {
      if (_length == 0) {
        $("#MasterGrid")
          .data("ejGrid")
          .dataSource([{ Number: $("#qty-inp").val(), Text: "" }]);
        for (let index = $("#qty-inp").val() - 1; index > 0; index--) {
          $("#MasterGrid")
            .data("ejGrid")
            .addRecord({ Number: index, Text: "" });
        }
      } else {
        var data = $("#MasterGrid").data("ejGrid")._dataManager.dataSource.json;
        for (let index = 1; index <= selectedLength - _length; index++) {
          data.push({ Number: _length + index, Text: "" });
        }
        $("#MasterGrid").data("ejGrid")._dataManager.dataSource.json = data;
        $("#MasterGrid").data("ejGrid").refreshContent();
      }
    }
    // $("#MasterGrid").data("ejGrid").sortColumn("Number", "ascending");
    $("#MasterGrid").data("ejGrid").editCell(0, "Text");
    currentrow = 0;
  });
};

var urlParams = new URLSearchParams(window.location.search);
var myParam = urlParams.has("debug");

if (myParam) {
  companyid = 144;
  year = 0;
  session = "311020211559588671441";
  loginid = 1;
  startApp();
} else {
  Focus8WAPI.getGlobalValue("startApp", "0", 1);
  //  startApp();
}
function monthDiff(d1, d2) {
  var months;
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= d1.getMonth();
  months += d2.getMonth();
  return months <= 0 ? 0 : months;
}
